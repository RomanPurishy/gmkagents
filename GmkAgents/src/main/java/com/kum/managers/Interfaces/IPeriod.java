package com.kum.managers.Interfaces;

import java.util.Date;

/**
 * Created by Роман on 24.12.13.
 */
public interface IPeriod {

    // Смена периода
    public void periodChanged(Date dateStart,Date dateEnd);
}
