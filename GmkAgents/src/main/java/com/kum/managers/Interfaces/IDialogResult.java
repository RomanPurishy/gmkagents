package com.kum.managers.Interfaces;

import com.kum.managers.Common.DialogResult;

/**
 * Created by Роман on 13.12.13.
 */
public interface IDialogResult {

    // Метод срабатывает после закрытия диалога
    public void DialogClose(DialogResult result,int resultCode);
}

