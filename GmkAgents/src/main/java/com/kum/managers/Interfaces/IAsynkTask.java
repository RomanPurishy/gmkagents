package com.kum.managers.Interfaces;

/**
 * Асинхроное выполнение задачи
 */
public interface IAsynkTask {

    // Событие что задача успешно выполнена
    public void TaskComplete();
    // Задача завершена с ошибкой
    public void TaskError();
}
