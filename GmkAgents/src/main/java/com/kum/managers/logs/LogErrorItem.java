package com.kum.managers.logs;

/**
 * Элемент описание ошибки
 */
public class LogErrorItem {

    public long ID; // идентификатор
    public String Description;  // описание ошибки
    public LogErrorItemTypeEnums Type; // тип ошибки

    public LogErrorItem(){
        ID = 0;
        Description = "";
        Type = LogErrorItemTypeEnums.Text;
    }

    // Создаем копию объекта
    public LogErrorItem Clone(){
        LogErrorItem item = new LogErrorItem();
        item.Description = Description;
        item.Type = Type;
        return item;
    }
}
