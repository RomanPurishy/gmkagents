package com.kum.managers.logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Роман on 11.03.14.
 */
public class LogErrorItemCollection implements List<LogErrorItem> {

    List<LogErrorItem> items = new ArrayList<LogErrorItem>();
    @Override
    public void add(int location, LogErrorItem object) {
        items.add(location,object);
    }

    @Override
    public boolean add(LogErrorItem object) {
        return items.add(object);
    }

    @Override
    public boolean addAll(int location, Collection<? extends LogErrorItem> collection) {
        return items.addAll(location,collection);
    }

    @Override
    public boolean addAll(Collection<? extends LogErrorItem> collection) {
        return items.addAll(collection);
    }

    @Override
    public void clear() {
        items.clear();
    }

    @Override
    public boolean contains(Object object) {
        return items.contains(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return items.containsAll(collection);
    }

    @Override
    public LogErrorItem get(int location) {
        return items.get(location);
    }

    @Override
    public int indexOf(Object object) {
        return items.indexOf(object);
    }

    @Override
    public boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public Iterator<LogErrorItem> iterator() {
        return items.iterator();
    }

    @Override
    public int lastIndexOf(Object object) {
        return items.lastIndexOf(object);
    }

    @Override
    public ListIterator<LogErrorItem> listIterator() {
        return items.listIterator();
    }

    @Override
    public ListIterator<LogErrorItem> listIterator(int location) {
        return items.listIterator();
    }

    @Override
    public LogErrorItem remove(int location) {
        return items.remove(location);
    }

    @Override
    public boolean remove(Object object) {
        return items.remove(object);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return items.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return items.retainAll(collection);
    }

    @Override
    public LogErrorItem set(int location, LogErrorItem object) {
        return items.set(location,object);
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public List<LogErrorItem> subList(int start, int end) {
        return items.subList(start,end);
    }

    @Override
    public Object[] toArray() {
        return items.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return items.toArray(array);
    }

    // Десиарилизация объекта
    public void DesirializeFromJson(JSONObject jsonObject) {

        try {
            JSONArray TreeRoot;
            TreeRoot = jsonObject.getJSONArray("data");
            for (int i = 0; i < TreeRoot.length(); i++) {
                //JSONObject c = TreeRoot.getString(i);
                LogErrorItem item = new LogErrorItem();
                item.Description = TreeRoot.getString(i);
                items.add(item);
            }
        } catch (JSONException e) {

        }

    }
}
