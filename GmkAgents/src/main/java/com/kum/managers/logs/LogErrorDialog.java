package com.kum.managers.logs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.kum.managers.R;


/**
 * Created by Роман on 11.03.14.
 */
public class LogErrorDialog extends android.support.v4.app.DialogFragment {

    private  LogErrorItemCollection errors = new LogErrorItemCollection();

    public void setErrorList(LogErrorItemCollection collection){
        errors = collection;
    }

    private void FillForTest(){
        LogErrorItem item = new LogErrorItem();
        item.Description = "ОШибка сохранения заявки";
        errors.add(item);
        item = new LogErrorItem();
        item.Description = "Ошибка № 2";
        errors.add(item);
        item = new LogErrorItem();
        item.Description = "Ошибка № 3";
        errors.add(item);
        item = new LogErrorItem();
        item.Description = "Заголовок";
        item.Type = LogErrorItemTypeEnums.Header;
        errors.add(item);
        item = new LogErrorItem();
        item.Description = "Ошибка № 11";
        errors.add(item);
        item = new LogErrorItem();
        item.Description = "Ошибка № 12";
        errors.add(item);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().setTitle("Внимание ошибки!");
        View v = inflater.inflate(R.layout.log_error_view, null);

        ListView lstErrors = (ListView)v.findViewById(R.id.log_error_list);


        LogErrorAdapter adapter = new LogErrorAdapter(v.getContext(), errors);
        try {
            lstErrors.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        Button btnCancel = (Button)v.findViewById(R.id.btn_close_log_error_window);

        // закрываем диалог
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        return v;
    }
}
