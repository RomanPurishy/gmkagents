package com.kum.managers.logs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kum.managers.R;

/**
 * Адаптер для логов
 */
public class LogErrorAdapter extends BaseAdapter {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private LogErrorItemCollection items;

    public LogErrorAdapter(Context context,LogErrorItemCollection collection){
        super();
        inflater =  (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        items = collection;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).ID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LogErrorItem item = items.get(position);
        View v;
        if(item.Type == LogErrorItemTypeEnums.Text){
            v =inflater.inflate(R.layout.log_error_item,null);
        }
        else{
            v =inflater.inflate(R.layout.log_error_item_header,null);
        }
        TextView txtDescription = (TextView)v.findViewById(R.id.txt_error_description);
        if(txtDescription != null){
            txtDescription.setText(item.Description);
        }
        return v;
    }
}
