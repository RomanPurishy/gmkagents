package com.kum.managers.logs;

/**
 * тип ошибки
 */
public enum LogErrorItemTypeEnums {
    Header,
    Text
}
