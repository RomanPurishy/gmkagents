package com.kum.managers.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kum.managers.Common.ActionDialog;
import com.kum.managers.Common.DialogResult;
import com.kum.managers.Common.Functions;
import com.kum.managers.Interfaces.IDialogResult;
import com.kum.managers.Interfaces.IPeriod;
import com.kum.managers.OrderActivity;
import com.kum.managers.R;
import com.kum.managers.store.Byer;
import com.kum.managers.store.Order;
import com.kum.managers.store.OrderCollection;
import com.kum.managers.store.OrderListAdapter;
import com.kum.managers.store.OrderStatus;
import com.kum.managers.store.Period;
import com.kum.managers.store.Shop;
import com.kum.managers.store.ShopListAdapter;
import com.kum.managers.store.Transaction;
import com.kum.managers.store.Workarea;

import java.util.Date;

/**
 * Created by PURISHI on 30.10.13.
 */
public class ShopsFragment extends android.support.v4.app.Fragment implements IDialogResult {

    public Byer CurrentByer;    // покупатель
    public Shop CurrentShop;        // магазин
    private Button btnDeleteOrder;
    private Button btnOpenOrder;
    private Button btnCopyOrder;        // создание копии заявки
    private Order SelectedOrder;        // выбранный заказ
    private OrderCollection Orders;
    ListView shopView;
    TextView titlePeriod;

    // покупатель
    public void setByer(Byer byer){
        CurrentByer = byer;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        double size = Functions.screenInches(getActivity());
        if(size<5){
            return inflater.inflate(R.layout.byer_shops_phone,container,false);
        }
        else{
            return inflater.inflate(R.layout.byer_shops,container,false);
        }

    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);

        Button btnByer = (Button)getActivity().findViewById(R.id.btn_byer);
        btnByer.setText(CurrentByer.Name);
        btnByer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadOrders(false);
            }
        });
        titlePeriod = (TextView)view.findViewById(R.id.title_period);
        titlePeriod.setText(Period.getTitle());
        Button btnCreateOrder = (Button)getActivity().findViewById(R.id.btnNewOrder);
        btnCreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateOrder();  // создаем новую заявку
            }
        });

        // копируем заявку
        btnCopyOrder = (Button)getActivity().findViewById(R.id.btnCopyOrder);
        btnCopyOrder.setVisibility(View.INVISIBLE);
        btnCopyOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CopyOrder();
            }
        });

        // удаляем заявку
        btnDeleteOrder = (Button)getActivity().findViewById(R.id.btnDeleteOrder);
        btnDeleteOrder.setVisibility(View.INVISIBLE);
        btnDeleteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // вызываем диалог
                showDialog();
            }
        });

        // отображаем свойства заявки
        btnOpenOrder = (Button)getActivity().findViewById(R.id.btnOpenOrder);
        btnOpenOrder.setVisibility(View.INVISIBLE);
        btnOpenOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SelectedOrder != null){
                    OpenOrder(SelectedOrder);
                }
            }
        });

        LoadShops(); // загрузка магазинов

        // загрузка заявок по покупателю
        LoadOrders(false);
        Period.setPeriodChangedListener(new IPeriod() {
            @Override
            public void periodChanged(Date dateStart, Date dateEnd) {
                // загрузка заявок по покупателю
                LoadOrders(false);
                titlePeriod.setText(Period.getTitle());
            }
        });

    }

    // Загрузка магазинов
    private void LoadShops(){

        if(!CurrentByer.Shops.IsLoaded){
            CurrentByer.Shops.Load();
        }


        // заполняем наш список
        shopView = (ListView)getActivity().findViewById(R.id.shopsList);
        shopView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        shopView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                CurrentShop = CurrentByer.Shops.get(position);
                Orders.LoadByShop(Period.Start, Period.End,CurrentShop.ID,getActivity());
                ListView ordersList = (ListView)getActivity().findViewById(R.id.ordersList);
                OrderListAdapter ad= (OrderListAdapter)ordersList.getAdapter();
                ad.notifyDataSetChanged();
                parent.requestFocusFromTouch(); // IMPORTANT!
                parent.setSelection(position);
            }
        });
        ShopListAdapter adapter = new ShopListAdapter(getActivity(), CurrentByer.Shops);
        try {
            shopView.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }

    // Загрузка заявок по покупателю
    private void LoadOrders(boolean byShop){
        Orders = new OrderCollection();
        if(byShop){
            Orders.LoadByShop(Period.Start, Period.End,CurrentShop.ID,getActivity());
        }
        else{
            Orders.LoadByByer(Period.Start, Period.End, CurrentByer.ID, getActivity());
        }

        ListView ordersList = (ListView)getActivity().findViewById(R.id.ordersList);
        ordersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedOrder = Orders.get(position);
                if(SelectedOrder.Status== OrderStatus.New || SelectedOrder.Status ==OrderStatus.Drufted){
                    btnDeleteOrder.setVisibility(View.VISIBLE);
                }
                else{
                    btnDeleteOrder.setVisibility(View.INVISIBLE);
                }
                if(SelectedOrder.Status != OrderStatus.New){
                    btnCopyOrder.setVisibility(View.VISIBLE);
                }
                else{
                    btnCopyOrder.setVisibility(View.INVISIBLE);
                }
                btnOpenOrder.setVisibility(View.VISIBLE);
            }
        });
        OrderListAdapter adapter = new OrderListAdapter(getActivity(), Orders);
        try {
            ordersList.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        btnCopyOrder.setVisibility(View.INVISIBLE);
        btnDeleteOrder.setVisibility(View.INVISIBLE);
        btnOpenOrder.setVisibility(View.INVISIBLE);
    }

    // Метод открывает текущую заявку
    private void OpenOrder(Order od){
        Intent intent = new Intent(getActivity(),OrderActivity.class);
        intent.putExtra("order",od);
        startActivityForResult(intent, Workarea.ORDER_ACTIVITY);
    }

    // создаем новую заявку
    private void CreateOrder(){
        Intent intent = new Intent(getActivity(),OrderActivity.class);
        Order order = new Order(0);
      //  order.Status =OrderStatus.Executed;
        order.Byer.ID = CurrentByer.ID;
        order.Byer.Name = CurrentByer.Name;
        order.Manager.ID = Workarea.Manager.ID;
        order.Manager.Name = Workarea.Manager.Name;

        if(CurrentShop != null){
            order.Shop.ID = CurrentShop.ID;
            order.Shop.Name = CurrentShop.Name;
        }

        order.Number = "-";
        intent.putExtra("order",order);
        startActivityForResult(intent, Workarea.ORDER_ACTIVITY);
    }

    // Копируем заявку
    private void CopyOrder(){
        Intent intent = new Intent(getActivity(),OrderActivity.class);
        Order order = new Order(0);
        SelectedOrder.TransList.Load(getActivity(),SelectedOrder.ID);
        order.Byer.ID = SelectedOrder.Byer.ID;
        order.Byer.Name = SelectedOrder.Byer.Name;
        order.Manager.ID = Workarea.Manager.ID;
        order.Manager.Name = Workarea.Manager.Name;
        order.Shop.ID = SelectedOrder.Shop.ID;
        order.Shop.Name = SelectedOrder.Shop.Name;
        order.WhareHouse.ID = SelectedOrder.WhareHouse.ID;
        order.WhareHouse.Name = SelectedOrder.WhareHouse.Name;
        order.Save(getActivity());
        order.Load(getActivity());
        for (Transaction transaction : SelectedOrder.TransList){
            Transaction trans = new Transaction();
            trans.OrderID=order.ID;
            trans.OnDate = order.OnDate;
            trans.Qty = transaction.Qty;
            trans.Fqty = transaction.Fqty;
            trans.Price = transaction.Price;
            trans.Sum = transaction.Sum;
            trans.Good.ID = transaction.Good.ID;
            trans.Good.Name = transaction.Good.Name;
            order.TransList.setTansactionNumber(getActivity(),trans,order.ID);
            order.TransList.Insert(getActivity(),trans);
            order.TransList.Recalc();
        }

        order.Number = "-";
        intent.putExtra("order",order);
        startActivityForResult(intent, Workarea.ORDER_ACTIVITY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Workarea.ORDER_ACTIVITY:
                if(CurrentShop != null){
                    LoadOrders(true);
                }
                else{
                    LoadOrders(false);
                }
                break;
            default:
                super.onActivityResult(requestCode,resultCode,data);
                break;
        }
    }

    // Отображаем диалог удаления заявки
    void showDialog() {
        ActionDialog dialog = new ActionDialog();
        dialog.setParams("Удаление заявки", "Удалить выбраную заявку. Внимание после удаления заявку нельзя будет восстановить.",ShopsFragment.this,100);
        dialog.show(getFragmentManager(),"deleteOrderDialog");
    }


    @Override
    public void DialogClose(DialogResult result, int resultCode) {
        if(result==DialogResult.Ok){
            Orders.DeleteOrder(getActivity(),SelectedOrder);
            if(CurrentShop != null){
                LoadOrders(true);
            }
            else{
                LoadOrders(false);
            }
        }
    }

}
