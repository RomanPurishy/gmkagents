package com.kum.managers.fragments;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kum.managers.Common.Functions;
import com.kum.managers.R;
import com.kum.managers.store.Good;
import com.kum.managers.store.GoodCollection;
import com.kum.managers.store.GoodListSelectorAdapter;
import com.kum.managers.store.GoodPopular;
import com.kum.managers.store.Order;
import com.kum.managers.store.Transaction;

import java.util.ArrayList;

/**
 * Диалог выбора товара
 */
public class GoodSelectorDialog extends DialogFragment implements  View.OnClickListener{

    public GoodCollection goods; // коллекция товаров
    private Order order;        // заявка
    ListView lstGoods;
    EditText txtSearch;
    CalculatorDialog calculatorDialog;      // калькулятор
    TextView txtFqty;
    TextView txtQty;
    TextView txtSum;
    private boolean showAll = false;


    private int RESULT_SPEECH_TO_TEXT=100;
/*
    public GoodSelectorDialog(GoodCollection collection,Order od){
        goods= collection;
        order = od;
    }*/

    // после загрузки
    public void onStart() {
        super.onStart();
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
/*        getDialog().setTitle("Выберите товар!");*/

        // получаем аргументы
        Bundle bundle=getArguments();
        goods = (GoodCollection)bundle.getSerializable("GOODS");
        order = (Order)bundle.getSerializable("ORDER");

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View v;

        double size = Functions.screenInches(getActivity());
        if(size<=5){
            v = inflater.inflate(R.layout.good_selector_layout_phone, null,false);
        }
        else{
            v = inflater.inflate(R.layout.good_selector_layout, null,false);
        }

        lstGoods = (ListView)v.findViewById(R.id.goodsList2);
        lstGoods.requestFocus();
        txtSearch = (EditText)v.findViewById(R.id.txtSearch);
        txtFqty = (TextView)v.findViewById(R.id.total_fqty);
        txtQty = (TextView)v.findViewById(R.id.total_qty);
        txtSum = (TextView)v.findViewById(R.id.total_sum);

        // отображаем все товары или показываем только по указаной группы
        final Button btnShowType = (Button)v.findViewById(R.id.btnShowType);
        btnShowType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(showAll){
                    btnShowType.setText("Показать все товары");
                    showAll = false;
                    goods.LoadPopular(getActivity(),order.WhareHouse.ID,order.Byer.ID,order.Shop.ID,order.ID);
                    GoodListSelectorAdapter adpt = (GoodListSelectorAdapter)lstGoods.getAdapter();
                    adpt.notifyDataSetChanged();
                }
                else{
                    btnShowType.setText("Показать популярные для покупателя");
                    goods.LoadByWarehouse(order.WhareHouse.ID,order.ID);
                    GoodListSelectorAdapter adpt = (GoodListSelectorAdapter)lstGoods.getAdapter();
                    adpt.notifyDataSetChanged();
                    showAll = true;
                }
            }
        });

        ImageButton btnClearFilter = (ImageButton)v.findViewById(R.id.filter_clear);
        btnClearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSearch.setText("");
                SearchGood();
            }
        });

        ImageButton btnReload = (ImageButton)v.findViewById(R.id.filter_search);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchGood();
            }
        });

        ImageButton btnSearch = (ImageButton)v.findViewById(R.id.btnSearchByVoice);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent speechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                    speechIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Назовите название товара");
                    startActivityForResult(speechIntent, RESULT_SPEECH_TO_TEXT);
                }
                catch (Exception ex){
                    Functions.showMessage(getActivity(), R.layout.order, ex.getMessage(), Functions.ToastMessageType.Error,5000);
                }

            }
        });

        final GoodListSelectorAdapter adapter = new GoodListSelectorAdapter(v.getContext(), goods);
        try {
            lstGoods.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        Button btnCancel = (Button)v.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        // изменение текста
        txtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(txtSearch.getText().length()==1){
                    SearchGood();
                }
            }
        });
        txtSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                switch (keyCode){
                    case 66:  // нажали enter
                        if(event.getAction() == KeyEvent.ACTION_DOWN){
                            SearchGood();
                            txtSearch.setText("");
                        }
                        return true ;
                }
                return false;
            }
        });

        // выбираем товар
        lstGoods.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(final AdapterView<?> parent, View view, int position, long id) {
                final Good selectedGood = goods.get(position);
                SelectedItemView = view;
                if(SelectedListener !=null){
                    SelectedListener.ItemSelected(selectedGood);
                }
                view.setSelected(true);
                calculatorDialog.setData(order,selectedGood);
                calculatorDialog.show(getFragmentManager(),"calculator");
                //dismiss();
            }
        });
        updateTotals();
        SetCalculator();  // установка калькулятора
        txtSearch.clearFocus();
        return v;
    }


    // обновляем итоги
    private void updateTotals(){
        txtFqty.setText(Functions.Format(order.TransList.Fqty));
        txtQty.setText(Functions.Format(order.TransList.Qty));
        txtSum.setText(Functions.Format(order.TransList.Sum));
    }

    private void SearchGood(){
        goods.SearchByName(order.WhareHouse.ID, txtSearch.getText().toString(),order.ID);
        GoodListSelectorAdapter adpt = (GoodListSelectorAdapter)lstGoods.getAdapter();
        adpt.notifyDataSetChanged();

    }

    public View SelectedItemView;

    // Открываем калькулятор и добавляем проводку в список
    private void SetCalculator(){
        calculatorDialog = new CalculatorDialog();
        calculatorDialog.CanTransactionDelete = true;
        calculatorDialog.setOnCloseDialogListener(new CalculatorDialog.IDialogClose() {
            @Override
            public void Closed(CalculatorDialog.ButtonType type) {
                if(type == CalculatorDialog.ButtonType.Ok){
                    if(calculatorDialog.good.Qty>0){
                        GoodListSelectorAdapter adpt= (GoodListSelectorAdapter)lstGoods.getAdapter();
                        Transaction trans = new Transaction();
                        trans.OrderID=order.ID;
                        trans.OnDate = order.OnDate;
                        trans.Qty = calculatorDialog.good.Qty;
                        trans.Fqty = calculatorDialog.good.Fqty;
                        trans.Price = calculatorDialog.good.Price;
                        trans.Sum = calculatorDialog.good.Sum;
                        trans.Good.ID = calculatorDialog.good.ID;
                        trans.Good.Name = calculatorDialog.good.Name;
                        order.TransList.setTansactionNumber(getActivity(),trans,order.ID);
                        order.TransList.Insert(getActivity(),trans);
                        adpt.UpdateRow(SelectedItemView, calculatorDialog.good);
                        order.TransList.Recalc();
                        updateTotals();
                        // запоминаем продукцию в популярных
                        GoodPopular popular = new GoodPopular();
                        popular.Add(trans.Good.ID,order.Byer.ID,order.Shop.ID,getActivity());
                    }
                    else{
                        int transID = 0;
                        for(Transaction tr : order.TransList){
                            if(tr.Good.ID == calculatorDialog.good.ID){
                                transID = tr.ID;
                                order.TransList.DeleteTransaction(getActivity(),tr);
                                break;
                            }
                        }
                        if(transID>0){
                            GoodListSelectorAdapter adpt= (GoodListSelectorAdapter)lstGoods.getAdapter();
                            adpt.UpdateRow(SelectedItemView, calculatorDialog.good);
                            updateTotals();
                        }

                    }
                }
            }
        });
    }

    public void onClick(View v) {
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if(DialogFragmentListener != null){
            DialogFragmentListener.Closed();
        }
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    // интерфейс закрытия окна
    private IDialogFragment DialogFragmentListener;
    public static interface IDialogFragment{
        public void Closed();
    }
    public void setDialogFragmentListener(IDialogFragment dialog){
        DialogFragmentListener = dialog;
    }

    private ISelected SelectedListener;
    public static interface ISelected{

        // Метод после выбора элемента
        public void ItemSelected(Good good);
    }

    public void setOnSelectedListener(ISelected selected){
        SelectedListener = selected;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_SPEECH_TO_TEXT) {
            ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(matches.size()>0){
                String founded = matches.get(0).replace("плюс"," ");
                txtSearch.setText(founded);
                SearchGood();
            }

        }
    }
}
