package com.kum.managers.fragments;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.kum.managers.ByersListAdapter;
import com.kum.managers.R;
import com.kum.managers.store.Byer;
import com.kum.managers.store.ByerCollection;

/**
 * Created by PURISHI on 06.11.13.
 */
public class ByerSelectorDialog extends DialogFragment implements OnClickListener{

    public Byer SelectedByer;  // выбранный покупатель
    final String LOG_TAG = "myLogs";
    public ByerCollection byers; // коллекция покупателей

    // Метод устанавливает коллекцию покупателей
    public void setByers(ByerCollection collection){
        byers = collection;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().setTitle("Выберите покупателя!");
        View v = inflater.inflate(com.kum.managers.R.layout.byer_selector_layout, null);

        ListView lstByers = (ListView)v.findViewById(R.id.byers_list);
        ByersListAdapter adapter = new ByersListAdapter(v.getContext(), byers);
        try {
            lstByers.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        Button btnCancel = (Button)v.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectedByer = null;
                dismiss();
            }
        });
        // выбираем покупателя
        lstByers.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedByer = byers.get(position);
                if(SelectedListener !=null){
                    SelectedListener.ItemSelected(SelectedByer);
                }
                view.setSelected(true);
                dismiss();
            }
        });

        return v;
    }

    public void onClick(View v) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        SelectedByer = null;
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }


    private ISelected SelectedListener;
    public static interface ISelected{

        // Метод после выбора элемента
        public void ItemSelected(Byer byer);
    }

    public void setOnSelectedListener(ISelected selected){
        SelectedListener = selected;
    }

}
