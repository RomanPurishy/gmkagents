package com.kum.managers.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.kum.managers.Common.ActionDialog;
import com.kum.managers.Common.DateFunctions;
import com.kum.managers.Common.DialogResult;
import com.kum.managers.Common.Functions;
import com.kum.managers.Interfaces.IAsynkTask;
import com.kum.managers.Interfaces.IDialogResult;
import com.kum.managers.Interfaces.IPeriod;
import com.kum.managers.OrderActivity;
import com.kum.managers.R;
import com.kum.managers.datastore.SendOrders;
import com.kum.managers.store.Order;
import com.kum.managers.store.OrderCollection;
import com.kum.managers.store.OrderListAdapter;
import com.kum.managers.store.OrderStatus;
import com.kum.managers.store.Period;
import com.kum.managers.store.Workarea;

import java.util.Calendar;
import java.util.Date;


// Список последних заявок
public class OrdersFragment extends android.support.v4.app.Fragment  implements IDialogResult{

    private ListView ordersView;
    private OrderStatus Status = OrderStatus.New;
    private boolean Onlylast=true;
    private boolean loaded=false;
    public Order SelectedOrder;
    private TextView qty;
    private Button btnOpen;
    private Button btnDeleteOrder;
    Button btnSendAllOrder;
    private OrderCollection _orders;     //текущие заявки
    OrderCollection _notSended;
    TextView titlePeriod;

    // после загрузки
    public void onStart() {
        super.onStart();
        loaded = true;
        LoadOrders();
        Period.setPeriodChangedListener(new IPeriod() {
            @Override
            public void periodChanged(Date dateStart, Date dateEnd) {
                LoadOrders();
                titlePeriod.setText(Period.getTitle());
            }
        });
    }

    public void setParams(OrderStatus status,boolean last){
        Onlylast = last;
        Status = status;
        if(loaded){
            LoadOrders();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.orders, null);
        qty = (TextView)v.findViewById(R.id.orders_qty);

        btnOpen = (Button)v.findViewById(R.id.btnOpenOrder);
        titlePeriod = (TextView)v.findViewById(R.id.title_period);
        btnSendAllOrder = (Button)v.findViewById(R.id.btnSendAllOrder);
        titlePeriod.setText(Period.getTitle());

        // открываем заявку
        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),OrderActivity.class);
                intent.putExtra("order",SelectedOrder);
                startActivityForResult(intent, Workarea.ORDER_ACTIVITY);
            }
        });

        // удаляем выбранную заявку из БД
        btnDeleteOrder = (Button)v.findViewById(R.id.btnDeleteOrder);
        btnDeleteOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionDialog dialog = new ActionDialog();
                dialog.setParams("Удаление заявки", "Удалить выбраную заявку. Внимание после удаления заявку нельзя будет восстановить.",OrdersFragment.this,100);
                dialog.show(getFragmentManager(),"deleteOrderDialog");
            }
        });

        // отправляем на сбыт все заявки
        btnSendAllOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _notSended= new OrderCollection();

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(DateFunctions.getCurrentShortDate());
                calendar.add(Calendar.DAY_OF_MONTH,1);
                Date dtStart = calendar.getTime();
                _notSended.Load(dtStart,DateFunctions.getDate(4000,0,1,0,0,0),OrderStatus.New,getActivity());
                if(_notSended.size()>0){
                    ActionDialog dialog = new ActionDialog();
                    dialog.setParams("Отправка заявок", "Внимание будут отправлены все заявки дата которых начинается с " +Functions.getDateShortString(dtStart),OrdersFragment.this,200);
                    dialog.show(getFragmentManager(),"deleteOrderDialog");
                }
                else{
                    Functions.showMessage(getActivity(),R.layout.activity_main,"Список заявок пуст. Нечего отправлять", Functions.ToastMessageType.Error,5000);
                }
            }
        });
        ordersView= (ListView)v.findViewById(R.id.orders_list);
        return v;

    }

    View SelectedView;

    // Метод загружает заявки
    public void LoadOrders(){
        _orders = new OrderCollection();
        if(Onlylast){
            _orders.Load(Period.Start,Period.End,getActivity());
        }
        else{
            _orders.Load(Period.Start,Period.End,Status,getActivity());
        }
        qty.setText(_orders.size()+"");
        OrderListAdapter adpt = new OrderListAdapter(getActivity(),_orders);
        ordersView.setAdapter(adpt);
        ordersView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SelectedView = view;
                SelectedOrder =_orders.get(position);
                switch (SelectedOrder.Status){
                    case New:
                        btnOpen.setVisibility(View.VISIBLE);
                        btnDeleteOrder.setVisibility(View.VISIBLE);
                        break;
                    case Sended:
                        btnOpen.setVisibility(View.VISIBLE);
                        btnDeleteOrder.setVisibility(View.GONE);
                        break;
                    case Drufted:
                        btnOpen.setVisibility(View.VISIBLE);
                        btnDeleteOrder.setVisibility(View.VISIBLE);
                        break;
                    case Deleted:
                        btnOpen.setVisibility(View.VISIBLE);
                        btnDeleteOrder.setVisibility(View.GONE);
                        break;
                    case Executed:
                        btnOpen.setVisibility(View.VISIBLE);
                        btnDeleteOrder.setVisibility(View.GONE);
                        break;
                }
                parent.requestFocusFromTouch(); // IMPORTANT!
                parent.setSelection(position);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Workarea.ORDER_ACTIVITY:
                //Functions.showMessage(getActivity(),R.layout.activity_main,SelectedOrder.Shop.Name, Functions.ToastMessageType.Info,5000);
                OrderListAdapter adpt = (OrderListAdapter)ordersView.getAdapter();
                adpt.UpdateOrderView(SelectedView,SelectedOrder);
                break;
            default:
                super.onActivityResult(requestCode,resultCode,data);
                break;
        }
    }

    @Override
    public void DialogClose(DialogResult result, int resultCode) {
        if(result==DialogResult.Ok){
            switch (resultCode){
                case 100:
                    _orders.DeleteOrder(getActivity(),SelectedOrder);
                    LoadOrders();
                    break;
                case 200: // отправляем заявки на сервер
                    if(_notSended != null){
                        final SendOrders action= new SendOrders(getActivity(),_notSended);
                        action.setOnCompleteListener(new IAsynkTask() {
                            @Override
                            public void TaskComplete() {
                                action.ShowErrors(getFragmentManager());
                                Functions.showMessage(getActivity(),R.layout.activity_main,"Отправка завершена", Functions.ToastMessageType.Info,5000);
                                LoadOrders();
                            }

                            @Override
                            public void TaskError() {

                            }
                        });
                        action.CheckBeforeSend();
                    }

                    break;
            }
        }
    }


}
