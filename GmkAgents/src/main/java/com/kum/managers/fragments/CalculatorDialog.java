package com.kum.managers.fragments;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kum.managers.Common.Functions;
import com.kum.managers.Controls.Border;
import com.kum.managers.Controls.BorderedTextView;
import com.kum.managers.R;
import com.kum.managers.store.Good;
import com.kum.managers.store.Order;


/**
 * Калькулятор
 */
public class CalculatorDialog extends DialogFragment implements View.OnClickListener{

    public Order order;  // текущая заявка
    public Good good;  // текущий товар
    ImageButton btnSave; // кнопка сохранения количества
    TextView txtQty;        // набраное количество по товару


    public void setData(Order od,Good gd){
        order = od;
        good = gd;
    }

    public boolean CanTransactionDelete = false ;  // можно удалить ли проводку

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View v;
        double size = Functions.screenInches(getActivity());
        if(size<=5){
            v = inflater.inflate(R.layout.calculator_phone, null,false);
        }
        else{
            v = inflater.inflate(R.layout.calculator, null,false);
        }

        TextView txtName = (TextView)v.findViewById(R.id.txt_good_name);
        txtName.setText(good.Name);
        btnSave = (ImageButton)v.findViewById(R.id.btn_addTransQty);  // кнопка сохранения количества
        ImageButton btnClose = (ImageButton)v.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        txtQty = (TextView)v.findViewById(R.id.txt_weight);
        Button calc0 = (Button)v.findViewById(R.id.calc0);
        Button calc1 = (Button)v.findViewById(R.id.calc1);
        Button calc2 = (Button)v.findViewById(R.id.calc2);
        Button calc3 = (Button)v.findViewById(R.id.calc3);
        Button calc4 = (Button)v.findViewById(R.id.calc4);
        Button calc5 = (Button)v.findViewById(R.id.calc5);
        Button calc6 = (Button)v.findViewById(R.id.calc6);
        Button calc7 = (Button)v.findViewById(R.id.calc7);
        Button calc8 = (Button)v.findViewById(R.id.calc8);
        Button calc9 = (Button)v.findViewById(R.id.calc9);
        Button calcPoint = (Button)v.findViewById(R.id.calcPoint);
        ImageButton btnErase = (ImageButton)v.findViewById(R.id.btn_erase);
        calc0.setOnClickListener(this);
        calc1.setOnClickListener(this);
        calc2.setOnClickListener(this);
        calc3.setOnClickListener(this);
        calc4.setOnClickListener(this);
        calc5.setOnClickListener(this);
        calc6.setOnClickListener(this);
        calc7.setOnClickListener(this);
        calc8.setOnClickListener(this);
        calc9.setOnClickListener(this);
        calcPoint.setOnClickListener(this);
        btnErase.setOnClickListener(this);

        if(good.Qty>0){
            txtQty.setText(Functions.Format(good.Qty));
        }
        else{
            txtQty.setText("0");
        }
        final Button btnWeight = (Button)v.findViewById(R.id.weight_info);

        // Сохраняем значение
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            String  result = txtQty.getText().toString();
           Double qty = Double.parseDouble(result.replace(",", "."));
                if(result.length()>0 && qty>0){
                    if(btnWeight.getText().equals("Вес")){
                        setQty();
                    }
                    else{
                        setFqty();
                    }
                    if(DialogClosedListener !=null){
                        DialogClosedListener.Closed(ButtonType.Ok);
                    }
                    dismiss();
                }
                else{
                    if(CanTransactionDelete){
                        if(btnWeight.getText().equals("Вес")){
                            setQty();
                        }
                        else{
                            setFqty();
                        }
                        if(DialogClosedListener !=null){
                            DialogClosedListener.Closed(ButtonType.Ok);
                        }
                        dismiss();
                    }
                    else{
                        Functions.showMessage(getActivity(),R.layout.order,"Значение не должно быть равно 0", Functions.ToastMessageType.Error,3000);
                    }
                }
            }
        });


        // Меняем штуки/вес
        btnWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(btnWeight.getText().equals("Вес")){
                    setQty();
                    txtQty.setText(Functions.Format(good.Fqty));
                    btnWeight.setText("Шт");
                    btnWeight.setBackgroundResource(R.drawable.button_black);
                    btnWeight.setTextColor(getResources().getColor(R.color.text_white));
                }
                else{
                    setFqty();
                    txtQty.setText(Functions.Format(good.Qty));
                    btnWeight.setText("Вес");
                    btnWeight.setBackgroundResource(R.drawable.button_lemon);
                    btnWeight.setTextColor(getResources().getColor(R.color.text));
                }
            }
        });

        setAttr(v);
        return v;
    }

    // устанавливаем колличество
    private void setQty(){
        Double qty = Double.parseDouble(txtQty.getText().toString().replace(",","."));
        if(qty>0){
            good.Qty = qty;
            good.Sum =Functions.Round(qty * good.Price,2);
        }
        else{
            good.Qty = 0.0;
            good.Sum = 0.0;
        }
    }

    // устанавливаем произвольное количество
    private void setFqty(){
        Double fQty = Double.parseDouble(txtQty.getText().toString().replace(",","."));
        if(fQty>0){
            good.Fqty = fQty;
        }
        else{
            good.Fqty = 0.0;
        }
    }

    @Override
    public void onClick(View v){
        switch (v.getId()) {
            case R.id.calc0: setQty("0"); break;
            case R.id.calc1: setQty("1"); break;
            case R.id.calc2: setQty("2"); break;
            case R.id.calc3: setQty("3"); break;
            case R.id.calc4: setQty("4"); break;
            case R.id.calc5: setQty("5"); break;
            case R.id.calc6: setQty("6"); break;
            case R.id.calc7: setQty("7"); break;
            case R.id.calc8: setQty("8"); break;
            case R.id.calc9: setQty("9"); break;
            case R.id.calcPoint: setQty(","); break;
            case R.id.btn_erase: setQty("<"); break;
        }
    }

    // Установка количества
    private void setQty(String val){
        String prevQty = txtQty.getText().toString();
        if(prevQty.equals("0") && !val.equals(",")){
            prevQty = "";
        }
        if(val=="<"){
            if(prevQty.length()>1){
                CharSequence result= prevQty.subSequence(0,prevQty.length()-1);
                txtQty.setText(result);
            }
            else{
                txtQty.setText("0");
            }
        }
        else{
            String result=prevQty+""+val;
            int indexPoint = result.indexOf(",");
            if(indexPoint>0){
                int pointIndex = indexPoint+4;
                if(pointIndex==result.length()){
                    txtQty.setText(val);
                }
                else{
                    txtQty.setText(result);
                }
            }
            else{
                txtQty.setText(result);
            }
        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(DialogClosedListener !=null){
            DialogClosedListener.Closed(ButtonType.Exit);
        }
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        if(DialogClosedListener !=null){
            DialogClosedListener.Closed(ButtonType.Exit);
        }
        super.onCancel(dialog);
    }

    private void setAttr(View v){
        BorderedTextView txtWeight= (BorderedTextView)v.findViewById(R.id.txt_weight);

        Border brdLeft = new Border(txtWeight.BORDER_LEFT);
        brdLeft.setColor(Color.BLACK);
        brdLeft.setWidth(2);
        Border brdBottom = new Border(txtWeight.BORDER_BOTTOM);
        brdBottom.setColor(Color.BLACK);
        brdBottom.setWidth(2);
        Border brdRight = new Border(txtWeight.BORDER_RIGHT);
        brdRight.setColor(Color.BLACK);
        brdRight.setWidth(2);
        Border brdTop = new Border(txtWeight.BORDER_TOP);
        brdTop.setColor(Color.BLACK);
        brdTop.setWidth(2);
        txtWeight.setBorders(new Border[]{brdBottom,brdLeft,brdRight,brdTop});
    }

    private IDialogClose DialogClosedListener;
    public static interface IDialogClose{

        // Тип нажатой кнопки
        public void Closed(ButtonType type);
    }

    public void setOnCloseDialogListener(IDialogClose closeDialogListener){
        DialogClosedListener = closeDialogListener;
    }

    // тип кнопки
    public enum ButtonType{
        Ok,
        Exit
    }
}
