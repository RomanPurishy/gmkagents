package com.kum.managers.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.kum.managers.R;
import com.kum.managers.store.Byer;
import com.kum.managers.store.Shop;
import com.kum.managers.store.ShopListAdapter;

// Окно выбора покупателя
public class ByerShopSelectorDialog extends DialogFragment {

    public Byer SelectedByer;  // выбранный покупатель

    public void setByer(Byer byer){
        SelectedByer = byer;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().setTitle("Выберите грузополучателя!");
        View v = inflater.inflate(R.layout.byershop_selector_layout, null);

        if(!SelectedByer.Shops.IsLoaded){
            SelectedByer.Shops.Load();
        }
        ListView lstShops = (ListView)v.findViewById(R.id.shopsList);
        ShopListAdapter adapter = new ShopListAdapter(v.getContext(), SelectedByer.Shops);
        try {
            lstShops.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        Button btnCancel = (Button)v.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        // выбираем покупателя
        lstShops.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Shop shop = SelectedByer.Shops.get(position);
                if(SelectedListener !=null){
                    SelectedListener.ItemSelected(shop);
                }
                view.setSelected(true);
                dismiss();
            }
        });

        return v;
    }

    private ISelected SelectedListener;
    public static interface ISelected{

        // Метод после выбора элемента
        public void ItemSelected(Shop shop);
    }

    public void setOnSelectedListener(ISelected selected){
        SelectedListener = selected;
    }
}
