package com.kum.managers.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kum.managers.ByersListAdapter;
import com.kum.managers.R;
import com.kum.managers.store.Byer;
import com.kum.managers.store.ByerCollection;

/**
 * Список покупателей
 */
public class ByerListFragment extends android.support.v4.app.Fragment {

    ByerCollection byers;
    private boolean loaded = false;
    View currentView;

    // после загрузки
    @Override
    public void onStart() {
        super.onStart();
        if(startListener !=null){
            startListener.OnStarted();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        currentView = inflater.inflate(R.layout.byer_list,null);
        return currentView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        loaded = true;
        LoadByers(false);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    //  Делегаты
    private OnStartListener startListener;

    public static interface OnStartListener{
        void OnStarted();
    }


    public void setOnByerSelectListener(OnByerSelectListener listener){
        byerSelectListener = listener;
    }
    private OnByerSelectListener byerSelectListener;
    public static interface OnByerSelectListener{
        void OnByerSelected(Byer byer);
    }

    // Загружаем покупателей
    public void LoadByers(boolean reload){
        if(!loaded) return;
        if(byers==null){
            byers = new ByerCollection(getActivity());
            byers.Load();
        }
        else{
            if(reload){
                byers.Load();
            }
        }

        // заполняем наш список
        ListView listView = (ListView)currentView.findViewById(R.id.lst_byers);
        ByersListAdapter adapter = new ByersListAdapter(getActivity(), byers);
        try {
            listView.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        //
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Byer byer  = byers.get(position);
                if(byerSelectListener !=null){
                    byerSelectListener.OnByerSelected(byer);
                }
                view.setSelected(true);
            }
        });
    }

}
