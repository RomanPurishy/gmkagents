package com.kum.managers.fragments;

import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.kum.managers.Common.DateFunctions;
import com.kum.managers.Common.Functions;
import com.kum.managers.Controls.BorderedTextView;
import com.kum.managers.R;
import com.kum.managers.store.Period;

import java.util.Calendar;
import java.util.Date;

/**
 * Диалог периода
 */
public class PeriodDialog extends DialogFragment implements  View.OnClickListener{

    private Date DateStart = new Date();
    private Date DateEnd = new Date();
    private LinearLayout grdMain;
    private LinearLayout freeLayout;
    private boolean freePeriod;
    private BorderedTextView edPeriodStart;
    private BorderedTextView edPeriodEnd;
    TextView title_period;
    LinearLayout toolbar;

    public PeriodDialog(){
        freePeriod = true;
    }

    // формирование окна выбора периода
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View v;

        double size = Functions.screenInches(getActivity());
        if(size<5){
            v = inflater.inflate(R.layout.period_phone, null);
        }
        else{
            v = inflater.inflate(R.layout.period, null);
        }

        grdMain = (LinearLayout)v.findViewById(R.id.buttons_content);
        freeLayout = (LinearLayout)v.findViewById(R.id.period_free);
        toolbar = (LinearLayout)v.findViewById(R.id.toolbar);
        edPeriodStart = (BorderedTextView)v.findViewById(R.id.edPeriodStart);
        edPeriodStart.setOnClickListener(this);
        edPeriodEnd =(BorderedTextView)v.findViewById(R.id.edPeriodEnd);
        edPeriodEnd.setOnClickListener(this);
        DateStart.setTime(Period.Start.getTime());
        DateEnd.setTime(Period.End.getTime());
        edPeriodStart.setText(android.text.format.DateFormat.format("dd/MM/yyyy", DateStart));
        edPeriodEnd.setText(android.text.format.DateFormat.format("dd/MM/yyyy", DateEnd));
        toolbar.setVisibility(View.GONE);
        title_period = (TextView)v.findViewById(R.id.title_period);
        title_period.setText(Period.getTitle());

        Button btnFree = (Button)v.findViewById(R.id.btn_free);
        Button btnCloseDialog = (Button)v.findViewById(R.id.btnCloseDialog);
        Button btnSetPeriod = (Button)v.findViewById(R.id.btnSetPeriod);
        btnFree.setOnClickListener(this);
        btnCloseDialog.setOnClickListener(this);
        btnSetPeriod.setOnClickListener(this);
        setbindToClick(v);
        return v;
    }

    private void setbindToClick(View v){
        Button btnMonth1 = (Button)v.findViewById(R.id.month_1);
        btnMonth1.setOnClickListener(this);
        Button btnMonth2 = (Button)v.findViewById(R.id.month_2);
        btnMonth2.setOnClickListener(this);
        Button btnMonth3 = (Button)v.findViewById(R.id.month_3);
        btnMonth3.setOnClickListener(this);
        Button btnMonth4 = (Button)v.findViewById(R.id.month_4);
        btnMonth4.setOnClickListener(this);
        Button btnMonth5 = (Button)v.findViewById(R.id.month_5);
        btnMonth5.setOnClickListener(this);
        Button btnMonth6 = (Button)v.findViewById(R.id.month_6);
        btnMonth6.setOnClickListener(this);
        Button btnMonth7 = (Button)v.findViewById(R.id.month_7);
        btnMonth7.setOnClickListener(this);
        Button btnMonth8 = (Button)v.findViewById(R.id.month_8);
        btnMonth8.setOnClickListener(this);
        Button btnMonth9 = (Button)v.findViewById(R.id.month_9);
        btnMonth9.setOnClickListener(this);
        Button btnMonth10 = (Button)v.findViewById(R.id.month_10);
        btnMonth10.setOnClickListener(this);
        Button btnMonth11 = (Button)v.findViewById(R.id.month_11);
        btnMonth11.setOnClickListener(this);
        Button btnMonth12 = (Button)v.findViewById(R.id.month_12);
        btnMonth12.setOnClickListener(this);
        Button period_kvartal_1 = (Button)v.findViewById(R.id.period_kvartal_1);
        if(period_kvartal_1 != null) period_kvartal_1.setOnClickListener(this);
        Button period_kvartal_2 = (Button)v.findViewById(R.id.period_kvartal_2);
        if(period_kvartal_2 != null) period_kvartal_2.setOnClickListener(this);
        Button period_kvartal_3 = (Button)v.findViewById(R.id.period_kvartal_3);
        if(period_kvartal_3 != null) period_kvartal_3.setOnClickListener(this);
        Button period_kvartal_4 = (Button)v.findViewById(R.id.period_kvartal_4);
        if(period_kvartal_4 != null)  period_kvartal_4.setOnClickListener(this);
        Button hafyear = (Button)v.findViewById(R.id.hafyear);
        if(hafyear !=null) hafyear.setOnClickListener(this);
        Button second_half = (Button)v.findViewById(R.id.second_half);
        if(second_half !=null) second_half.setOnClickListener(this);
        Button full_year = (Button)v.findViewById(R.id.full_year);
        if(full_year != null) full_year.setOnClickListener(this);
        Button today = (Button)v.findViewById(R.id.today);
        today.setOnClickListener(this);
        Button days30 = (Button)v.findViewById(R.id.days30);
        days30.setOnClickListener(this);
        Button days60 = (Button)v.findViewById(R.id.days60);
        days60.setOnClickListener(this);
        Button all_period = (Button)v.findViewById(R.id.all_period);
        all_period.setOnClickListener(this);
        Button nine_month = (Button)v.findViewById(R.id.nine_month);
        nine_month.setOnClickListener(this);
        Button btn_tommorow = (Button)v.findViewById(R.id.btn_tomorrow);
        btn_tommorow.setOnClickListener(this);

    }


    private void setDateStart(){
        Calendar c = Calendar.getInstance();
        c.setTime(Period.Start);
        final DatePickerDialog dateDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,monthOfYear,dayOfMonth);
                DateStart =Functions.getSimpleDate(calendar.getTime());
                if(DateStart.getTime()>DateEnd.getTime()){
                    DateEnd.setTime(DateStart.getTime());
                    edPeriodEnd.setText(android.text.format.DateFormat.format("dd/MM/yyyy", DateEnd));
                }
                edPeriodStart.setText(android.text.format.DateFormat.format("dd/MM/yyyy", DateStart));
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        dateDialog.show();
    }

    private void setDateEnd(){
        Calendar c = Calendar.getInstance();
        c.setTime(Period.End);
        final DatePickerDialog dateDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,monthOfYear,dayOfMonth);
                DateEnd = Functions.getSimpleDate(calendar.getTime());
                if(DateEnd.getTime() <DateStart.getTime()){
                    Functions.showMessage(getActivity(),R.layout.order,"Дата окончания не может больше даты начала", Functions.ToastMessageType.Info,3000);
                    Calendar c = Calendar.getInstance();
                    c.setTime(DateStart);
                    c.add(Calendar.DATE, 1);
                    DateEnd = Functions.getSimpleDate(c.getTime());
                }
                edPeriodEnd.setText(android.text.format.DateFormat.format("dd/MM/yyyy", DateEnd));
            }
        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        dateDialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_free:
                if(freePeriod){
                    grdMain.setVisibility(View.GONE);
                    freeLayout.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.VISIBLE);
                }
                else{
                    freeLayout.setVisibility(View.GONE);
                    grdMain.setVisibility(View.VISIBLE);
                    toolbar.setVisibility(View.GONE);
                }
                freePeriod = !freePeriod;
                break;
            case R.id.btnCloseDialog:
                dismiss();
                break;
            case R.id.edPeriodStart:
                setDateStart();
                break;
            case R.id.edPeriodEnd:
                setDateEnd();
                break;
            case R.id.btnSetPeriod:
                Period.Start = DateStart;
                Period.End = DateEnd;
                Period.ChangePeriod(DateStart,DateEnd);
                dismiss();
                break;
            case R.id.month_1:
                setNewPeriod(0);
                dismiss();
                break;
            case R.id.month_2:
                setNewPeriod(1);
                dismiss();
                break;
            case R.id.month_3:
                setNewPeriod(2);
                dismiss();
                break;
            case R.id.month_4:
                setNewPeriod(3);
                dismiss();
                break;
            case R.id.month_5:
                setNewPeriod(4);
                dismiss();
                break;
            case R.id.month_6:
                setNewPeriod(5);
                dismiss();
                break;
            case R.id.month_7:
                setNewPeriod(6);
                dismiss();
                break;
            case R.id.month_8:
                setNewPeriod(7);
                dismiss();
                break;
            case R.id.month_9:
                setNewPeriod(8);
                dismiss();
                break;
            case R.id.month_10:
                setNewPeriod(9);
                dismiss();
                break;
            case R.id.month_11:
                setNewPeriod(10);
                dismiss();
                break;
            case R.id.month_12:
                setNewPeriod(11);
                dismiss();
                break;
            case R.id.period_kvartal_1:
                int year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,0,1,0,0,0);
                Date dt = DateFunctions.getDate(year,2,1,0,0,0);
                int  lastDay = DateFunctions.getLastDayInMonth(dt);
                Period.End = DateFunctions.getDate(year,2,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.period_kvartal_2:
                year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,3,1,0,0,0);
                dt = DateFunctions.getDate(year,5,1,0,0,0);
                lastDay = DateFunctions.getLastDayInMonth(dt);
                Period.End = DateFunctions.getDate(year,5,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.period_kvartal_3:
                year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,6,1,0,0,0);
                dt = DateFunctions.getDate(year,8,1,0,0,0);
                lastDay = DateFunctions.getLastDayInMonth(dt);
                Period.End = DateFunctions.getDate(year,8,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.period_kvartal_4:
                year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,9,1,0,0,0);
                dt = DateFunctions.getDate(year,11,1,0,0,0);
                lastDay = DateFunctions.getLastDayInMonth(dt);
                Period.End = DateFunctions.getDate(year,11,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.hafyear:
                year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,0,1,0,0,0);
                dt = DateFunctions.getDate(year,5,1,0,0,0);
                lastDay = DateFunctions.getLastDayInMonth(dt);
                Period.End = DateFunctions.getDate(year,5,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.second_half:
                year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,6,1,0,0,0);
                dt = DateFunctions.getDate(year,11,1,0,0,0);
                lastDay = DateFunctions.getLastDayInMonth(dt);
                Period.End = DateFunctions.getDate(year,11,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.nine_month:
                Calendar cal = Calendar.getInstance();
                Period.End = DateFunctions.getCurrentShortDate();
                cal.setTime(Period.End );
                cal.add(Calendar.MONTH, -9);
                Period.Start = cal.getTime();
                dismiss();
                break;
            case R.id.full_year:
                year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
                Period.Start = DateFunctions.getDate(year,0,1,0,0,0);
                Date lastMonth = DateFunctions.getDate(year,11,1,0,0,0);
                lastDay = DateFunctions.getLastDayInMonth(lastMonth);
                Period.End = DateFunctions.getDate(year,11,lastDay,0,0,0);
                dismiss();
                break;
            case R.id.today:
                Period.Start = DateFunctions.getCurrentShortDate();
                Period.End = DateFunctions.getCurrentShortDate();
                dismiss();
                break;
            case R.id.days30:
                cal = Calendar.getInstance();
                Period.End = DateFunctions.getCurrentShortDate();
                cal.setTime(Period.End );
                cal.add(Calendar.DAY_OF_MONTH,-30);
                Period.Start = cal.getTime();
                dismiss();
                break;
            case R.id.days60:
                cal = Calendar.getInstance();
                Period.End = DateFunctions.getCurrentShortDate();
                cal.setTime(Period.End );
                cal.add(Calendar.DAY_OF_MONTH,-60);
                Period.Start = cal.getTime();
                dismiss();
                break;
            case R.id.all_period:
                Period.Start = DateFunctions.getDate(1900,0,1,0,0,0);
                Period.End = DateFunctions.getDate(4000, 0, 1, 0, 0, 0);
                dismiss();
                break;
            case R.id.btn_tomorrow:
                Date now = DateFunctions.getCurrentShortDate();
                Period.Start = DateFunctions.addDaysToShortDate(now,1);
                Period.End = Period.Start;
                dismiss();
                break;
        }
        title_period.setText(Period.getTitle());
        Period.ChangePeriod(Period.Start,Period.End);
    }

    private void setNewPeriod(int month){
        int year = DateFunctions.getYear(DateFunctions.getCurrentShortDate());
        Period.Start = DateFunctions.getDate(year,month,1,0,0,0);
        int lastDay = DateFunctions.getLastDayInMonth(Period.Start);
        Period.End = DateFunctions.getDate(year,month,lastDay,0,0,0);
    }



/*    DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;
            edPeriodStart.setText(myDay + "/" + myMonth + "/" + myYear);
        }
    };*/
}
