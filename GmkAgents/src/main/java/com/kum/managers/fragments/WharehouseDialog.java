package com.kum.managers.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.kum.managers.R;
import com.kum.managers.store.Wharehouse;
import com.kum.managers.store.WharehouseCollection;
import com.kum.managers.store.WharehousesAdapter;

/**
 * Created by Роман on 18.11.13.
 */
public class WharehouseDialog extends DialogFragment {

    WharehouseCollection wharehouses;

    // Коллекция складов
    public void setWarehouses(WharehouseCollection collection){
        wharehouses = collection;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().setTitle("Выберите склад!");
        View v = inflater.inflate(R.layout.wharehouse_selector_layout, null);


        ListView lstWharehouses = (ListView)v.findViewById(R.id.wharehouse_list);
        WharehousesAdapter adapter = new WharehousesAdapter(v.getContext(), wharehouses);
        try {
            lstWharehouses.setAdapter(adapter);
        } catch (Exception e) {
            Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        Button btnCancel = (Button)v.findViewById(R.id.btnCancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        // выбираем слад
        lstWharehouses.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Wharehouse wharehouse =wharehouses.get(position);
                if(SelectedListener !=null){
                    SelectedListener.ItemSelected(wharehouse);
                }
                view.setSelected(true);
                dismiss();
            }
        });

        return v;
    }



    private ISelected SelectedListener;
    public static interface ISelected{

        // Метод после выбора элемента
        public void ItemSelected(Wharehouse wharehouse);
    }

    public void setOnSelectedListener(ISelected selected){
        SelectedListener = selected;
    }

}
