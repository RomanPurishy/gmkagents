package com.kum.managers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kum.managers.store.Byer;
import com.kum.managers.store.ByerCollection;

// адаптер данных для отображения покупателей
public class ByersListAdapter extends BaseAdapter {


    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private ByerCollection byers;  // коллекция покупателей

    public ByersListAdapter(Context context,ByerCollection coll){
        super();
        byers = coll;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return byers.size();
    }

    @Override
    public Object getItem(int i) {
        return byers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView =inflater.inflate(R.layout.byer_view,null);
        Byer byer = byers.get(i);
        TextView txtName = (TextView)convertView.findViewById(R.id.byer_name);
        TextView txtShopsCount = (TextView)convertView.findViewById(R.id.ShopsCount);
        if(byer.ShopsCount>0){
            txtShopsCount.setText(byer.ShopsCount+"");
        }
        else{
            txtShopsCount.setVisibility(View.INVISIBLE);
        }
        txtName.setText(byer.Name);
        return convertView;
    }
}
