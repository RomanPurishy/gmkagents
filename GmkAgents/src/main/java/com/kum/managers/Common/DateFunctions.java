package com.kum.managers.Common;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Роман on 26.12.13.
 */
public class DateFunctions {

    public static Date getCurrentShortDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getDate(int Year,int month,int day,int hour,int minutes,int second){
        Calendar cal = Calendar.getInstance();
        cal.set(Year,month,day,hour,minutes,second);
        return cal.getTime();
    }

    public static int getYear(Date  date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static int getMonth(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static int getDay(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    // функция возвращает последний день в месяце
    public static int getLastDayInMonth(Date date){
        Calendar cal = Calendar.getInstance();
        int year = getYear(date);
        int month = getMonth(date);
        cal.set(year,month,1);
        cal.add(Calendar.MONTH, 1);
        cal.add(Calendar.DAY_OF_MONTH,-1);
        return getDay(cal.getTime());
    }

    /**
     *  Метод добавляет дни к текущей дате
     *
     * @param date дата к которой будут добавлены дни
     * @param days количество добавляемых дней
     */
    public static Date addDaysToShortDate(Date date,int days){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

}
