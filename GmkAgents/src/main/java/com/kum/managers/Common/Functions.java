package com.kum.managers.Common;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kum.managers.R;
import com.kum.managers.store.OrderStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by Роман on 21.11.13.
 */
public class Functions {

    public static String Domen = "http://91.237.3.15:8099/";

    public static double Round(double number, int scale) {
        int pow = 10;
        for (int i = 1; i < scale; i++)
            pow *= 10;
        double tmp = number * pow;
        return (double) (int) ((tmp - (int) tmp) >= 0.5d ? tmp + 1 : tmp) / pow;
    }

    // форматирование денежного значения
    public static String Format(double val){
        return String.format("%,.2f", val);
    }

    // Метод возвращает строковое представление даты для записи в БД
    public static String getDateForSql(Date dt){
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        String month = (cal.get(Calendar.MONTH)+1)+"";
        if(month.length()==1){
            month="0"+month;
        }
        String days = cal.get(Calendar.DAY_OF_MONTH)+"";
        if(days.length()==1){
            days = "0"+days;
        }
        return cal.get(Calendar.YEAR)+"-"+month+"-"+days+ " 00:00:00.000";
    }

    public static String getDateShortString(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String month = (cal.get(Calendar.MONTH)+1)+"";
        if(month.length()==1){
            month="0"+month;
        }
        String days = cal.get(Calendar.DAY_OF_MONTH)+"";
        if(days.length()==1){
            days = "0"+days;
        }
        return days+"/"+month+"/"+cal.get(Calendar.YEAR);
    }

    // Метод возвращает длинное строковое представление даты
    public static String getDateLongForSql(Date dt){
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        String month = (cal.get(Calendar.MONTH)+1)+"";
        if(month.length()==1){
            month="0"+month;
        }
        String days = cal.get(Calendar.DAY_OF_MONTH)+"";
        if(days.length()==1){
            days = "0"+days;
        }
        String hour = cal.get(Calendar.HOUR_OF_DAY)+"";
        if(hour.length()==1){
            hour = "0"+hour;
        }
        String minutes =cal.get(Calendar.MINUTE)+"";
        if(minutes.length()==1){
            minutes="0"+minutes;
        }
        String seconds = cal.get(Calendar.SECOND)+"";
        if(seconds.length()==1){
            seconds = "0"+seconds;
        }
        return cal.get(Calendar.YEAR)+"-"+month+"-"+days+" "+hour+":"+minutes+":"+seconds+ "."+cal.get(Calendar.MILLISECOND);
    }

    // Метод возвращает целое предтавление значения статуса заявки
    public static int getOrderStatus(OrderStatus status){
        switch (status){
            case New: return 0;
            case Sended: return 1;
            case Drufted:return 2;
            case Deleted:return 3;
            case Executed:return 4;
            default: return 0;
        }
    }

    // Возвращает перечисление статуса заявки
    public static OrderStatus getOrderStatus(int status){
        switch (status){
            case 0: return OrderStatus.New;
            case 1: return OrderStatus.Sended;
            case 2: return OrderStatus.Drufted;
            case 3: return OrderStatus.Deleted;
            case 4:return OrderStatus.Executed;
            default: return OrderStatus.New;
        }
    }

    public static  Date stringToDate(String aDate) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(aDate);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return convertedDate;

    }

    // функция возвращает размер экрана в дюймах
    public static double screenInches(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        double x = Math.pow(dm.widthPixels/dm.xdpi,2);
        double y = Math.pow(dm.heightPixels/dm.ydpi,2);
        return Math.sqrt(x+y);
    }

    // Функция возвращает разрещение экрана
    public static Point screenSize(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }


    public static void showMessage(Context c, String text, int duration) {
        Toast.makeText(c, text, duration).show();
    }

    public static void showMessage(Context c, int id, int duration) {
        Toast.makeText(c, id, duration).show();
    }

    public static void showMessage(Context c, String text, int duration, int gravity) {
        Toast t = Toast.makeText(c, text, duration);
        t.setGravity(gravity, 0, 0);
        t.show();
    }

    public static void showErrorMessage(Context c,String className,String methodName,String error){
        Toast t = Toast.makeText(c, "В классе ["+className+"] метода=["+methodName+"] \n Ошибка:\n"+error, 10000);
        t.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        t.show();
    }

    public static void showMessage(Context c, String text, int duration, int gravity, int[] offset) {
        Toast t = Toast.makeText(c, text, duration);
        t.setGravity(gravity, offset[0], offset[1]);
        t.show();
    }

    public static boolean IsOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }

    // context - контекст
    // activityId - идентификатор Layout активити
    // message - сообщение
    // type - тип сообщения
    // duration - время в милисикундах на задержку
    public static void showErrorMessage(Context context,int activityId,String className,String methodName,String error,int duration){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View v =inflater.inflate(activityId,null);
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)v.findViewById(R.id.toast_layout_root));

        TextView text = (TextView)layout.findViewById(R.id.text);
        ImageView img = (ImageView)layout.findViewById(R.id.toast_ico);

        img.setImageResource(R.drawable.error48);
        layout.setBackgroundResource(R.drawable.alert_error);

        text.setText("В классе ["+className+"] метода=["+methodName+"] \n Ошибка:\n"+error);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();
    }

    // Функция показывает окно сообщения
    // context - контекст
    // activityId - идентификатор Layout активити
    // message - сообщение
    // type - тип сообщения
    // duration - время в милисикундах на задержку
    public static void showMessage(Context context,int activityId,String message,ToastMessageType type,int duration){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View v =inflater.inflate(activityId,null);
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)v.findViewById(R.id.toast_layout_root));

        TextView text = (TextView)layout.findViewById(R.id.text);
        ImageView img = (ImageView)layout.findViewById(R.id.toast_ico);

        switch (type){
            case Error:
                img.setImageResource(R.drawable.error48);
                layout.setBackgroundResource(R.drawable.alert_error);
                break;
            case Info:
                img.setImageResource(R.drawable.info48);
                layout.setBackgroundResource(R.drawable.alert_info);
                break;
            case Success:
                img.setImageResource(R.drawable.ok48);
                layout.setBackgroundResource(R.drawable.alert_success);
                break;
            case Question:
                img.setImageResource(R.drawable.question48);
                layout.setBackgroundResource(R.drawable.alert_question);
                break;
        }
        text.setText(message);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();
    }

       // фунция возвращает простую дату
    public static Date getSimpleDate(Date longDate){
        Calendar c = Calendar.getInstance();
        c.setTime(longDate);
        Calendar newDate = Calendar.getInstance();
        newDate.set(c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH));
        return newDate.getTime();
    }

    /**
    * функция возвращает текущую дату без времени
    */
    public static Date getShortDate(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     *  Метод добавляет дни к текущей дате
     *
     * @param date дата к которой будут добавлены дни
     * @param days количество добавляемых дней
     */
    public static Date addDaysToShortDate(Date date,int days){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }


    public enum ToastMessageType{
        Error,
        Info,
        Success,
        Question
    }
}
