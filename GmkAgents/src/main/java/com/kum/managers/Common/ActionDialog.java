package com.kum.managers.Common;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kum.managers.Interfaces.IDialogResult;
import com.kum.managers.R;

public class ActionDialog extends android.support.v4.app.DialogFragment  implements OnClickListener {

    private IDialogResult _resultView;
    private String _title;
    private String _message;
    private int _resultCode;

    public ActionDialog(){
        _title = "";
        _message = "";
    }

    public void setParams(String title,String msg,IDialogResult view,int code){
        _resultView = view;
        _title = title;
        _message =msg;
        _resultCode = code;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        getDialog().setTitle(_title);
        View v = inflater.inflate(R.layout.action_dialog, null);
        TextView txtMessage = (TextView)v.findViewById(R.id.txt_message);
        txtMessage.setText(_message);
        v.findViewById(R.id.btnYes).setOnClickListener(this);
        v.findViewById(R.id.btnNo).setOnClickListener(this);
        return v;
    }

    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnYes:
                if (_resultView != null) {
                    _resultView.DialogClose(DialogResult.Ok, _resultCode);
                }
                break;
            case R.id.btnCancel:
                if (_resultView != null) {
                    _resultView.DialogClose(DialogResult.Cancel, _resultCode);
                }
                break;
        }
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

}