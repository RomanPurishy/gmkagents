package com.kum.managers.Common;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import com.kum.managers.R;
import com.kum.managers.store.Workarea;

/**
 * Файл настройки диалога
 */
public class ConfigsDialog extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        getDialog().setTitle("Настройки приложения");
        View v = inflater.inflate(R.layout.config_dialog,null);

        Button btnClose = (Button)v.findViewById(R.id.btnNo);
        if(btnClose != null){
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

        // выбираем домен 1
        RadioButton rdDomen1 = (RadioButton)v.findViewById(R.id.rdDomen1);
        if(rdDomen1 !=null){
            if(Functions.Domen == Workarea.DomenUrl_1){
                rdDomen1.setChecked(true);
            }
            rdDomen1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.Domen = Workarea.DomenUrl_1;
                }
            });
        }

        // выбираем домен 2
        RadioButton rdDomen2 = (RadioButton)v.findViewById(R.id.rdDomen2);
        if(rdDomen2 !=null){
            if(Functions.Domen == Workarea.DomenUrl_2){
                rdDomen2.setChecked(true);
            }
            rdDomen2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.Domen = Workarea.DomenUrl_2;
                }
            });
        }

        return v;
    }

}
