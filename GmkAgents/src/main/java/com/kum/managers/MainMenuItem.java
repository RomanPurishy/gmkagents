package com.kum.managers;

/**
 *  Элемент главного меню
 */
public class MainMenuItem {
    public java.lang.CharSequence title;
    public int iconRes;
    public int counter;
    public boolean isHeader;
    public int id;  // идентификатор меню

    public MainMenuItem(java.lang.CharSequence title, int iconRes,boolean header,int counter,int id) {
        this.title = title;
        this.iconRes = iconRes;
        this.isHeader=header;
        this.counter=counter;
        this.id = id;
    }

    public MainMenuItem(java.lang.CharSequence title, int iconRes,boolean header,int id){
        this(title,iconRes,header,0,id);
    }

    public MainMenuItem(java.lang.CharSequence title, int iconRes,int id) {
        this(title,iconRes,false,id);
    }
}
