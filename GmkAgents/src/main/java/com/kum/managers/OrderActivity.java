package com.kum.managers;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.kum.managers.Common.ActionDialog;
import com.kum.managers.Common.DialogResult;
import com.kum.managers.Common.Functions;
import com.kum.managers.Controls.Border;
import com.kum.managers.Controls.BorderedTextView;
import com.kum.managers.Interfaces.IAsynkTask;
import com.kum.managers.Interfaces.IDialogResult;
import com.kum.managers.fragments.ByerSelectorDialog;
import com.kum.managers.fragments.ByerShopSelectorDialog;
import com.kum.managers.fragments.CalculatorDialog;
import com.kum.managers.fragments.GoodSelectorDialog;
import com.kum.managers.fragments.WharehouseDialog;
import com.kum.managers.logs.LogErrorDialog;
import com.kum.managers.logs.LogErrorItem;
import com.kum.managers.logs.LogErrorItemCollection;
import com.kum.managers.logs.LogErrorItemTypeEnums;
import com.kum.managers.store.Byer;
import com.kum.managers.store.ByerCollection;
import com.kum.managers.store.Good;
import com.kum.managers.store.GoodCollection;
import com.kum.managers.store.Order;
import com.kum.managers.store.OrderCollection;
import com.kum.managers.store.OrderStatus;
import com.kum.managers.store.Shop;
import com.kum.managers.store.TransListAdapter;
import com.kum.managers.store.Transaction;
import com.kum.managers.store.Wharehouse;
import com.kum.managers.store.WharehouseCollection;
import com.kum.managers.store.Workarea;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by PURISHI on 05.11.13.
 */
public class OrderActivity extends ActionBarActivity  implements IDialogResult {

    private Order order;  // заявка (новая или старая)
    private WharehouseCollection wharehouses;
    private ByerCollection byers;           // коллекция покупателей
    private DrawerLayout mDrawerLayout;
    private ListView mMenuLayout;       // контейнер меню
    private CustomActionBarDrawerToggle mDrawerToggle;
    private MainMenuAdapter mAdapter;
    private GoodCollection goods;       // коллекция товаров
    private Button btnDeleteTrans;      // удалить проводку
    final int GOOD_SELECTOR=101;
    private int ORDER_ACTIVITY=100;
    private Transaction SelectedTransaction; // выбранная проводка
    private CalculatorDialog calculatorDialog;
    private Button btnEditTrans;
    public View SelectedTransItemView;
    private int TransactionPosition;    // позиция выбраной проводки
    private AdapterView TransListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        double size = Functions.screenInches(getApplicationContext());
        if(size<5){
            setContentView(R.layout.order_phone);
        }
        else{
            setContentView(R.layout.order);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.order_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new CustomActionBarDrawerToggle(this, mDrawerLayout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        order = (Order)getIntent().getSerializableExtra("order");
        SetOrderNumber();  // установка номера заявки
        SetOrderDate();         // установка даты заявки
        setFiscal();        // установка фискальности заявки
        setByerAttr();       // установка атрибутов покупателя
        setWareHouses();            // установка складов
        InitMenu();  // инициализация меню
        addGood();  // добавляем товары
        // если ID заявки равно нулю то сохраняем ее
        if(order.ID==0){
            if(!order.Save(getApplicationContext())){
                Functions.showMessage(getApplicationContext(),R.layout.order,"ОШИБКА в сохранении заявки", Functions.ToastMessageType.Error,5000);
            }
        }
        setTransList(); // загрузка списка проводок
        setDeleteTransactionButton();
        setOrderAttribute(); // установка атрибутов заказа
        EditTransaction();  // редактируем проводку
        SetCalculator();        // калькулятор
      //  setGoods(); // товары
    }

    // Кнопка удаляет товар
    private void setDeleteTransactionButton(){
        btnDeleteTrans = (Button)findViewById(R.id.btnDeleteTrans);
        btnDeleteTrans.setVisibility(View.INVISIBLE);
        btnDeleteTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActionDialog dialog = new ActionDialog();
                dialog.setParams("Удаление проводки", "Удалить товар "+SelectedTransaction.Good.Name+" из заказа?.",OrderActivity.this,200);
                dialog.show(getSupportFragmentManager(),"deleteOrderDialog");
            }
        });
    }

    // добавляем товар
    private void addGood(){
        Button btnAddGood = (Button)findViewById(R.id.btnAddGood);
        if(order.Status== OrderStatus.Sended || order.Status ==  OrderStatus.Executed){
            btnAddGood.setEnabled(false);
        }
        btnAddGood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(goods==null){
                    goods = new GoodCollection(getApplicationContext());
                }

                goods.LoadPopular(getApplicationContext(),order.WhareHouse.ID,order.Byer.ID,order.Shop.ID,order.ID);
                Bundle bndle = new Bundle();
                bndle.putSerializable("ORDER",order);
                bndle.putSerializable("GOODS",goods);
                GoodSelectorDialog dlg = new GoodSelectorDialog();
                dlg.setArguments(bndle);
                dlg.setDialogFragmentListener(new GoodSelectorDialog.IDialogFragment() {
                    @Override
                    public void Closed() {
                        setTransList();
                    }
                });
                dlg.show(getFragmentManager(),"goodsSelectorDialog");
            }
        });

    }

    // Инициализация меню
    private void InitMenu() {
        mAdapter = new MainMenuAdapter(this);

        mAdapter.addHeader(getString(R.string.main_menu_header2));
        if(order.Status==OrderStatus.New ){
            // отправляем на сбыт
            MainMenuItem mnu =  new MainMenuItem(getString(R.string.order_menu_send),R.drawable.order_sended64,1);
            mAdapter.addItem(mnu);
            // в корзину
            mnu =  new MainMenuItem(getString(R.string.order_menu_trush),R.drawable.order_deleted64,3);
            mAdapter.addItem(mnu);
        }

/*        if(order.Status !=OrderStatus.Drufted){
            // помещаем в черновик
            mnu =  new MainMenuItem(getString(R.string.order_menu_drufted),R.drawable.order_drufted64,2);
            mAdapter.addItem(mnu);
        }*/

        mMenuLayout = (ListView)findViewById(R.id.order_menu);
        if (mMenuLayout != null)
            mMenuLayout.setAdapter(mAdapter);

        mMenuLayout.setOnItemClickListener(new DrawerItemClickListener());

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.order_menu, menu);
        // Set up ShareActionProvider's default share intent
/*        MenuItem shareItem = menu.findItem(R.id.btn_order_close);*/
/*        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultIntent());*/

        return super.onCreateOptionsMenu(menu);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        // Выбираем пункт из меню
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mMenuLayout.setItemChecked(position, true);
            MainMenuItem selected = mAdapter.getItem(position);
            switch (selected.id){
                case 1: // отправляем на сбыт
                    if(Functions.IsOnline(OrderActivity.this)){
                        if(order.TransList.size()>0){
                            // проверка даты отправки
                            Date dateNow = Functions.getShortDate();
                            Date orderDate = Functions.addDaysToShortDate(dateNow,1);
                            if(order.OnDate.getTime()<orderDate.getTime()){
                                Functions.showMessage(getApplicationContext(),R.layout.order,
                                        "Дата заявки не может быть меньше чем ["+Functions.getDateShortString(orderDate)+"]", Functions.ToastMessageType.Error,5000);
                            }
                            else{
                                // вначале проверка отправки
                                CheckCanSendOrderOnSbit checkForSend = new CheckCanSendOrderOnSbit();
                                checkForSend.setOnCompleteListener(new IAsynkTask() {
                                    @Override
                                    public void TaskComplete() {
                                        OrderSendToSbit action= new OrderSendToSbit();
                                        action.execute();
                                    }

                                    @Override
                                    public void TaskError() {

                                    }
                                });
                                checkForSend.execute();
                            }
                        }
                        else{
                            Functions.showMessage(getApplicationContext(),R.layout.order,
                                    "Список товаров в заказе пуст. Добавьте вначале товары", Functions.ToastMessageType.Error,5000);
                        }
                    }
                    else{
                        Functions.showMessage(OrderActivity.this,"СЕТИ НЕ ДОСТУПНЫ",5000);
                    }
                    break;
                case 3:  // отправить в корзину
                    ActionDialog dialog = new ActionDialog();
                    dialog.setParams("Удаление заявки", "Удалить выбраную заявку. Внимание после удаления заявку нельзя будет восстановить.",OrderActivity.this,100);
                    dialog.show(getSupportFragmentManager(),"deleteOrderDialog");
                    break;
                default:

                    break;
            }
            mDrawerLayout.closeDrawer(mMenuLayout);
        }
    }

    // редактируем проводку
    private void EditTransaction(){
        btnEditTrans = (Button)findViewById(R.id.btn_edit_trans);
        btnEditTrans.setVisibility(View.INVISIBLE);
        btnEditTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SelectedTransaction != null){
                    Good gd = new Good(getApplicationContext());
                    gd.Fqty = SelectedTransaction.Fqty;
                    gd.Qty = SelectedTransaction.Qty;
                    gd.Price = SelectedTransaction.Price;
                    gd.Sum = SelectedTransaction.Sum;
                    gd.ID = SelectedTransaction.Good.ID;
                    gd.Name = SelectedTransaction.Good.Name;
                    calculatorDialog.setData(order,gd);
                    calculatorDialog.CanTransactionDelete = false;
                    calculatorDialog.show(getFragmentManager(),"calcTrans");
                }
            }
        });
    }

    // Открываем калькулятор и добавляем проводку в список
    private void SetCalculator(){
        calculatorDialog = new CalculatorDialog();
        calculatorDialog.setOnCloseDialogListener(new CalculatorDialog.IDialogClose() {
            @Override
            public void Closed(CalculatorDialog.ButtonType type) {
                if(type == CalculatorDialog.ButtonType.Ok){
                    ListView tranlist = (ListView)findViewById(R.id.tranlist);
                    TransListAdapter adpt= (TransListAdapter)tranlist.getAdapter();
                    SelectedTransaction.Qty = calculatorDialog.good.Qty;
                    SelectedTransaction.Price = calculatorDialog.good.Price;
                    SelectedTransaction.Sum = calculatorDialog.good.Sum;
                    SelectedTransaction.Fqty = calculatorDialog.good.Fqty;

                    order.TransList.Recalc();
                    SelectedTransaction.Save(getApplicationContext());
                    order.Save(getApplicationContext());
                    setTotals();
                    tranlist.requestFocus();
                    adpt.UpdateRow(SelectedTransItemView, SelectedTransaction);
                    TransListAdapter.requestFocusFromTouch();
                    TransListAdapter.setSelection(TransactionPosition);
                }
            }
        });
    }

    // установка номера заявки
    private void SetOrderNumber(){
        // номер
        BorderedTextView txtNumber = (BorderedTextView)findViewById(R.id.od_number);
        txtNumber.setText(order.Number);

        Border brdLeft = new Border(txtNumber.BORDER_LEFT);
        brdLeft.setColor(Color.BLACK);
        brdLeft.setWidth(2);
        Border brdBottom = new Border(txtNumber.BORDER_BOTTOM);
        brdBottom.setColor(Color.BLACK);
        brdBottom.setWidth(2);
        Border brdRight = new Border(txtNumber.BORDER_RIGHT);
        brdRight.setColor(Color.BLACK);
        brdRight.setWidth(2);
        Border brdTop = new Border(txtNumber.BORDER_TOP);
        brdTop.setColor(Color.BLACK);
        brdTop.setWidth(2);
        txtNumber.setBorders(new Border[]{brdBottom,brdLeft,brdRight,brdTop});
    }

    // устанавливаем дату
    private void SetOrderDate(){
        final BorderedTextView txtDate= (BorderedTextView)findViewById(R.id.od_date);
        txtDate.setText(android.text.format.DateFormat.format("dd/MM/yyyy", order.OnDate));

        Border brdLeft = new Border(txtDate.BORDER_LEFT);
        brdLeft.setColor(Color.BLACK);
        brdLeft.setWidth(2);
        Border brdBottom = new Border(txtDate.BORDER_BOTTOM);
        brdBottom.setColor(Color.BLACK);
        brdBottom.setWidth(2);
        Border brdRight = new Border(txtDate.BORDER_RIGHT);
        brdRight.setColor(Color.BLACK);
        brdRight.setWidth(2);
        Border brdTop = new Border(txtDate.BORDER_TOP);
        brdTop.setColor(Color.BLACK);
        brdTop.setWidth(2);
        txtDate.setBorders(new Border[]{brdBottom,brdLeft,brdRight,brdTop});
        txtDate.setClickable(true);
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(order.Status ==OrderStatus.New || order.Status == OrderStatus.Drufted){
                    Calendar c = Calendar.getInstance();
                    c.setTime(order.OnDate);
                    final DatePickerDialog dateDialog = new DatePickerDialog(OrderActivity.this, new DatePickerDialog.OnDateSetListener() {
                        public void onDateSet(final DatePicker view, final int year, final int monthOfYear, final int dayOfMonth) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(year,monthOfYear,dayOfMonth);
                            Date selected = Functions.getSimpleDate(calendar.getTime());
                            // проверка даты отправки
                            Date dateNow = Functions.getShortDate();
                            Date orderDate = Functions.addDaysToShortDate(dateNow,1);
                            if(selected.getTime()<orderDate.getTime()){
                                Functions.showMessage(getApplicationContext(),R.layout.order,
                                        "Дата заявки не может быть меньше чем ["+Functions.getDateShortString(orderDate)+"]", Functions.ToastMessageType.Error,5000);
                                orderDate.setTime(orderDate.getTime());
                                order.OnDate =orderDate;
                            }
                            else{
                                order.OnDate =  selected;
                            }
                            txtDate.setText(Functions.getDateShortString(order.OnDate));
                        }
                    }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                    dateDialog.show();
                }

            }
        });
    }

    // установка фискальная или нет заявка
    private void setFiscal(){
        final TextView txtFiscal = (TextView)findViewById(R.id.txt_order_fiscal);
        final   LinearLayout layout = (LinearLayout)findViewById(R.id.fiscal_layout);


        if(order.Fiscal){
            txtFiscal.setText(getResources().getString(R.string.order_fiscal));
            layout.setBackgroundColor(getResources().getColor(R.color.order_fiscal));
        }
        else{
            txtFiscal.setText(getResources().getString(R.string.order_official));
            layout.setBackgroundColor(getResources().getColor(R.color.order_oficial));
        }

        txtFiscal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(order.Status ==OrderStatus.New || order.Status == OrderStatus.Drufted){
                    if(order.Fiscal ){
                        order.Fiscal = false;
                        txtFiscal.setText(getResources().getString(R.string.order_official));
                        layout.setBackgroundColor(getResources().getColor(R.color.order_oficial));
                    }
                    else{
                        txtFiscal.setText(getResources().getString(R.string.order_fiscal));
                        layout.setBackgroundColor(getResources().getColor(R.color.order_fiscal));
                        order.Fiscal = true;
                    }
                }

            }
        });
    }

    // Установка атрибутов покупателя и грузополучателя
    private void setByerAttr(){
        final BorderedTextView txtByer = (BorderedTextView)findViewById(R.id.edByer);
        final BorderedTextView txtByerShop = (BorderedTextView)findViewById(R.id.edByerShop);

        if(byers==null){
            byers = new ByerCollection(getApplicationContext());
        }
        if(!byers.IsLoaded){
            byers.Load();
        }
        if(order.Byer.ID==0){
            if(byers.size()>0){
                Byer br = byers.get(0);
                order.Byer.ID = br.ID;
                order.Byer.Name = br.Name;
                txtByer.setText(br.Name);
                if(!br.Shops.IsLoaded){
                    br.Shops.Load();
                    if(br.Shops.size()>0){
                        Shop sh = br.Shops.get(0);
                        order.Shop.ID = sh.ID;
                        order.Shop.Name = sh.Name;
                        txtByerShop.setText(sh.Name);
                    }
                    else{
                        txtByerShop.setText(br.Name);
                    }
                }
            }
        }
        else{
            txtByer.setText(order.Byer.Name);
            if(order.Shop.ID==0){
                Byer br = new Byer(getApplicationContext(),order.Byer.ID);
                br.Load();
                br.Shops.Load();
                if(br.Shops.size()>0){
                    Shop sh = br.Shops.get(0);
                    order.Shop.ID = sh.ID;
                    order.Shop.Name = sh.Name;
                    txtByerShop.setText(sh.Name);
                }
                else{
                    txtByerShop.setText(br.Name);
                }
            }
            else{
                txtByerShop.setText(order.Shop.Name);
            }
        }

        Border brdBottom = new Border(txtByer.BORDER_BOTTOM);
        brdBottom.setColor(Color.BLACK);
        brdBottom.setWidth(2);

        txtByer.setBorders(new Border[]{brdBottom});
        txtByerShop.setBorders(new Border[]{brdBottom});

        txtByer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ByerSelectorDialog dlg = new ByerSelectorDialog();

                dlg.setByers(byers);
                dlg.show(getFragmentManager(),"byerDialog");
                dlg.setOnSelectedListener(new ByerSelectorDialog.ISelected() {
                    @Override
                    public void ItemSelected(Byer byer) {
                        order.Byer.ID = byer.ID;
                        order.Byer.Name = byer.Name;
                        txtByer.setText(byer.Name);

                        if(!byer.Shops.IsLoaded){
                            byer.Shops.Load();
                        }

                        if(byer.Shops.size()>0){
                            Shop shop = byer.Shops.get(0);
                            txtByerShop.setText(shop.Name);
                            order.Shop.ID = shop.ID;
                            order.Shop.Name = shop.Name;
                        }
                        else{
                            txtByerShop.setText(byer.Name);
                            order.Shop.ID = 0;
                            order.Shop.Name = "";
                        }
                    }
                });
            }
        });

        txtByerShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ByerShopSelectorDialog dlg = new ByerShopSelectorDialog();
                Byer byer = new Byer(getApplicationContext(),order.Byer.ID);
                byer.Load();
                dlg.SelectedByer =byer;
                dlg.show(getFragmentManager(),"shopDialog");
                dlg.setOnSelectedListener(new ByerShopSelectorDialog.ISelected() {
                    @Override
                    public void ItemSelected(Shop shop) {
                        order.Shop.ID = shop.ID;
                        order.Shop.Name = shop.Name;
                        txtByerShop.setText(shop.Name);
                    }
                });
            }
        });

        if(order.Status== OrderStatus.Sended || order.Status ==  OrderStatus.Executed){
            txtByer.setClickable(false);
            txtByerShop.setClickable(false);
        }
    }

    // Установка складов
    private void setWareHouses(){
        final BorderedTextView selWharehouse = (BorderedTextView)findViewById(R.id.selector_whareHouse);
        Border brdBottom = new Border(selWharehouse.BORDER_BOTTOM);
        brdBottom.setColor(Color.BLACK);
        brdBottom.setWidth(2);

        if(wharehouses == null){
            wharehouses = new WharehouseCollection(getApplicationContext());
        }
        if(!wharehouses.IsLoaded){
            wharehouses.Load();
        }

        if(order.WhareHouse.ID==0){
            Wharehouse defWh = wharehouses.GetDefault();
            if(defWh !=null){
                order.WhareHouse.ID = defWh.ID;
                order.WhareHouse.Name = defWh.Name;
                selWharehouse.setText(defWh.Name);
            }
        }
        else{
            selWharehouse.setText(order.WhareHouse.Name);
        }

        selWharehouse.setBorders(new Border[]{brdBottom});
        selWharehouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(order.TransList.size()==0){
                    WharehouseDialog dlg = new WharehouseDialog();
                    dlg.setWarehouses(wharehouses);

                    dlg.show(getFragmentManager(),"wharehouseDialog");
                    dlg.setOnSelectedListener(new WharehouseDialog.ISelected() {
                        @Override
                        public void ItemSelected(Wharehouse wharehouse) {
                            order.WhareHouse.ID = wharehouse.ID;
                            order.WhareHouse.Name = wharehouse.Name;
                            selWharehouse.setText(wharehouse.Name);
                        }
                    });
                    Functions.showMessage(getApplicationContext(),"Товары есть можно выбирать склад",300, Gravity.TOP);
                }
                else{
                    Functions.showMessage(getApplicationContext(),"Нельзя добавить склад если выбраны товары",300, Gravity.TOP);
                }
            }
        });
        if(order.Status== OrderStatus.Sended || order.Status ==  OrderStatus.Executed){
            selWharehouse.setClickable(false);
        }
    }

    // Загрузка списка проводок
    private void setTransList(){
        ListView transView = (ListView)findViewById(R.id.tranlist);
        TransListAdapter transListAdapter = new TransListAdapter(getApplicationContext(),order);
        transView.setAdapter(transListAdapter);

       // Log.println(7, "OrderActivity", "setTransList");

        transView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TransListAdapter = parent;
                TransactionPosition = position;
                SelectedTransaction = order.TransList.get(position);
                SelectedTransItemView = view;

                if(SelectedTransaction != null){
                    if(order.Status== OrderStatus.New || order.Status ==  OrderStatus.Drufted){
                        btnDeleteTrans.setVisibility(View.VISIBLE);
                        btnEditTrans.setVisibility(View.VISIBLE);
                    }
                    else{
                        btnDeleteTrans.setVisibility(View.INVISIBLE);
                        btnEditTrans.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        setTotals();
    }

    // Установка атрибутов заказа
    private void setOrderAttribute(){
        TextView  txtMemo = (TextView)findViewById(R.id.txtMemo);
        if(order.Status== OrderStatus.Sended || order.Status ==  OrderStatus.Executed){
            txtMemo.setEnabled(false);
        }
        txtMemo.setText(order.Memo);
        ImageView status = (ImageView)findViewById(R.id.order_status);
        switch (order.Status){
            case New:
                status.setImageResource(R.drawable.order_new48);
                break;
            case Sended:
                status.setImageResource(R.drawable.order_sended48);
                break;
            case Drufted:
                status.setImageResource(R.drawable.order_drafted48);
                break;
            case Deleted:
                status.setImageResource(R.drawable.order_deleted48);
                break;
            case Executed:
                status.setImageResource(R.drawable.order_executed48);
                break;
        }
    }

    // установка итогов
    private void setTotals(){
        TextView txtQty = (TextView)findViewById(R.id.order_qty);
        txtQty.setText(Functions.Format(order.TransList.Qty));
        TextView txtFqty = (TextView)findViewById(R.id.order_fqty);
        txtFqty.setText(Functions.Format(order.TransList.Fqty));
        TextView txtSum = (TextView)findViewById(R.id.order_sum);
        txtSum.setText(Functions.Format(order.TransList.Sum));
    }

    private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle {

        public CustomActionBarDrawerToggle(Activity mActivity,DrawerLayout mDrawerLayout){
            super(
                    mActivity,
                    mDrawerLayout,
                    R.drawable.ic_drawer,
                    R.string.ns_menu_open,
                    R.string.ns_menu_close);
        }

        @Override
        public void onDrawerClosed(View view) {
            invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
        }
    }

    // ВЕРХНИЙ ТУЛБАР
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.btn_order_close:
                closeActivity();
                setResult(ORDER_ACTIVITY);
                super.onBackPressed();
                return true;
            case R.id.action_backup:  // создаем бекап бд
               // BackUpDataBase();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mMenuLayout);
     //   menu.findItem(R.id.action_refresh).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    // Сохраняем активность при закрытии
    private void closeActivity(){
        // сохраняем заявку
        if(order.Status==OrderStatus.New || order.Status==OrderStatus.Drufted){
            TextView txtMemo = (TextView)findViewById(R.id.txtMemo);
            order.Memo = txtMemo.getText().toString();
            order.Save(getApplicationContext());
        }
    }

    @Override
    public void onBackPressed() {
        closeActivity();
        setResult(ORDER_ACTIVITY);
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GOOD_SELECTOR) {

        }
    }

    @Override
    public void DialogClose(DialogResult result, int resultCode) {
        if(result== DialogResult.Ok){
            switch (resultCode){
                case 100:
                    OrderCollection orders = new OrderCollection();
                    orders.DeleteOrder(getApplicationContext(), order);
                    setResult(ORDER_ACTIVITY);
                    super.onBackPressed();
                    break;
                case 200:
                    // удаляем проводку
                    order.TransList.DeleteTransaction(getApplicationContext(),SelectedTransaction);
                    // обновляем список
                    setTransList();
                    break;
            }

        }
    }

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser(); // Creating JSON Parser object

    // проверяем можно ли отправить заявку на сбыт
    private class CheckCanSendOrderOnSbit extends AsyncTask<Void,Void,HttpResponse>{

        // свойство завершения события
        public IAsynkTask onAsynkTask;

        public void setOnCompleteListener(IAsynkTask task){
            onAsynkTask = task;
        }

        // Перед отправкой заявки
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView txtMemo = (TextView)findViewById(R.id.txtMemo);
            order.Memo = txtMemo.getText().toString();
            order.Save(OrderActivity.this);
            pDialog = new ProgressDialog(OrderActivity.this);
            pDialog.setMessage("Проверка заявки ...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        // обработка фонового задания
        @Override
        protected HttpResponse doInBackground(Void... params) {
            try{
                String strJson = order.getJsonString();
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost( Functions.Domen+"manager/order-cansend");
                List<NameValuePair> parms = new ArrayList<NameValuePair>();
                parms.add(new BasicNameValuePair("Order",Uri.encode(strJson)));
                parms.add(new BasicNameValuePair("Ticket", Workarea.Ticket));
                httpPost.setEntity(new UrlEncodedFormEntity(parms));
                return httpClient.execute(httpPost);
            }
            catch (Exception e){
                return null;
            }
        }

        @Override
        protected void onPostExecute(HttpResponse result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if(result != null){
                try{
                    Header header= result.getFirstHeader("UpdateStatus");
                    if(header !=null) {
                        if (header.getValue().equals("ERROR")) {
                            Functions.showMessage(getApplicationContext(), R.layout.order, "ДОСТУП ЗАПРЕЩЕН", Functions.ToastMessageType.Error, 5000);
                            if(onAsynkTask != null){
                                onAsynkTask.TaskError();
                            }
                        } else {
                            JSONObject jsonObject= jsonParser.GetJsonObject(result);
                            LogErrorItemCollection errors = new LogErrorItemCollection();
                            errors.DesirializeFromJson(jsonObject);
                            if(errors.size()==0){
                                if(onAsynkTask != null){
                                    onAsynkTask.TaskComplete();
                                }
                            }
                            else{
                                LogErrorItem headerItem = new LogErrorItem();
                                headerItem.Description = "Заявка для ["+order.Shop.Name+"]";
                                headerItem.Type = LogErrorItemTypeEnums.Header;
                                errors.add(0,headerItem);
                                LogErrorDialog dlg = new LogErrorDialog();
                                dlg.setErrorList(errors);
                                dlg.show(getSupportFragmentManager(),"LogErrorWindow");
                                if(onAsynkTask != null){
                                    onAsynkTask.TaskError();
                                }
                            }
                        }
                    }

                }
                catch (Exception ex){
                    Functions.showErrorMessage(getApplicationContext(), R.layout.order, "OrderActivity.java", "CheckCanSendOrderOnSbit.onPostExecute()", ex.getMessage(), 15000);
                }
            }
            else{
                Functions.showMessage(getApplicationContext(),R.layout.order,"Ошибка передачи данных по сети. Проверьте доступность интернета", Functions.ToastMessageType.Error,5000);
            }
        }
    }

    // отправляем заявку на сбыт
    private class OrderSendToSbit extends AsyncTask<Void, Void, HttpResponse>{

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            TextView txtMemo = (TextView)findViewById(R.id.txtMemo);
            order.Memo = txtMemo.getText().toString();
            order.Save(OrderActivity.this);
            pDialog = new ProgressDialog(OrderActivity.this);
            pDialog.setMessage("Отправка заявки на сервер ...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected HttpResponse doInBackground(Void... params) {
            try{

                String strJson = order.getJsonString();
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost( Functions.Domen+"manager/order-send");

                List<NameValuePair> parms = new ArrayList<NameValuePair>();
                parms.add(new BasicNameValuePair("Order",Uri.encode(strJson)));
                parms.add(new BasicNameValuePair("Ticket", Workarea.Ticket));
                httpPost.setEntity(new UrlEncodedFormEntity(parms));
                HttpResponse result = httpClient.execute(httpPost);
                return result;
            }
            catch (Exception e){
                return null;
            }
        }


        @Override
        protected void onPostExecute(HttpResponse result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            if(result != null){
                try{
                    Header header= result.getFirstHeader("UpdateStatus");
                    if(header !=null){
                        if(header.getValue().equals("ERROR")){
                            Functions.showMessage(getApplicationContext(),R.layout.order,"ДОСТУП ЗАПРЕЩЕН", Functions.ToastMessageType.Error,5000);
                        }
                        else{
                            // обновляем БД и
                            JSONParser parser = new JSONParser();
                            Order newOrder= new Order();
                            newOrder.DesirializeFromJson(OrderActivity.this,parser.GetJsonObject(result));

                            // обновляем заявку
                            order.OrderID = newOrder.OrderID;
                            order.LinkID = newOrder.LinkID;
                            order.Status = newOrder.Status;
                            order.Number = newOrder.Number;
                            order.Save(getApplicationContext());
                            int i=0;
                            for(Transaction tr : order.TransList){
                                Transaction trans = newOrder.TransList.get(i);
                                  i++;
                                tr.Status = trans.Status;
                            }
                            order.TransList.Update(getApplicationContext());
                            setResult(ORDER_ACTIVITY);
                            Functions.showMessage(getApplicationContext(),R.layout.order,"Передача данных завершена успешно", Functions.ToastMessageType.Success,5000);
                            finish();
                        }
                    }
                }
                catch (Exception ex){
                    Functions.showErrorMessage(getApplicationContext(), R.layout.order, "OrderActivity.java", "OrderSendToSbit.onPostExecute()", ex.getMessage(), 15000);
                }
            }
            else{
                Functions.showMessage(getApplicationContext(),R.layout.order,"Ошибка передачи данных по сети. Проверьте доступность интернета", Functions.ToastMessageType.Error,5000);
            }
        }
    }

}
