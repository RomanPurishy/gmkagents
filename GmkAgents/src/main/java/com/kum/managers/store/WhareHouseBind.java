package com.kum.managers.store;

import java.io.Serializable;

// Класс для связывания
public class WhareHouseBind implements Serializable {

    public int ID; // идентификатор
    public String Name;     // наименование склада

    public WhareHouseBind(){
        ID = 0;
        Name = "";
    }

}
