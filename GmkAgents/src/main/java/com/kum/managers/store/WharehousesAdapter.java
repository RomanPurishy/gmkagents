package com.kum.managers.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kum.managers.R;

/**
 * Created by Роман on 18.11.13.
 */
public class WharehousesAdapter extends BaseAdapter {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private WharehouseCollection houses;  // коллекция складов

    public WharehousesAdapter(Context context,WharehouseCollection coll){
        super();
        houses = coll;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return houses.size();
    }

    @Override
    public Object getItem(int i) {
        return houses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView =inflater.inflate(R.layout.wharehouse_view,null);
        Wharehouse wharehouse = houses.get(i);
        TextView txtName = (TextView)convertView.findViewById(R.id.wharehouseName);
        txtName.setText(wharehouse.Name);
        return convertView;
    }
}
