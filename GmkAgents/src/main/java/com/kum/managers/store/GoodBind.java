package com.kum.managers.store;

import java.io.Serializable;

// товар для связывания данных
public class GoodBind implements Serializable {

    public int ID;  // идентификатор товара
    public String Name; // наименование товара

    public GoodBind(){
        this.ID = 0;
        this.Name = "";
    }
}
