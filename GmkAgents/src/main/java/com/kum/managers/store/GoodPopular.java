package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

/**
 *  Популярная продукция для покупателя
 */
public class GoodPopular {

    // Метод добавляет запись популярной продукции если  она есть
    public void Add(int goodId,int byerId,int byerShopId,Context context){
        if(CheckLink(goodId,byerId,byerShopId,context)==false){
            SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

            try{
                String sql = "insert into GOODS_POPULAR(GoodID,ByerID,ByerShopID) values("+goodId+","+byerId+","+byerShopId+")";
                dbHelper.execSQL(sql);
            }
            catch (Exception ex){

            }
            finally {
                dbHelper.close();
            }
        }
    }

    public boolean CheckLink(int goodId,int byerId,int byerShopId,Context context){
        // создаем объект для создания и управления версиями БД
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();
        try {
            Cursor mCursor =dbHelper.rawQuery("SELECT GoodID FROM  GOODS_POPULAR where GoodID="+goodId+" and ByerID="+byerId+" and ByerShopID="+byerShopId,null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                   int id= mCursor.getInt(0);
                   return id>0;
                }
            }
            mCursor.close();

        }
        catch (Exception e){
            return false;
        }
        finally {
            dbHelper.close();
        }
        return false;
    }
}
