package com.kum.managers.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.kum.managers.Common.Functions;
import com.kum.managers.R;

/**
 * Created by Роман on 25.11.13.
 */
public class OrderListAdapter  extends BaseAdapter {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private OrderCollection orders;  // коллекция заявок
    private Context _context;

    public OrderListAdapter(Context context,OrderCollection collection){
        super();
        _context = context;
        orders = collection;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return orders.get(position).ID;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        double size = Functions.screenInches(_context);
        View convertView;
        if(size<=5){
            convertView =inflater.inflate(R.layout.order_row_phone,null);
        }
        else{
            convertView =inflater.inflate(R.layout.order_row,null);
        }
        Order od = orders.get(i);
        UpdateOrderView(convertView,od);
        return convertView;
    }

    // Метод обновляет заказ
    public void UpdateOrderView(View v,Order order){
        TextView txtOnDate = (TextView)v.findViewById(R.id.order_ondate);
        TextView txtNumber = (TextView)v.findViewById(R.id.order_number);
        TextView txtByer = (TextView)v.findViewById(R.id.order_byer);
        TextView txtShop =(TextView)v.findViewById(R.id.order_shop);
        TextView txtSum = (TextView)v.findViewById(R.id.order_sum);
        TextView txtQty = (TextView)v.findViewById(R.id.order_qty);
        ImageView status = (ImageView)v.findViewById(R.id.order_status);

        txtOnDate.setText(android.text.format.DateFormat.format("dd/MM/yyyy", order.OnDate));
        txtByer.setText(order.Byer.Name);
        txtShop.setText(order.Shop.Name);
        txtNumber.setText(order.Number+"");
        if(order.Qty>0){
            txtQty.setText(Functions.Format(order.Qty));
        }
        else{
            txtQty.setText("");
        }
        if(order.Sum>0){
            txtSum.setText(Functions.Format(order.Sum));
        }
        else{
            txtSum.setText("");
        }

        switch (order.Status){
            case New:
                status.setImageResource(R.drawable.order_new48);
                break;
            case Sended:
                status.setImageResource(R.drawable.order_sended48);
                break;
            case Drufted:
                status.setImageResource(R.drawable.order_drafted48);
                break;
            case Deleted:
                status.setImageResource(R.drawable.order_deleted48);
                break;
            case Executed:
                status.setImageResource(R.drawable.order_executed48);
                break;
        }
    }

}
