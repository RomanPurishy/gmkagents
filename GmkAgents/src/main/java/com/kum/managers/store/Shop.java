package com.kum.managers.store;

// Класс магазина
public class Shop {

    public int ID;             // идентификатор
    public String Guid;    // гуид
    public String Name;   // наименование покупателя
    public Byer ParentByer;          // покупатель связаный с магазином
    public int ByerID;      // идентификатор покупателя

    public Shop(int id,String gd,String name,int byerID,Byer byer){
        this.ID = id;
        this.Guid = gd;
        this.Name = name;
        this.ParentByer = byer;
        this.ByerID = byerID;
    }
}
