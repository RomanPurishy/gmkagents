package com.kum.managers.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kum.managers.Common.Functions;
import com.kum.managers.R;

/**
 * Created by PURISHI on 31.10.13.
 */
public class ShopListAdapter   extends BaseAdapter {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private ShopCollection shops;  // коллекция магазинов
    private Context _context;

    public ShopListAdapter(Context context,ShopCollection coll){
        super();
        shops = coll;
        _context = context;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return shops.size();
    }

    @Override
    public Object getItem(int i) {
        return shops.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        double size = Functions.screenInches(_context);
        View convertView;
        if(size<=5){
            convertView =inflater.inflate(R.layout.shop_view_phone,null);
        }
        else{
            convertView =inflater.inflate(R.layout.shop_view,null);
        }


        Shop shop = shops.get(i);
       // Log.println(7,"shops",shop.Name);
        TextView txtName = (TextView)convertView.findViewById(R.id.shopName);
        txtName.setText(shop.Name);
        return convertView;
    }
}
