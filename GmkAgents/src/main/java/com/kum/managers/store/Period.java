package com.kum.managers.store;

import com.kum.managers.Common.DateFunctions;
import com.kum.managers.Common.Functions;
import com.kum.managers.Interfaces.IPeriod;

import java.util.Calendar;
import java.util.Date;


/*
 * Период
 */
public class Period {

    public static Date Start;   // дата начала
    public static Date End;     // дата окончания

    public Period(){

    }

    public static void setNow(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Start = Functions.getSimpleDate(cal.getTime());
        cal.add(Calendar.DATE, 1);
        End =Functions.getSimpleDate(cal.getTime());
    }

    public static String getTitle(){
        int startYear = DateFunctions.getYear(Start);
        int endYear = DateFunctions.getYear(End);
        int startMonth = DateFunctions.getMonth(Start);
        int endMonth = DateFunctions.getMonth(End);
        int startDay = DateFunctions.getDay(Start);
        int endDay = DateFunctions.getDay(End);
        int lastDay = DateFunctions.getLastDayInMonth(End);
        if((startMonth==endMonth && startYear==endYear) && (startDay !=endDay || startMonth != endMonth)){
            // по месяцам
            switch (startMonth){
                case 0: return "Январь "+startYear;
                case 1: return "Февраль "+startYear;
                case 2: return "Март "+startYear;
                case 3: return "Апрель "+startYear;
                case 4: return "Май "+startYear;
                case 5: return "Июнь "+startYear;
                case 6: return "Июль "+startYear;
                case 7: return "Август "+startYear;
                case 8: return "Сентябрь "+startYear;
                case 9: return "Октябрь "+startYear;
                case 10: return "Ноябрь "+startYear;
                case 11: return "Декабрь "+startYear;
            }
        }
        else if(Start.getTime()==End.getTime() && Start.getTime()==DateFunctions.getCurrentShortDate().getTime()){
            return "СЕГОДНЯ";
        }
        else if(startYear ==endYear){
            if(startDay==1 && startMonth==0 && lastDay==endDay && endMonth==2){
                return "I квартал "+startYear;
            }
            else if(startDay==1 && startMonth==3 && lastDay==endDay && endMonth==5){
                return "II квартал "+startYear;
            }
            else if(startDay==1 && startMonth==6 && lastDay==endDay && endMonth==8){
                return "III квартал "+startYear;
            }
            else if(startDay==1 && startMonth==9 && lastDay==endDay && endMonth==11){
                return "IV квартал "+startYear;
            }
            else if(startDay==1 && startMonth==0 && lastDay==endDay && endMonth==5){
                return "I полугодие "+startYear;
            }
            else if(startDay==1 && startMonth==6 && lastDay==endDay && endMonth==11){
                return "II полугодие "+startYear;
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(Period.End );
        cal.add(Calendar.MONTH, -9);
        Date dt= cal.getTime();
        if(dt.getTime() ==Start.getTime()){
            return "за 9 месяцев";
        }

        if(startDay==1 && startMonth==0 && startYear==endYear && endMonth==11 && endDay==31){
            return "Весь "+startYear;
        }

        if(startDay==1 && startMonth==0 && startYear ==1900 && endDay==1 && endMonth==0 && endYear==4000){
            return "Все данные";
        }

        cal = Calendar.getInstance();
        cal.setTime(Period.End );
        cal.add(Calendar.DAY_OF_MONTH,-30);
        dt = cal.getTime();
        if(dt.getTime() ==Start.getTime()){
            return "за 30 дней";
        }

        cal = Calendar.getInstance();
        cal.setTime(Period.End );
        cal.add(Calendar.DAY_OF_MONTH,-60);
        dt = cal.getTime();
        if(dt.getTime() ==Start.getTime()){
            return "за 60 дней";
        }

        Date now = DateFunctions.addDaysToShortDate(DateFunctions.getCurrentShortDate(),1);
        if(Start.getTime()==now.getTime() && End.getTime() == now.getTime()){
            return "завтра "+Functions.getDateShortString(Start);
        }
        return "с "+ Functions.getDateShortString(Start)+ " по "+Functions.getDateShortString(End);
    }

    public static IPeriod OnChangedPeriod;

/*    public interface OnPeriodChanged{

        void OnPeriodChaged(Date dateStart,Date dateEnd);
    }*/
    public static void setPeriodChangedListener(IPeriod periodChanged){
        OnChangedPeriod = periodChanged;
    }

    // Метод меняет период и вызывает событие смены периода
    public static void ChangePeriod(Date dateStart,Date dateEnd){
        Start = dateStart;
        End = dateEnd;

        if(OnChangedPeriod != null){
            OnChangedPeriod.periodChanged(dateStart, dateEnd);
        }
    }


}
