package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

// Коллекция складов
public class WharehouseCollection implements List<Wharehouse> {
    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;

    private List<Wharehouse> houses = new ArrayList<Wharehouse>();

    // Коллекция складов
    public WharehouseCollection(Context context){
        appContext = context;
        IsLoaded = false;
    }

    @Override
    public void add(int i, Wharehouse wharehouse) {
        houses.add(i,wharehouse);
        IsLoaded = false;
    }

    @Override
    public boolean add(Wharehouse wharehouse) {
        return houses.add(wharehouse);
    }

    @Override
    public boolean addAll(int i, Collection<? extends Wharehouse> wharehouses) {
        return houses.addAll(i,wharehouses);
    }

    @Override
    public boolean addAll(Collection<? extends Wharehouse> wharehouses) {
        return  houses.addAll(wharehouses);
    }

    @Override
    public void clear() {
        houses.clear();
    }

    @Override
    public boolean contains(Object o) {
        return houses.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return houses.containsAll(objects);
    }

    @Override
    public Wharehouse get(int i) {
        return houses.get(i);
    }

    @Override
    public int indexOf(Object o) {
        return houses.indexOf(o);
    }

    @Override
    public boolean isEmpty() {
        return houses.isEmpty();
    }

    @Override
    public Iterator<Wharehouse> iterator() {
        return houses.iterator();
    }

    @Override
    public int lastIndexOf(Object o) {
        return houses.lastIndexOf(o);
    }

    @Override
    public ListIterator<Wharehouse> listIterator() {
        return houses.listIterator();
    }

    @Override
    public ListIterator<Wharehouse> listIterator(int i) {
        return houses.listIterator(i);
    }

    @Override
    public Wharehouse remove(int i) {
        return houses.remove(i);
    }

    @Override
    public boolean remove(Object o) {
        return houses.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return houses.retainAll(objects);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return houses.retainAll(objects);
    }

    @Override
    public Wharehouse set(int i, Wharehouse wharehouse) {
        return houses.set(i,wharehouse);
    }

    @Override
    public int size() {
        return houses.size();
    }

    @Override
    public List<Wharehouse> subList(int i, int i2) {
        return houses.subList(i,i2);
    }

    @Override
    public Object[] toArray() {
        return houses.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return houses.toArray(ts);
    }

    public boolean IsLoaded;

    // Загрузка данных из бд
    public void Load(){
        clear();
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();

        // переменные для query
        String[] columns = new String[]{"ID","GUID","Name","IsDefault"};
        String orderBy ="IsDefault DESC,Name2";
        Cursor mCursor =dbHelper.query("WAREHOUSE",columns,null,null,null,null,orderBy);
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                do {
                    Wharehouse sklad = new Wharehouse(appContext);
                    sklad.ID =mCursor .getInt(0);
                    sklad.Guid = mCursor.getString(1);
                    sklad.Name = mCursor.getString(2);
                    sklad.Default = mCursor.getInt(3)>0;
                    houses.add(sklad);
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }
        dbHelper.close();
        IsLoaded = true;
    }

    // Функция возвращает склад по умолчанию
    public Wharehouse GetDefault(){
        Wharehouse def  = null;

        for(int i=0;i<size();i++){
            if(get(i).Default){
                def=get(i);
                break;
            }
        }

        return def;
    }
}
