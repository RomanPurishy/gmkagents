package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

import java.io.Serializable;

// товар
public class Good implements Serializable {

    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;

    public int ID;
    public String Guid;
    public String Name;
    public double Price;
    public int WareHouseID;
    public boolean IsVisible;
    public double Qty;      // заявленное количество
    public double Fqty;     // произвольное количество
    public double Sum;      // сумма по заявку

    public Good(Context context){
        appContext= context;
        ID=0;
        Guid = "";
        Name = "";
        Price = 0.0;
        WareHouseID=0;
        IsVisible = true;
        Qty =0.0;
        Fqty = 0.0;
        Sum = 0.0;
    }

    public Good(Context context,int id){
        appContext= context;
        ID=id;
        Guid = "";
        Name = "";
        Price = 0.0;
        WareHouseID=0;
        IsVisible = true;
        Qty =0.0;
        Fqty = 0.0;
        Sum = 0.0;
    }

    // Загрузка свойств
    public void Load(){
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();
        // переменные для query
        String selection = "ID= ?";
        String[] selectionArgs = new String[] {ID+"" };
        String[] columns = new String[]{"ID","GUID","Name","Price","WhareHouseID"};
        Cursor mCursor =dbHelper.query("GOODS",columns,selection,selectionArgs,null,null,null);
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                this.ID =mCursor .getInt(0);
                this.Guid = mCursor.getString(1);
                this.Name = mCursor.getString(2);
                this.Price = mCursor.getDouble(3);
                this.WareHouseID = mCursor.getInt(4);
            }
            mCursor.close();
        }
        dbHelper.close();
    }
}
