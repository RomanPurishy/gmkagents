package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

// Коллекция покупателя
public class ShopCollection  implements List<Shop> {
    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;
    List<Shop> shops = new ArrayList<Shop>();

    public ShopCollection(Context context,Byer byer){
        IsLoaded = false;
        appContext = context;
        ParentByer = byer;
    }

    // Загружена ли была коллекция
    public boolean IsLoaded;

    @Override
    public void add(int i, Shop shop) {
        shops.add(i,shop);
    }

    @Override
    public boolean add(Shop shop) {
        return shops.add(shop);
    }

    @Override
    public boolean addAll(int i, Collection<? extends Shop> coll) {
        return shops.addAll(i,coll);
    }

    @Override
    public boolean addAll(Collection<? extends Shop> coll) {
        return shops.addAll(coll);
    }

    @Override
    public void clear() {
        shops.clear();
    }

    @Override
    public boolean contains(Object o) {
        return shops.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return shops.containsAll(objects);
    }

    @Override
    public Shop get(int i) {
        return shops.get(i);
    }

    @Override
    public int indexOf(Object o) {
        return shops.indexOf(o);
    }

    @Override
    public boolean isEmpty() {
        return shops.isEmpty();
    }

    @Override
    public Iterator<Shop> iterator() {
        return shops.iterator();
    }

    @Override
    public int lastIndexOf(Object o) {
        return shops.lastIndexOf(o);
    }

    @Override
    public ListIterator<Shop> listIterator() {
        return shops.listIterator();
    }

    @Override
    public ListIterator<Shop> listIterator(int i) {
        return shops.listIterator(i);
    }

    @Override
    public Shop remove(int i) {
        return shops.remove(i);
    }

    @Override
    public boolean remove(Object o) {
        return shops.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return shops.removeAll(objects);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return shops.retainAll(objects);
    }

    @Override
    public Shop set(int i, Shop shop) {
        return shops.set(i,shop);
    }

    @Override
    public int size() {
        return shops.size();
    }

    @Override
    public List<Shop> subList(int i, int i2) {
        return subList(i,i2);
    }

    @Override
    public Object[] toArray() {
        return shops.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return shops.toArray(ts);
    }

    // родительский покупатель
    public Byer ParentByer;

    // Метод устанавливает покупателя
    public void setByer(Byer byer){
        ParentByer = byer;
    }



    // Загрузка коллекции заявок
    public void Load(){
        clear();
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();
        // переменные для query
        String selection = "BYER_ID= ?";
        String[] selectionArgs = new String[] {ParentByer.ID+"" };
        String[] columns = new String[]{"ID","GUID","Name","BYER_ID"};
        Cursor mCursor =dbHelper.query("SHOPS",columns,selection,selectionArgs,null,null,"Name2");
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                do {
                    Integer id =mCursor .getInt(0);
                    String guid = mCursor.getString(1);
                    String name = mCursor.getString(2);
                    int byerID = mCursor.getInt(3);
                    Shop shop = new Shop(id,guid,name,byerID,ParentByer);
                    add(shop);
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }
        IsLoaded = true;
    }
}
