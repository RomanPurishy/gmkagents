package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.DBHelper;

import java.io.Serializable;
import java.util.Date;

// Проводка
public class Transaction implements Serializable {

    public int ID;  // идентификатор проводки
    public Date DateAdded;  // дата добавления записи в БД
    public Date OnDate; // дата проводки
    public int OrderID; // идентификатор заявки
    public String Guid; // гуид проводки
    public GoodBind Good;   // выбранный товар
    public int OrderLinkID;     // идентификатор связаного заказа в бд менеджеров
    public double Qty;  // количество заявленое
    public double Fqty; // произвольное количество
    public double Fqty2;
    public double Qty2;     // количество выполненное
    public double Qty3;
    public double Qty4;
    public double Qty5;
    public double Price;
    public double Price2;
    public double Sum;
    public double Sum2;
    public String Memo;
    public int Number;      // номер проводки
    public ByerBind Byer;       // покупатель
    public ShopBind Shop;       // магазин покупателя
    public AgentBind Manager;   // региональный менеджер
    public boolean IsFiscal;        // фискальная или нет проводка
    public OrderStatus Status;      // статус проводки

    public Transaction(){
        ID=0;
        DateAdded = new Date();
        OnDate = new Date();
        OrderID = 0;
        Guid = "";
        Good = new GoodBind();
        OrderLinkID = 0;
        Memo = "";
        Qty = 0.0;
        Fqty2=0.0;
        Fqty = 0.0;
        Qty2=0.0;
        Qty3 = 0.0;
        Qty4= 0.0;
        Qty5 = 0.0;
        Price = 0.0;
        Price2 = 0.0;
        Sum =0.0;
        Sum2 = 0.0;
        Number = 1;
        Byer = new ByerBind();
        Shop = new ShopBind();
        Manager = new AgentBind();
        IsFiscal = true;
        Status = OrderStatus.New;
    }

       // Метод устанавливает количества товара
    public void setQty(double qty){
        this.Qty = qty;
        this.Sum = Functions.Round(qty * this.Price,2);
    }

    // Загрузка свойств проводки по гуиду
    public void Load(Context context, String guid){
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT  jrn.ID,jrn.Guid,jrn.DateAdded,jrn.OrderID,jrn.OrderLinkID,jrn.GoodID,gd.Name as GoodName,jrn.Date,jrn.Memo,jrn.Qty,jrn.Qty2,jrn.Qty3,jrn.Qty4,jrn.Qty5,\n" +
                "jrn.Fqty,jrn.Fqty2,jrn.Price,jrn.Price2,jrn.Sum,jrn.Sum2,jrn.IsFiscal,jrn.Number,jrn.ByerID,byer.Name as ByerName,jrn.ByerShopID,shop.Name as ByerShopName,jrn.Status,jrn.AgentID\n" +
                "FROM JOURNAL as jrn INNER JOIN\n" +
                "GOODS  as gd ON jrn.GoodID=gd.ID INNER JOIN\n" +
                "BYERS as byer ON jrn.ByerID=byer.ID LEFT OUTER JOIN\n" +
                "SHOPS as shop ON jrn.ByerShopID=shop.ID\n" +
                "where jrn.Guid='"+guid+"'\n" +
                "ORDER BY jrn.Number";

        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    this.ID = mCursor.getInt(0);
                    this.Guid = mCursor.getString(1);
                    if(mCursor.getString(2) != null){
                        this.DateAdded = Functions.stringToDate(mCursor.getString(2));
                    }
                    this.OrderID = mCursor.getInt(3);
                    this.OrderLinkID = mCursor.getInt(4);
                    this.Good.ID = mCursor.getInt(5);
                    this.Good.Name = mCursor.getString(6);
                    if(mCursor.getString(7) != null){
                        this.OnDate = Functions.stringToDate(mCursor.getString(7));
                    }
                    this.Memo = mCursor.getString(8);
                    this.Qty = mCursor.getDouble(9);
                    this.Qty2 = mCursor.getDouble(10);
                    this.Qty3 = mCursor.getDouble(11);
                    this.Qty4 = mCursor.getDouble(12);
                    this.Qty5 = mCursor.getDouble(13);
                    this.Fqty = mCursor.getDouble(14);
                    this.Fqty2 = mCursor.getDouble(15);
                    this.Price = mCursor.getDouble(16);
                    this.Price2 = mCursor.getDouble(17);
                    this.Sum = mCursor.getDouble(18);
                    this.Sum2 = mCursor.getDouble(19);
                    this.IsFiscal = mCursor.getInt(20)>0;
                    this.Number = mCursor.getInt(21);
                    this.Byer.ID = mCursor.getInt(22);
                    this.Byer.Name = mCursor.getString(23);
                    this.Shop.ID = mCursor.getInt(24);
                    this.Shop.Name = mCursor.getString(25);
                    this.Status = Functions.getOrderStatus(mCursor.getShort(26));

                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG);
        }
        finally {
            dbHelper.close();
        }
    }

    // Метод сохраняет проводку
    public boolean Save(Context context){
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();
        try {
            String sql = "UPDATE JOURNAL SET OrderLinkID="+OrderLinkID+",Memo='"+Memo+"',Qty="+Qty+
                    ",Qty2="+Qty2+",Qty3="+Qty3+",Qty4="+Qty4+",Qty5="+Qty5+",Fqty="+Fqty+
                    ",Fqty2="+Fqty2+",Price="+Price+",Price2="+Price2+"," +
                    "Sum="+Sum+",Sum2="+Sum2+",IsFiscal="+(IsFiscal ? 1 : 0)+",Number="+Number+",Status="+Functions.getOrderStatus(Status)+"\n" +
                    "WHERE ID="+ID;
            dbHelper.execSQL(sql);
           // Log.println(7,"Transaction","количество по проводке=["+Qty+"]");
            return true;
        }
        catch (Exception ex){
            return false;
        }
        finally {
            dbHelper.close();
        }
    }
}
