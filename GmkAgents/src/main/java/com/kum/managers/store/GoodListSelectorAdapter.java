package com.kum.managers.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kum.managers.Common.Functions;
import com.kum.managers.R;

/**
 * Адаптер данных для выбора товаров в заявке
 */
public class GoodListSelectorAdapter  extends BaseAdapter  {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private GoodCollection goods;  // коллекция товаров
    private Context _context;

    public GoodListSelectorAdapter(Context context,GoodCollection collection){
        super();
        goods = collection;
        _context = context;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int i) {
        return goods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return goods.get(i).ID;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        double size = Functions.screenInches(_context);
        View convertView;
        if(size<=5){
            convertView =inflater.inflate(R.layout.good_selector_view_phone,null);
        }
        else{
            convertView =inflater.inflate(R.layout.good_selector_view,null);
        }
        Good good = goods.get(i);
        UpdateRow(convertView,good);
        return convertView;
    }

    public void UpdateRow(View v,Good good){
        TextView txtName = (TextView)v.findViewById(R.id.good_name);
        ImageView transOk = (ImageView)v.findViewById(R.id.trans_ok);
        TextView transSum = (TextView)v.findViewById(R.id.good_sum);
        TextView fQtyText = (TextView)v.findViewById(R.id.good_fqty);

       // Log.println(7,"transaction",good.Name);
        txtName.setText(good.Name);
        // количество
        TextView txtQty = (TextView)v.findViewById(R.id.good_qty);
        if(good.Qty>0){
            txtQty.setText(Functions.Format(good.Qty));
            transOk.setVisibility(View.VISIBLE);
        }
        else{
            transOk.setVisibility(View.GONE);
            txtQty.setText("");
        }
        if(good.Sum>0){
            transSum.setText(Functions.Format(good.Sum));
        }
        else{
            transSum.setText("");
        }
        if(good.Fqty>0){
            fQtyText.setText(Functions.Format(good.Fqty));
        }
        else{
            fQtyText.setText("");
        }

        // цена
        TextView txtPrice = (TextView)v.findViewById(R.id.good_price);
        if(good.Price>0){
            txtPrice.setText(Functions.Format(good.Price));
        }
        else{
            txtPrice.setText("");
        }


    }

}
