package com.kum.managers.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kum.managers.Common.Functions;
import com.kum.managers.R;

/**
 * Список проводок по заказу
 */
public class TransListAdapter extends BaseAdapter {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private Order order;  // заявка
    Context _context;

    public TransListAdapter(Context context,Order od){
        super();
        _context = context;
        order = od;
        order.TransList.Load(context,order.ID);
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return order.TransList.size();
    }

    @Override
    public Object getItem(int position) {
        return order.TransList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return order.TransList.get(position).ID;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        double size = Functions.screenInches(_context);
        View convertView;
        if(size<=5){
            convertView =inflater.inflate(R.layout.order_transaction_phone,null);
        }
        else{
            convertView =inflater.inflate(R.layout.order_transaction,null);
        }

        Transaction trans = order.TransList.get(i);
        UpdateRow(convertView,trans);
        return convertView;
    }

    // Метод обновляет строку
    public void UpdateRow(View v,Transaction trans){
        TextView txtNumber = (TextView)v.findViewById(R.id.tr_number);
        TextView txtGoodName= (TextView)v.findViewById(R.id.tr_good_name);
        TextView txtQty =(TextView)v.findViewById(R.id.tr_qty);
        TextView txtPrice= (TextView)v.findViewById(R.id.tr_price);
        TextView txtSum = (TextView)v.findViewById(R.id.tr_sum);
        TextView txtFqty = (TextView)v.findViewById(R.id.tr_fqty);

        txtNumber.setText(trans.Number+"");
        txtGoodName.setText(trans.Good.Name);
       // Log.println(7,"test","Проводка ="+trans.Number+ " "+ trans.Good.Name+" Qty=["+trans.Qty+"]");

        if(trans.Qty>0){
            txtQty.setText(Functions.Format(trans.Qty));
        }
        else{
            txtQty.setText("");
        }
        if(trans.Price>0){
            txtPrice.setText(Functions.Format(trans.Price));
        }
        else{
            txtPrice.setText("");
        }
        if(trans.Sum>0){
            txtSum.setText(Functions.Format(trans.Sum));
        }
        else{
            txtSum.setText("");
        }
        if(trans.Fqty>0){
            txtFqty.setText(Functions.Format(trans.Fqty));
        }
        else{
            txtFqty.setText("");
        }
    }
}
