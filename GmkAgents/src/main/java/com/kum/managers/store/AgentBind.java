package com.kum.managers.store;

import java.io.Serializable;

// Агент для связки
public class AgentBind implements Serializable {

    public int ID;
    public String Name;

    public AgentBind(){
        ID=0;
        Name = "";
    }
}
