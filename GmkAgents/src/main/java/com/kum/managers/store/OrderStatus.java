package com.kum.managers.store;

import java.io.Serializable;

// Статус заявки
public enum OrderStatus  implements Serializable {
    New,
    Sended,
    Drufted,
    Deleted,
    Executed
}
