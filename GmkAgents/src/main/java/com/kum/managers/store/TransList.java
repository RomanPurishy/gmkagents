package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.DBHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

// Список проводок
public class TransList implements List<Transaction> ,Serializable{

    List<Transaction> transactions = new ArrayList<Transaction>();

    public double Qty;
    public double Fqty;     // произвольное колличество
    public double Qty2;
    public double Sum;      // итоговая сумма
    public double Sum2;
    public Order ParentOrder;       // родительская заявка

    public TransList(Order od){
        Qty=0.0;
        Qty2=0.0;
        Sum =0.0;
        Sum2 = 0.0;
        Fqty = 0.0;
        ParentOrder = od;
        IsLoaded = false;
    }

    @Override
    public void add(int i, Transaction transaction) {
        transactions.add(i,transaction);
    }

    @Override
    public boolean add(Transaction transaction) {
        return transactions.add(transaction);
    }

    @Override
    public boolean addAll(int i, Collection<? extends Transaction> list) {
        return transactions.addAll(i,list);
    }

    @Override
    public boolean addAll(Collection<? extends Transaction> list) {
        return transactions.addAll(list);
    }

    @Override
    public void clear() {
        transactions.clear();
    }

    @Override
    public boolean contains(Object o) {
        return transactions.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return transactions.containsAll(objects);
    }

    @Override
    public Transaction get(int i) {
        return transactions.get(i);
    }

    @Override
    public int indexOf(Object o) {
        return transactions.indexOf(o);
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Iterator<Transaction> iterator() {
        return transactions.iterator();
    }

    @Override
    public int lastIndexOf(Object o) {
        return transactions.lastIndexOf(o);
    }

    @Override
    public ListIterator<Transaction> listIterator() {
        return transactions.listIterator();
    }

    @Override
    public ListIterator<Transaction> listIterator(int i) {
        return transactions.listIterator(i);
    }

    @Override
    public Transaction remove(int i) {
        return transactions.remove(i);
    }

    @Override
    public boolean remove(Object o) {
        return transactions.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return transactions.retainAll(objects);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return transactions.retainAll(objects);
    }

    @Override
    public Transaction set(int i, Transaction transaction) {
        return transactions.set(i,transaction);
    }

    @Override
    public int size() {
        return transactions.size();
    }

    @Override
    public List<Transaction> subList(int i, int i2) {
        return transactions.subList(i,i2);
    }

    @Override
    public Object[] toArray() {
        return transactions.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return transactions.toArray(ts);
    }

    public boolean IsLoaded;

    // Метод загружает коллекцию проводок
    public void Load(Context context,int orderId){
        transactions.clear();
        IsLoaded = false;
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT  jrn.ID,jrn.Guid,jrn.DateAdded,jrn.OrderID,jrn.OrderLinkID,jrn.GoodID,gd.Name as GoodName,jrn.Date,jrn.Memo,jrn.Qty,jrn.Qty2,jrn.Qty3,jrn.Qty4,jrn.Qty5,\n" +
                "jrn.Fqty,jrn.Fqty2,jrn.Price,jrn.Price2,jrn.Sum,jrn.Sum2,jrn.IsFiscal,jrn.Number,jrn.ByerID,byer.Name as ByerName,jrn.ByerShopID,shop.Name as ByerShopName,jrn.Status,jrn.AgentID\n" +
                "FROM JOURNAL as jrn INNER JOIN\n" +
                "GOODS  as gd ON jrn.GoodID=gd.ID INNER JOIN\n" +
                "BYERS as byer ON jrn.ByerID=byer.ID LEFT OUTER JOIN\n" +
                "SHOPS as shop ON jrn.ByerShopID=shop.ID\n" +
                "where jrn.OrderID="+orderId+"\n" +
                "ORDER BY jrn.Number";

        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    do {
                        Transaction trans = new Transaction();
                        trans.ID = mCursor.getInt(0);
                        trans.Guid = mCursor.getString(1);
                        if(mCursor.getString(2) != null){
                            trans.DateAdded = Functions.stringToDate(mCursor.getString(2));
                        }
                        trans.OrderID = mCursor.getInt(3);
                        trans.OrderLinkID = mCursor.getInt(4);
                        trans.Good.ID = mCursor.getInt(5);
                        trans.Good.Name = mCursor.getString(6);
                        if(mCursor.getString(7) != null){
                            trans.OnDate = Functions.stringToDate(mCursor.getString(7));
                        }
                        trans.Memo = mCursor.getString(8);
                        trans.Qty = mCursor.getDouble(9);
                        trans.Qty2 = mCursor.getDouble(10);
                        trans.Qty3 = mCursor.getDouble(11);
                        trans.Qty4 = mCursor.getDouble(12);
                        trans.Qty5 = mCursor.getDouble(13);
                        trans.Fqty = mCursor.getDouble(14);
                        trans.Fqty2 = mCursor.getDouble(15);
                        trans.Price = mCursor.getDouble(16);
                        trans.Price2 = mCursor.getDouble(17);
                        trans.Sum = mCursor.getDouble(18);
                        trans.Sum2 = mCursor.getDouble(19);
                        trans.IsFiscal = mCursor.getInt(20)>0;
                        trans.Number = mCursor.getInt(21);
                        trans.Byer.ID = mCursor.getInt(22);
                        trans.Byer.Name = mCursor.getString(23);
                        trans.Shop.ID = mCursor.getInt(24);
                        trans.Shop.Name = mCursor.getString(25);
                        trans.Status = Functions.getOrderStatus(mCursor.getShort(26));
                        trans.Manager.ID = mCursor.getInt(27);
                        trans.Manager.Name = Workarea.Manager.Name;

                        add(trans);
                    } while (mCursor.moveToNext());
                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG);
        }
        finally {
            IsLoaded = true;
            dbHelper.close();
        }
        Recalc();
    }

    // Метод доваляет проводку
    public void Insert(Context context,Transaction trans){
        boolean exist = false;
        Transaction founded = null;
        for(int i=0;i<transactions.size();i++){
            founded=transactions.get(i);
            if(trans.Good.ID==founded.Good.ID){
                founded.Qty=trans.Qty;
                founded.Qty2=trans.Qty2;
                founded.Qty3=trans.Qty3;
                founded.Qty4=trans.Qty4;
                founded.Qty5=trans.Qty5;
                founded.Sum=trans.Sum;
                founded.Sum2=trans.Sum2;
                founded.Fqty=trans.Fqty;
                founded.Fqty2=trans.Fqty2;
                exist = true;
                break;
            }
        }
        if(exist==false){
            addToTransList(context, trans);
        }
        else{
            if(founded != null){
                founded.Save(context);
            }
        }

        Recalc();
    }

    // Обновляем список проводок
    public boolean Update(Context context){
        boolean result = false;
        for(Transaction trans : this){
            result= trans.Save(context);
            if(result) return false;
        }
        return result;
    }

    // Метод удаляет проводку из коллекции
    public boolean DeleteTransaction(Context context,Transaction trans){
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        try{
            String sql = "DELETE from JOURNAL WHERE ID="+trans.ID;
            dbHelper.execSQL(sql);
            remove(trans);
            Recalc();
            ParentOrder.Save(context);
            for(Transaction tr : this){
                tr.Save(context);
            }
            return true;
        }
        catch (Exception ex){
            return false;
        }
        finally {
            dbHelper.close();
        }
    }


    // Метод добавляет проводку в список проводок
    private boolean addToTransList(Context context,Transaction trans){
        Calendar dt = Calendar.getInstance();
        Date now =new Date(dt.getTimeInMillis());
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();
        trans.Guid= UUID.randomUUID().toString();
        trans.Byer.ID = ParentOrder.Byer.ID;
        trans.Shop.ID = ParentOrder.Shop.ID;
        trans.Manager.ID = ParentOrder.Manager.ID;
        trans.Status = OrderStatus.New;
        try{
            String sql = "INSERT INTO JOURNAL(Guid,DateAdded,OrderLinkID,OrderID,GoodID,Date,Fqty,Qty,Price,Sum,IsFiscal,Number,ByerID,ByerShopID,Status,AgentID,Memo)\n" +
                    "VALUES('"+trans.Guid+"','"+Functions.getDateLongForSql(now)+"',0,"+ParentOrder.ID+","+trans.Good.ID+",'"+
                    Functions.getDateForSql(ParentOrder.OnDate)+"',"+trans.Fqty+","+trans.Qty+","+trans.Price+","+trans.Sum+","+(trans.IsFiscal ? 1 : 0)+","+trans.Number+
                    ","+trans.Byer.ID+","+trans.Shop.ID+","+Functions.getOrderStatus(trans.Status)+","+trans.Manager.ID+",'"+trans.Memo+ "')";
            dbHelper.execSQL(sql);
        }
        catch (Exception ex){
            return false;
        }
        finally {
            dbHelper.close();
        }
        trans.Load(context,trans.Guid);
        return add(trans);
   }

    // Метод устанавливает номер проводки
    // context -  контекст выполнения задачи
    // trans -  проводка
    // orderId - идентификатор заявки
    public void setTansactionNumber(Context context,Transaction trans,int orderId){
        trans.Number = 1;
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();
        try {
            String sql = "SELECT COUNT(ID) FROM JOURNAL where OrderID="+orderId;
            Cursor mCursor =dbHelper.rawQuery(sql, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    trans.Number = mCursor.getInt(0)+1;
                }
            }
        }
        catch (Exception ex){
            Toast.makeText(context,ex.getMessage(),Toast.LENGTH_LONG);
        }
        finally {
            dbHelper.close();
        }
    }


    // Пересчет данных
    public void Recalc(){
        this.Qty = 0.0;
        this.Qty2=0.0;
        this.Sum = 0.0;
        this.Sum2 = 0.0;
        this.Fqty = 0.0;
        for(int i=0;i<transactions.size();i++){
            Transaction trans = transactions.get(i);
            trans.Number = i+1;
            Sum+=trans.Sum;
            Sum2+=trans.Sum2;
            Qty+=trans.Qty;
            Qty2+=trans.Qty2;
            Fqty+=trans.Fqty;
        }
        ParentOrder.Sum = this.Sum;
        ParentOrder.Qty = this.Qty;
    }
}
