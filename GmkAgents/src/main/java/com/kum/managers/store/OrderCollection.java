package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.DBHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Роман on 25.11.13.
 */
public class OrderCollection implements List<Order> {

    List<Order> _orders = new ArrayList<Order>();

    public OrderCollection(){
        IsLoaded = false;
    }

    @Override
    public void add(int location, Order object) {
        _orders.add(location,object);
    }

    @Override
    public boolean add(Order object) {
        return _orders.add(object);
    }

    @Override
    public boolean addAll(int location, Collection<? extends Order> collection) {
        return _orders.addAll(location,collection);
    }

    @Override
    public boolean addAll(Collection<? extends Order> collection) {
        return _orders.addAll(collection);
    }

    @Override
    public void clear() {
        _orders.clear();
    }

    @Override
    public boolean contains(Object object) {
        return _orders.contains(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return _orders.containsAll(collection);
    }

    @Override
    public Order get(int location) {
        return _orders.get(location);
    }

    @Override
    public int indexOf(Object object) {
        return _orders.indexOf(object);
    }

    @Override
    public boolean isEmpty() {
        return _orders.isEmpty();
    }

    @Override
    public Iterator<Order> iterator() {
        return _orders.iterator();
    }

    @Override
    public int lastIndexOf(Object object) {
        return _orders.lastIndexOf(object);
    }

    @Override
    public ListIterator<Order> listIterator() {
        return _orders.listIterator();
    }

    @Override
    public ListIterator<Order> listIterator(int location) {
        return _orders.listIterator(location);
    }

    @Override
    public Order remove(int location) {
        return _orders.remove(location);
    }

    @Override
    public boolean remove(Object object) {
        return _orders.remove(object);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return _orders.removeAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return _orders.retainAll(collection);
    }

    @Override
    public Order set(int location, Order object) {
        return _orders.set(location,object);
    }

    @Override
    public int size() {
        return _orders.size();
    }

    @Override
    public List<Order> subList(int start, int end) {
        return _orders.subList(start,end);
    }

    @Override
    public Object[] toArray() {
        return _orders.toArray();
    }

    @Override
    public <T> T[] toArray(T[] array) {
        return _orders.toArray(array);
    }

    public boolean IsLoaded;

    // Метод загружает коллекцию заявок по магазину покупателя
    public void LoadByShop(Date dateStart,Date dateEnd,int shopID,Context context){
        _orders.clear();
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT od.ID,od.Guid,od.OrderID,od.DateAdded,od.OnDate,od.DateDelivary,od.Number,od.Status,od.Memo,od.Fiscal,od.ByerID,BYERS.NAME,\n" +
                "od.ByerShopID,SHOPS.NAME,od.AgentID,\n" +
                "od.Qty,od.OrderSum,od.WhareHouseID,WAREHOUSE.NAME,od.LinkID,od.StoreCheck,od.StoreCheckDate\n" +
                "FROM ORDERS as od INNER JOIN BYERS on BYERS.ID=od.ByerID \n" +
                "INNER JOIN WAREHOUSE ON WAREHOUSE.ID=od.WhareHouseID \n"+
                "LEFT OUTER JOIN SHOPS on SHOPS.ID=od.ByerShopID\n" +
                "WHERE (OnDate Between '"+Functions.getDateForSql(dateStart)+"' and  '"+Functions.getDateForSql(dateEnd)+"') and ByerShopID="+shopID+"\n" +
                "ORDER BY od.OnDate DESC,od.Number DESC";
        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    do {
                        Order order = new Order();
                        order.ID = mCursor.getInt(0);
                        order.Guid = mCursor.getString(1);
                        order.OrderID = mCursor.getInt(2);
                        if(mCursor.getString(3) != null){
                            order.DateAdded = Functions.stringToDate(mCursor.getString(3));
                        }
                        if(mCursor.getString(4) != null){
                            order.OnDate = Functions.stringToDate(mCursor.getString(4));
                        }
                        if(mCursor.getString(5) !=null){
                            order.DateDelivary = Functions.stringToDate(mCursor.getString(5));
                        }

                        order.Number = mCursor.getString(6);
                        order.Status = Functions.getOrderStatus(mCursor.getInt(7));
                        order.Memo = mCursor.getString(8);
                        order.Fiscal = mCursor.getInt(9)>0;
                        order.Byer.ID = mCursor.getInt(10);
                        order.Byer.Name = mCursor.getString(11);
                        order.Shop.ID = mCursor.getInt(12);
                        order.Shop.Name = mCursor.getString(13);
                        order.Manager.ID = mCursor.getInt(14);
                        order.Qty = mCursor.getDouble(15);
                        order.Sum = mCursor.getDouble(16);
                        order.WhareHouse.ID = mCursor.getInt(17);
                        order.WhareHouse.Name = mCursor.getString(18);
                        order.LinkID = mCursor.getInt(19);
                        order.StorCheck = mCursor.getInt(20)>0;
                        order.StorCheckDate = Functions.stringToDate(mCursor.getString(21));

                        add(order);
                    } while (mCursor.moveToNext());
                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Functions.showErrorMessage(context, "OrderCollection.java", "LoadByShop", ex.getMessage());
        }
        finally {
            dbHelper.close();
        }
        IsLoaded = true;
    }

    //Метод загружает коллекцию заявок по покупателю
    public void LoadByByer(Date dateStart,Date dateEnd,int byerID,Context context){
        _orders.clear();
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT od.ID,od.Guid,od.OrderID,od.DateAdded,od.OnDate,od.DateDelivary,od.Number,od.Status,od.Memo,od.Fiscal,od.ByerID,BYERS.NAME,\n" +
                "od.ByerShopID,SHOPS.NAME,od.AgentID,\n" +
                "od.Qty,od.OrderSum,od.WhareHouseID,WAREHOUSE.NAME,od.LinkID,od.StoreCheck,od.StoreCheckDate\n" +
                "FROM ORDERS as od INNER JOIN BYERS on BYERS.ID=od.ByerID \n" +
                "INNER JOIN WAREHOUSE ON WAREHOUSE.ID=od.WhareHouseID \n"+
                "LEFT OUTER JOIN SHOPS on SHOPS.ID=od.ByerShopID\n" +
                "WHERE (OnDate Between '"+Functions.getDateForSql(dateStart)+"' and  '"+Functions.getDateForSql(dateEnd)+"') and ByerID="+byerID+"\n" +
                "ORDER BY od.OnDate DESC,od.Number  DESC";
        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    do {
                        Order order = new Order();
                        order.ID = mCursor.getInt(0);
                        order.Guid = mCursor.getString(1);
                        order.OrderID = mCursor.getInt(2);
                        if(mCursor.getString(3) != null){
                            order.DateAdded = Functions.stringToDate(mCursor.getString(3));
                        }
                        if(mCursor.getString(4) != null){
                            order.OnDate = Functions.stringToDate(mCursor.getString(4));
                        }
                        if(mCursor.getString(5) !=null){
                            order.DateDelivary = Functions.stringToDate(mCursor.getString(5));
                        }

                        order.Number = mCursor.getString(6);
                        order.Status = Functions.getOrderStatus(mCursor.getInt(7));
                        order.Memo = mCursor.getString(8);
                        order.Fiscal = mCursor.getInt(9)>0;
                        order.Byer.ID = mCursor.getInt(10);
                        order.Byer.Name = mCursor.getString(11);
                        order.Shop.ID = mCursor.getInt(12);
                        order.Shop.Name = mCursor.getString(13);
                        order.Manager.ID = mCursor.getInt(14);
                        order.Qty = mCursor.getDouble(15);
                        order.Sum = mCursor.getDouble(16);
                        order.WhareHouse.ID = mCursor.getInt(17);
                        order.WhareHouse.Name = mCursor.getString(18);
                        order.LinkID = mCursor.getInt(19);
                        order.StorCheck = mCursor.getInt(20)>0;
                        order.StorCheckDate = Functions.stringToDate(mCursor.getString(21));

                        add(order);
                    } while (mCursor.moveToNext());
                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Functions.showErrorMessage(context, "OrderCollection.java", "LoadByShop", ex.getMessage());
        }
        finally {
            dbHelper.close();
        }
        IsLoaded = true;
    }

    // Загрузка заявок за указаный период
    public void Load(Date dateStart,Date dateEnd,OrderStatus status,Context context){
        _orders.clear();
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT od.ID,od.Guid,od.OrderID,od.DateAdded,od.OnDate,od.DateDelivary,od.Number,od.Status,od.Memo,od.Fiscal,od.ByerID,BYERS.NAME,\n" +
                "od.ByerShopID,SHOPS.NAME,od.AgentID,\n" +
                "od.Qty,od.OrderSum,od.WhareHouseID,WAREHOUSE.NAME,od.LinkID,od.StoreCheck,od.StoreCheckDate\n" +
                "FROM ORDERS as od INNER JOIN BYERS on BYERS.ID=od.ByerID \n" +
                "INNER JOIN WAREHOUSE ON WAREHOUSE.ID=od.WhareHouseID \n"+
                "LEFT OUTER JOIN SHOPS on SHOPS.ID=od.ByerShopID\n" +
                "WHERE (OnDate Between '"+Functions.getDateForSql(dateStart)+"' and  '"+Functions.getDateForSql(dateEnd)+"') and od.Status="+Functions.getOrderStatus(status)+"\n" +
                "ORDER BY od.OnDate DESC,od.Number  DESC";
        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    do {
                        Order order = new Order();
                        order.ID = mCursor.getInt(0);
                        order.Guid = mCursor.getString(1);
                        order.OrderID = mCursor.getInt(2);
                        if(mCursor.getString(3) != null){
                            order.DateAdded = Functions.stringToDate(mCursor.getString(3));
                        }
                        if(mCursor.getString(4) != null){
                            order.OnDate = Functions.stringToDate(mCursor.getString(4));
                        }
                        if(mCursor.getString(5) !=null){
                            order.DateDelivary = Functions.stringToDate(mCursor.getString(5));
                        }

                        order.Number = mCursor.getString(6);
                        order.Status = Functions.getOrderStatus(mCursor.getInt(7));
                        order.Memo = mCursor.getString(8);
                        order.Fiscal = mCursor.getInt(9)>0;
                        order.Byer.ID = mCursor.getInt(10);
                        order.Byer.Name = mCursor.getString(11);
                        order.Shop.ID = mCursor.getInt(12);
                        order.Shop.Name = mCursor.getString(13);
                        order.Manager.ID = mCursor.getInt(14);
                        order.Qty = mCursor.getDouble(15);
                        order.Sum = mCursor.getDouble(16);
                        order.WhareHouse.ID = mCursor.getInt(17);
                        order.WhareHouse.Name = mCursor.getString(18);
                        order.LinkID = mCursor.getInt(19);
                        order.StorCheck = mCursor.getInt(20)>0;
                        order.StorCheckDate = Functions.stringToDate(mCursor.getString(21));

                        add(order);
                    } while (mCursor.moveToNext());
                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Functions.showErrorMessage(context, "OrderCollection.java", "Load", ex.getMessage());
        }
        finally {
            dbHelper.close();
        }
        IsLoaded = true;
    }

    public void Load(Date dateStart,Date dateEnd,Context context){
        _orders.clear();
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT od.ID,od.Guid,od.OrderID,od.DateAdded,od.OnDate,od.DateDelivary,od.Number,od.Status,od.Memo,od.Fiscal,od.ByerID,BYERS.NAME,\n" +
                "od.ByerShopID,SHOPS.NAME,od.AgentID,\n" +
                "od.Qty,od.OrderSum,od.WhareHouseID,WAREHOUSE.NAME,od.LinkID,od.StoreCheck,od.StoreCheckDate\n" +
                "FROM ORDERS as od INNER JOIN BYERS on BYERS.ID=od.ByerID \n" +
                "INNER JOIN WAREHOUSE ON WAREHOUSE.ID=od.WhareHouseID \n"+
                "LEFT OUTER JOIN SHOPS on SHOPS.ID=od.ByerShopID\n" +
                "WHERE (OnDate Between '"+Functions.getDateForSql(dateStart)+"' and  '"+Functions.getDateForSql(dateEnd)+"') \n" +
                "ORDER BY od.OnDate DESC,od.Number  DESC";
        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    do {
                        Order order = new Order();
                        order.ID = mCursor.getInt(0);
                        order.Guid = mCursor.getString(1);
                        order.OrderID = mCursor.getInt(2);
                        if(mCursor.getString(3) != null){
                            order.DateAdded = Functions.stringToDate(mCursor.getString(3));
                        }
                        if(mCursor.getString(4) != null){
                            order.OnDate = Functions.stringToDate(mCursor.getString(4));
                        }
                        if(mCursor.getString(5) !=null){
                            order.DateDelivary = Functions.stringToDate(mCursor.getString(5));
                        }

                        order.Number = mCursor.getString(6);
                        order.Status = Functions.getOrderStatus(mCursor.getInt(7));
                        order.Memo = mCursor.getString(8);
                        order.Fiscal = mCursor.getInt(9)>0;
                        order.Byer.ID = mCursor.getInt(10);
                        order.Byer.Name = mCursor.getString(11);
                        order.Shop.ID = mCursor.getInt(12);
                        order.Shop.Name = mCursor.getString(13);
                        order.Manager.ID = mCursor.getInt(14);
                        order.Qty = mCursor.getDouble(15);
                        order.Sum = mCursor.getDouble(16);
                        order.WhareHouse.ID = mCursor.getInt(17);
                        order.WhareHouse.Name = mCursor.getString(18);
                        order.LinkID = mCursor.getInt(19);
                        order.StorCheck = mCursor.getInt(20)>0;
                        order.StorCheckDate = Functions.stringToDate(mCursor.getString(21));

                        add(order);
                    } while (mCursor.moveToNext());
                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Functions.showErrorMessage(context, "OrderCollection.java", "Load", ex.getMessage());


        }
        finally {
            dbHelper.close();
        }
        IsLoaded = true;
    }

    // Метод удаляет заказ из коллекции
    public boolean DeleteOrder(Context context,Order od){
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        try{
            String strSQL = "DELETE FROM JOURNAL WHERE OrderID="+od.ID;
            dbHelper.execSQL(strSQL);
            strSQL = "DELETE FROM ORDERS WHERE ID="+od.ID;
            dbHelper.execSQL(strSQL);
            return true;
        }
        catch (Exception ex){
            return false;
        }
        finally {
            dbHelper.close();
        }

    }


}
