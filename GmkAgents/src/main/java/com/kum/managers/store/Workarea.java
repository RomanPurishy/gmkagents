package com.kum.managers.store;

import android.app.Application;
import android.content.Context;
import android.util.Xml;

import com.kum.managers.Common.DialogResult;
import com.kum.managers.Common.Functions;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by Роман on 22.11.13.
 */
public class Workarea extends Application {

    public static Manager Manager; // менеджер
    public static boolean RememberMe;       // Запомни меня
    public static String Ticket;
    public static final int ORDER_ACTIVITY = 1001;
    public static final int LOGON_ACTIVITY=1002;
    public static final String DomenUrl_1 = "http://91.237.3.15:8099/";
    public static final String DomenUrl_2 = "http://82.207.114.246:8099/";



    final String FILENAME = "gmk_configs";

    @Override
    public void onCreate() {
        super.onCreate();

        String tt = "ddd";
/*        Intent intent = new Intent(this, LogOnActivity.class);
        startActivity(intent);*/
        // Do something here.
    }

    // Сохраняем данные
    public void SaveConfigs(Context context) {
        try {
            // отрываем поток для записи
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(context.getApplicationContext().openFileOutput(FILENAME, MODE_PRIVATE)));
            // пишем данные
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();

            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "MANAGER");
            serializer.attribute("", "Login", Manager.Login);
            serializer.attribute("","ID",Manager.ID+"");
            serializer.attribute("","Name",Manager.Name);
            serializer.attribute("", "Password", Manager.Password);
            serializer.attribute("","RememberMe",RememberMe ? "1" : "0");
            serializer.attribute("","Ticket",Ticket);
            serializer.attribute("","PeriodStart",Period.Start.toString());
            serializer.attribute("","PeriodEnd",Period.End.toString());
            serializer.attribute("","Domen",Functions.Domen);
            serializer.endTag("", "MANAGER");
            serializer.endDocument();


            bw.write(writer.toString());
            // закрываем поток
            bw.close();
            Functions.showMessage(context,"Файл записан",4000);
        }
        catch (Exception ex){
            Functions.showMessage(context,ex.getMessage(),4000);
        }
    }

    // Считываем данные
    public  void ReadConfigs(Context context){
        Manager = new Manager();
        String dt1 = "";
        String dt2 = "";
        try {
            StringBuilder xml = new StringBuilder();
            // открываем поток для чтения
            BufferedReader br = new BufferedReader(new InputStreamReader(context.openFileInput(FILENAME)));
            String line;
            // записываем данные в строку
            while ((line = br.readLine()) != null) {
                xml.append(line);
                xml.append('\n');
            }
            XmlPullParser xpp = prepareXpp(new StringReader(xml.toString()));
            // читаем данные
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    // начало документа
                    case XmlPullParser.START_DOCUMENT:
                       // Log.println(7,LOG_TAG, "START_DOCUMENT");
                        break;
                    // начало тэга
                    case XmlPullParser.START_TAG:
                        if(xpp.getName().equals("MANAGER")){
                            for (int i = 0; i < xpp.getAttributeCount(); i++) {
                                String name =xpp.getAttributeName(i);
                                if(name.equals("Login")){
                                    Workarea.Manager.Login = xpp.getAttributeValue(i);
                                }
                                else if(name.equals("Password")){
                                    Workarea.Manager.Password = xpp.getAttributeValue(i);
                                }
                                else if(name.equals("RememberMe")){
                                    Workarea.RememberMe = xpp.getAttributeValue(i).equals("1") ? true: false;
                                }
                                else if(name.equals("Ticket")){
                                    Workarea.Ticket = xpp.getAttributeValue(i);
                                }
                                else if(name.equals("ID")){
                                    Workarea.Manager.ID =Integer.parseInt( xpp.getAttributeValue(i));
                                }
                                else if(name.equals("Name")){
                                    Workarea.Manager.Name = xpp.getAttributeValue(i);
                                }
                                else if(name.equals("PeriodStart")){
                                    dt1 = xpp.getAttributeValue(i);
                                    if(dt1.length()>0){
                                        Period.Start =Functions.stringToDate(dt1);
                                    }
                                }
                                else if(name.equals("PeriodEnd")){
                                    dt2 = xpp.getAttributeValue(i);
                                    if(dt2.length()>0){
                                        Period.Start =Functions.stringToDate(dt1);
                                    }
                                }
                                else if(name.equals("Domen")){
                                    String domen = xpp.getAttributeValue(i);
                                    if(domen.length()>0){
                                        Functions.Domen =domen;
                                    }
                                }
                            }
                        }
                        break;
                    // конец тэга
                    case XmlPullParser.END_TAG:
                       // Log.println(7,LOG_TAG, "END_TAG: name = " + xpp.getName());
                        break;
                    // содержимое тэга
                    case XmlPullParser.TEXT:
                        //Log.println(7,LOG_TAG, "text = " + xpp.getText());
                        break;

                    default:
                        break;
                }
                // следующий элемент
                xpp.next();
            }

            if(dt1.length()==0 || dt2.length()==0){
                Period.setNow();
            }
        }
        catch (Exception ex){
           // Functions.showMessage(context, ex.getMessage(), 4000);
        }
    }

    XmlPullParser prepareXpp(StringReader reader) throws XmlPullParserException {
        // получаем фабрику
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        // включаем поддержку namespace (по умолчанию выключена)
        factory.setNamespaceAware(true);
        // создаем парсер
        XmlPullParser xpp = factory.newPullParser();
        // даем парсеру на вход Reader
        xpp.setInput(reader);
        return xpp;
    }
}

