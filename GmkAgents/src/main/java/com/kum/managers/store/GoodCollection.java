package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.DBHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

// Коллекция товаров
public class GoodCollection implements List<Good>,Serializable {
    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;
    List<Good> goods = new ArrayList<Good>();

    public GoodCollection(Context context){
        appContext = context;
    }

    @Override
    public void add(int i, Good good) {
        goods.add(good);
    }

    @Override
    public boolean add(Good good) {
        return goods.add(good);
    }

    @Override
    public boolean addAll(int i, Collection<? extends Good> collection) {
        return goods.addAll(i,collection);
    }

    @Override
    public boolean addAll(Collection<? extends Good> collection) {
        return goods.addAll(collection);
    }

    @Override
    public void clear() {
        goods.clear();
    }

    @Override
    public boolean contains(Object o) {
        return goods.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return goods.containsAll(objects);
    }

    @Override
    public Good get(int i) {
        return goods.get(i);
    }

    @Override
    public int indexOf(Object o) {
        return goods.indexOf(o);
    }

    @Override
    public boolean isEmpty() {
        return goods.isEmpty();
    }

    @Override
    public Iterator<Good> iterator() {
        return goods.iterator();
    }

    @Override
    public int lastIndexOf(Object o) {
        return goods.lastIndexOf(o);
    }

    @Override
    public ListIterator<Good> listIterator() {
        return goods.listIterator();
    }

    @Override
    public ListIterator<Good> listIterator(int i) {
        return goods.listIterator(i);
    }

    @Override
    public Good remove(int i) {
        return goods.remove(i);
    }

    @Override
    public boolean remove(Object o) {
        return goods.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return goods.retainAll(objects);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return goods.retainAll(objects);
    }

    @Override
    public Good set(int i, Good good) {
        return goods.set(i,good);
    }

    @Override
    public int size() {
        return goods.size();
    }

    @Override
    public List<Good> subList(int i, int i2) {
        return goods.subList(i,i2);
    }

    @Override
    public Object[] toArray() {
        return goods.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return goods.toArray(ts);
    }

    // Загрузка данных из бд
    public void Load(){
        clear();
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();

        // переменные для query
        String[] columns = new String[]{"ID","GUID","Name","Price","WhareHouseID"};
        Cursor mCursor =dbHelper.query("GOODS",columns,null,null,null,null,"Name2");
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                do {
                    Good good = new Good(appContext);
                    good.ID =mCursor .getInt(0);
                    good.Guid = mCursor.getString(1);
                    good.Name = mCursor.getString(2);
                    good.Price = mCursor.getDouble(3);
                    goods.add(good);
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }

        dbHelper.close();
    }


    // Загрузка товаров по складу
    public void LoadByWarehouse(int id,int orderId){
        clear();
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();

        String sql ="SELECT gd.ID,gd.GUID,gd.Name,gd.Name2,gd.Price,gd.WhareHouseID,j.Qty,j.Sum\n" +
                "FROM GOODS as gd left outer join JOURNAL as j on j.GoodID=gd.ID and j.OrderID="+orderId+"\n" +
                "where gd.WhareHouseID=" +id+"\n" +
                "ORDER BY gd.Name2";

        Cursor mCursor =dbHelper.rawQuery(sql,null);
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                do {
                    Good good = new Good(appContext);
                    good.ID =mCursor .getInt(0);
                    good.Guid = mCursor.getString(1);
                    good.Name = mCursor.getString(2);
                    good.Price = mCursor.getDouble(4);
                    good.Qty = mCursor.getDouble(6);
                    good.Sum = mCursor.getDouble(7);
                    goods.add(good);
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }

        dbHelper.close();
    }

    // метод загружает коллекцию товаров популярную для покупателя
    // warehouseId - идентификатор склада
    // byerId - идентификатор покупателя
    // byerIdShop - идентификатор магазина
    // идентификатор заказа
    public void LoadPopular(Context context,int warehouseId,int byerId,int byerIdShop,int orderId){
        clear();

        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();
        try{
            String sql ="SELECT gd.ID,gd.GUID,gd.Name,gd.Name2,gd.Price,gd.WhareHouseID,j.Qty,j.Sum\n" +
                    "FROM GOODS as gd inner join GOODS_POPULAR as pop on\n" +
                    " pop.GoodID=gd.ID left outer join JOURNAL as j on j.GoodID=gd.ID and j.OrderID="+orderId+"\n" +
                    "where gd.WhareHouseID=" +warehouseId+" and pop.ByerID="+byerId+" and pop.ByerShopID="+byerIdShop+" \n" +
                    "ORDER BY gd.Name2";
            if(byerIdShop==0){
                sql ="SELECT gd.ID,gd.GUID,gd.Name,gd.Name2,gd.Price,gd.WhareHouseID,j.Qty,j.Sum\n" +
                        "FROM GOODS as gd inner join GOODS_POPULAR as pop on\n" +
                        " pop.GoodID=gd.ID left outer join JOURNAL as j on j.GoodID=gd.ID and j.OrderID="+orderId+"\n" +
                        "where gd.WhareHouseID=" +warehouseId+" and pop.ByerID="+byerId+" \n" +
                        "ORDER BY gd.Name2";
            }


            Cursor mCursor =dbHelper.rawQuery(sql,null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    do {
                        Good good = new Good(context);
                        good.ID =mCursor .getInt(0);
                        good.Guid = mCursor.getString(1);
                        good.Name = mCursor.getString(2);
                        good.Price = mCursor.getDouble(4);
                        good.Qty = mCursor.getDouble(6);
                        good.Sum = mCursor.getDouble(7);
                        goods.add(good);
                    } while (mCursor.moveToNext());
                }
                mCursor.close();
            }
         //   Log.println(7,"LoadPopular","загрузка товаров="+size());
        }
        catch(Exception ex){
            Functions.showErrorMessage(context, "GoodCollection.cs", "LoadPopular", ex.getMessage());
        }
        finally {
            dbHelper.close();
        }

    }

    // Метод ищет товары по наименованию и складу
    public void SearchByName(int whId,String name,int orderId){
        clear();
        name = name.replace(" ","%");
        if(name.length()==1){
            name = name+"%";
        }
        else{
            name = "%"+name+"%";
        }
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();

        String sql ="SELECT gd.ID,gd.GUID,gd.Name,gd.Name2,gd.Price,gd.WhareHouseID,j.Qty,j.Sum\n" +
                "FROM GOODS as gd left outer join JOURNAL as j on j.GoodID=gd.ID and j.OrderID="+orderId+"\n" +
                "where gd.WhareHouseID=" +whId+" and gd.Name2 Like '"+name+"' \n" +
                "ORDER BY gd.Name2";

        Cursor mCursor =dbHelper.rawQuery(sql,null);
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                do {
                    Good good = new Good(appContext);
                    good.ID =mCursor .getInt(0);
                    good.Guid = mCursor.getString(1);
                    good.Name = mCursor.getString(2);
                    good.Price = mCursor.getDouble(4);
                    good.Qty = mCursor.getDouble(6);
                    good.Sum = mCursor.getDouble(7);
                    goods.add(good);
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }

        dbHelper.close();
    }
}
