package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

import java.io.Serializable;

// покупатель
public class Byer implements Serializable {
    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;

    public int ID;             // идентификатор
    public String Guid;    // гуид
    public String Name;   // наименование покупателя
    public int ShopsCount; // количество магазинов
    public ShopCollection Shops;  // коллекция магазинов

    public Byer(Context context){
        appContext= context;
        this.Shops = new ShopCollection(context,this);
        this.ID = 0;
        this.Guid = "";
        this.Name = "";
        this.ShopsCount = 0;
    }

    public Byer(Context context,int id){
        appContext = context;
        this.ID = id;
        this.Shops = new ShopCollection(context,this);
        this.Guid = "";
        this.Name = "";
        this.ShopsCount = 0;
    }


    // Загрузка свойств покупателя
    public void Load(){
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();
        // переменные для query
        String selection = "ID= ?";
        String[] selectionArgs = new String[] {ID+"" };
        String[] columns = new String[]{"ID","GUID","Name","SHOPS"};
        Cursor mCursor =dbHelper.query("BYERS",columns,selection,selectionArgs,null,null,"Name");
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                this.ID =mCursor .getInt(0);
                this.Guid = mCursor.getString(1);
                this.Name = mCursor.getString(2);
                this.ShopsCount = mCursor.getInt(3);
            }
            mCursor.close();
        }
        dbHelper.close();
    }
}
