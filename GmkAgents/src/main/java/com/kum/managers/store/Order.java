package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;
import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.DBHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

// Заявка
public class Order  implements Serializable{

    public int ID;  //
    public String Guid;
    public Date DateAdded;  // дата добавления
    public Date OnDate; // дата заявки
    public Date DateDelivary;       // дата доставки заявки
    public String Number;   // номер заявки
    public OrderStatus Status; // статус заявки
    public String Memo; // примечание
    public boolean StorCheck;       // установлен ли сторчек
    public Date StorCheckDate;      // дата сторчека
    public boolean Fiscal;      // фискальная заявка
    public TransList TransList; // коллекция проводок
    public WhareHouseBind WhareHouse;       // склад для связки
    public ByerBind Byer;       // покупатель
    public ShopBind Shop;       // магазин
    public AgentBind Manager; // агент
    public double Qty;      // количество по заявке
    public double Sum;      // сумма по заявке
    public int LinkID;          // ID связаного заказа в Акценте
    public int OrderID;         // идентификатор заказа в агентской базе


    public Order(int id){
        LoadDefault();
        ID=id;
    }

    public Order(){
        LoadDefault();
    }

    // Загрузка свойств по умолчанию
    public void LoadDefault(){
        ID=0;
        Guid = "";
        DateAdded = new Date();
        DateDelivary = new Date(1900,1,1);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.DATE, 1);
        OnDate = cal.getTime();
        Number = "";
        Status = OrderStatus.New;
        Memo = "";
        StorCheck = false;
        cal.set(1900,1,1);
        StorCheckDate =cal.getTime();
        Fiscal = true;
        TransList = new TransList(this);
        WhareHouse = new WhareHouseBind();
        Byer = new ByerBind();
        Shop = new ShopBind();
        Manager = new AgentBind();
        Qty = 0.0;
        Sum = 0.0;
        LinkID = 0;
        OrderID = 0;
    }

     // загрузка заявки
    public void Load(Context context){
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();

        String strSQL = "SELECT od.ID,od.Guid,od.OrderID,od.DateAdded,od.OnDate,od.DateDelivary,od.Number,od.Status,od.Memo,od.Fiscal,od.ByerID,BYERS.NAME,\n" +
                "od.ByerShopID,SHOPS.NAME,od.AgentID,\n" +
                "od.Qty,od.OrderSum,od.WhareHouseID,WAREHOUSE.NAME,od.LinkID,od.StoreCheck,od.StoreCheckDate\n" +
                "FROM ORDERS as od INNER JOIN BYERS on BYERS.ID=od.ByerID \n" +
                "INNER JOIN WAREHOUSE ON WAREHOUSE.ID=od.WhareHouseID \n"+
                "LEFT OUTER JOIN SHOPS on SHOPS.ID=od.ByerShopID\n" +
                "WHERE od.ID = "+ID;
        try{
            Cursor mCursor =dbHelper.rawQuery(strSQL, null);
            if (mCursor != null) {
                if (mCursor.moveToFirst()) {
                    ID = mCursor.getInt(0);
                    Guid = mCursor.getString(1);
                    OrderID = mCursor.getInt(2);
                    if(mCursor.getString(3) != null){
                        DateAdded = Functions.stringToDate(mCursor.getString(3));
                    }
                    if(mCursor.getString(4) != null){
                        OnDate = Functions.stringToDate(mCursor.getString(4));
                    }
                    if(mCursor.getString(5) !=null){
                        DateDelivary = Functions.stringToDate(mCursor.getString(5));
                    }

                    Number = mCursor.getString(6);
                    Status = Functions.getOrderStatus(mCursor.getInt(7));
                    Memo = mCursor.getString(8);
                    Fiscal = mCursor.getInt(9)>0;
                    Byer.ID = mCursor.getInt(10);
                    Byer.Name = mCursor.getString(11);
                    Shop.ID = mCursor.getInt(12);
                    Shop.Name = mCursor.getString(13);
                    Manager.ID = mCursor.getInt(14);
                    Qty = mCursor.getDouble(15);
                    Sum = mCursor.getDouble(16);
                    WhareHouse.ID = mCursor.getInt(17);
                    WhareHouse.Name = mCursor.getString(18);
                    LinkID = mCursor.getInt(19);
                    StorCheck = mCursor.getInt(20)>0;
                    StorCheckDate = Functions.stringToDate(mCursor.getString(21));
                }
                mCursor.close();
            }
        }
        catch (Exception ex){
            Functions.showErrorMessage(context, "Order.java", "Load", ex.getMessage());
        }
        finally {
            dbHelper.close();
        }
    }

    // Метод сохраняет заявку
    public boolean Save(Context context){
        SQLiteDatabase dbHelper = new DBHelper(context).getWritableDatabase();
        if(ID==0){
            Guid= UUID.randomUUID().toString();
            // дата добавления в бд
            Calendar c = Calendar.getInstance();
            String dateAdded = Functions.getDateLongForSql(c.getTime());

            try{
                String sql = "INSERT INTO ORDERS(Guid,DateAdded,OnDate,OrderSum,ByerID,Status,AgentID,WhareHouseID)\n" +
                        "VALUES('"+Guid+"','"+dateAdded +"','"+Functions.getDateForSql(OnDate)+"',0,"+Byer.ID+",0,"+Manager.ID+","+WhareHouse.ID+")";
                dbHelper.execSQL(sql);

                Cursor mCursor =dbHelper.rawQuery("SELECT ID FROM ORDERS WHERE Guid='"+Guid+"'",null);
                if (mCursor != null) {
                    if (mCursor.moveToFirst()) {
                        ID = mCursor.getInt(0);
                    }
                    mCursor.close();
                }
                // обновляем свойства
                Update(context,dbHelper);
            }
            catch(Exception ex){
                Toast.makeText(context,"ОШИБКА запроса =>"+ex.getMessage(),Toast.LENGTH_SHORT);
            }
        }
        else{
            Update(context,dbHelper);
        }

        dbHelper.close();
        return true;
    }

    // Обновляем данные по заявке
    private void Update(Context context,SQLiteDatabase database){
        try{
            // обновляем ЗАЯВКУ
            String sql = "UPDATE ORDERS SET Memo='"+Memo+"',Number='"+Number+"',Status="+Functions.getOrderStatus(Status)+",StoreCheck="+(StorCheck ? 1 : 0)+
                    ",StoreCheckDate='"+Functions.getDateForSql(StorCheckDate)+"',Fiscal="+(Fiscal ? 1 : 0)+",WhareHouseID="+WhareHouse.ID+",ByerID="+Byer.ID+
                    ",ByerShopID="+Shop.ID+",AgentID="+Manager.ID+",Qty="+Qty+",OrderSum="+Sum+",LinkID="+LinkID+",OrderID="+OrderID+",OnDate='"+Functions.getDateForSql(OnDate)
                    +"' where ID="+ID;
            database.execSQL(sql);
        }
        catch(Exception ex){
            Toast.makeText(context,"ОШИБКА запроса =>"+ex.getMessage(),Toast.LENGTH_SHORT);
        }
    }

    // Дессириализуем данные
    public void DesirializeFromJson(Context context,JSONObject jsonObject){

        try {
            JSONArray TreeRoot =jsonObject.getJSONArray("data");
            JSONObject c = TreeRoot.getJSONObject(0);
            ID=c.getInt("ID");
            Guid = c.getString("Guid");
            OrderID = c.getInt("OrderID");
            DateAdded =Functions.stringToDate(c.getString("DateAdded"));
            OnDate = Functions.stringToDate(c.getString("OnDate"));
            DateDelivary = Functions.stringToDate(c.getString("DateDelivary"));
            Number = c.getString("Number");
            Status = Functions.getOrderStatus(c.getInt("Status"));
            Memo = c.getString("Memo");
            Fiscal = c.getBoolean("Fiscal");
            Byer.ID = c.getInt("ByerID");
            Shop.ID = c.getInt("ByerShopID");
            Manager.ID = c.getInt("AgentID");
            Qty = c.getDouble("Qty");
            Sum = c.getDouble("OrderSum");
            WhareHouse.ID = c.getInt("WhareHouseID");
            LinkID = c.getInt("LinkID");
            StorCheck = c.getBoolean("StoreCheck");
            StorCheckDate = Functions.stringToDate(c.getString("StoreCheckDate"));

            // добавляем проводки
            JSONArray shopArray = c.getJSONArray("TransList");
            for(int j=0;j<shopArray.length();j++){
                JSONObject tr = shopArray.getJSONObject(j);
                Transaction transaction = new Transaction();
                transaction.ID = tr.getInt("ID");
                transaction.DateAdded = Functions.stringToDate(tr.getString("DateAdded"));
                transaction.OnDate = Functions.stringToDate(tr.getString("OnDate"));
                transaction.OrderID = tr.getInt("OrderID");
                transaction.Guid = tr.getString("Guid");
                transaction.Good.ID = tr.getInt("GoodID");
                transaction.OrderLinkID = tr.getInt("OrderLinkID");
                transaction.Memo = tr.getString("Memo");
                transaction.Qty = tr.getDouble("Qty");
                transaction.Qty2 = tr.getDouble("Qty2");
                transaction.Qty3 = tr.getDouble("Qty3");
                transaction.Qty4 = tr.getDouble("Qty4");
                transaction.Qty5 = tr.getDouble("Qty5");
                transaction.Fqty = tr.getDouble("Fqty");
                transaction.Fqty2 = tr.getDouble("Fqty2");
                transaction.Price = tr.getDouble("Price");
                transaction.Price2 = tr.getDouble("Price2");
                transaction.Sum = tr.getDouble("Sum");
                transaction.Sum2 = tr.getDouble("Sum2");
                transaction.Number = tr.getInt("Number");
                transaction.Byer.ID = tr.getInt("ByerID");
                transaction.Shop.ID = tr.getInt("ShopID");
                transaction.Manager.ID = tr.getInt("ManagerID");
                transaction.IsFiscal = tr.getBoolean("IsFiscal");
                transaction.Status  =Functions.getOrderStatus(tr.getInt("Status"));

                TransList.add(transaction);
            }

        } catch (JSONException e) {
            Functions.showErrorMessage(context,"Order.java","DesirializeFromJson",e.getMessage());
        }
    }

    // Метод возвращает  строку json объекта
    public String getJsonString(){
        StringBuilder str = new StringBuilder();
/*        str.append('[');*/
        str.append("{\"ID\":"+ID+",\"Guid\":\""+Guid+"\",\"DateAdded\":\""+Functions.getDateLongForSql(DateAdded)+"\",\"OrderID\":"+ OrderID+
        ",\"OnDate\":\""+Functions.getDateLongForSql(OnDate)+"\",\"DateDelivary\":\""+Functions.getDateLongForSql(DateDelivary)+"\",\"Number\":\""+Number+"\""+
        ",\"Status\":"+Functions.getOrderStatus(Status)+",\"Memo\":\""+ Memo + "\",\"Fiscal\":\"" + Fiscal + "\",\"ByerID\":" + Byer.ID +
                ",\"ByerShopID\":" + Shop.ID + ",\"AgentID\":" + Manager.ID + ",\"Qty\":" + Qty + ",\"OrderSum\":" + Sum + ",\"WhareHouseID\":" + WhareHouse.ID +
                ",\"LinkID\":" + LinkID + ",\"StoreCheck\":\"" + StorCheck + "\",\"StoreCheckDate\":\"" + Functions.getDateLongForSql(StorCheckDate) + "\""
        );

        str.append(",\"TransList\":[");
        for(int i=0;i<TransList.size();i++){
            Transaction trans = TransList.get(i);
            str.append("{\"ID\":"+trans.ID+",\"DateAdded\":\""+Functions.getDateLongForSql(trans.DateAdded)+"\",\"OnDate\":\""+Functions.getDateLongForSql(trans.OnDate)
                    +"\",\"OrderID\":"+trans.OrderID+",\"Guid\":\""+trans.Guid+"\",\"GoodID\":"+trans.Good.ID+",\"OrderLinkID\":"+trans.OrderLinkID+",\"Memo\":\""+trans.Memo+"\""+
                    ",\"Qty\":"+trans.Qty+",\"Qty2\":"+trans.Qty2+",\"Qty3\":"+trans.Qty3+",\"Qty4\":"+trans.Qty4+",\"Qty5\":"+trans.Qty5+
                    ",\"Fqty\":"+trans.Fqty+",\"Fqty2\":"+trans.Fqty2+",\"Price\":"+trans.Price+",\"Price2\":"+trans.Price2+",\"Sum\":"+trans.Sum+",\"Sum2\":"+trans.Sum2+
                    ",\"Number\":"+trans.Number+",\"ByerID\":"+trans.Byer.ID+",\"ShopID\":"+trans.Shop.ID+
                    ",\"ManagerID\":"+trans.Manager.ID+",\"IsFiscal\":\""+trans.IsFiscal+"\",\"Status\":"+Functions.getOrderStatus(trans.Status)+"}");
            if(i<TransList.size()-1){
                str.append(",");
            }
        }
        str.append("]}");
        return str.toString();
    }
}
