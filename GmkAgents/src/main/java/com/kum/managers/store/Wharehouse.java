package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

// Склад
public class Wharehouse {

    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;

    public int ID;  // идентификатор склада
    public String Guid; // код репликации
    public String Name;  // наименование склада
    public boolean Default; // склад по умолчанию

    // Склад
    public Wharehouse(Context context){
        appContext= context;
        this.ID = 0;
        this.Guid = "";
        this.Name = "";
        this.Default = false;
    }

    // Склад
    public Wharehouse(Context context,int id){
        appContext= context;
        this.ID = id;
        this.Guid = "";
        this.Name = "";
        this.Default = false;
    }

    // Загрузка свойств
    public void Load(){
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();
        // переменные для query
        String selection = "ID= ?";
        String[] selectionArgs = new String[] {ID+"" };
        String[] columns = new String[]{"ID","GUID","Name","IsDefault"};
        Cursor mCursor =dbHelper.query("WAREHOUSE",columns,selection,selectionArgs,null,null,null);
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                this.ID =mCursor .getInt(0);
                this.Guid = mCursor.getString(1);
                this.Name = mCursor.getString(2);
                this.Default = mCursor.getInt(3)>0;
            }
            mCursor.close();
        }
        dbHelper.close();
    }
}
