package com.kum.managers.store;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kum.managers.datastore.DBHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

// Коллекция покупателей
public class ByerCollection implements List<Byer>,Serializable {
    SQLiteDatabase dbHelper; // работа с БД
    Context appContext;
    List<Byer> byers = new ArrayList<Byer>();

    public ByerCollection(Context context){
        IsLoaded = false;
        appContext= context;
    }

    @Override
    public void add(int i, Byer byer) {
        byers.add(i,byer);
    }

    @Override
    public boolean add(Byer byer) {
        return byers.add(byer);
    }

    @Override
    public boolean addAll(int i, Collection<? extends Byer> coll) {
        return byers.addAll(i,coll);
    }

    @Override
    public boolean addAll(Collection<? extends Byer> coll) {
        return byers.addAll(coll);
    }

    @Override
    public void clear() {
        byers.clear();
    }

    @Override
    public boolean contains(Object o) {
        return byers.contains(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return byers.containsAll(objects);
    }

    @Override
    public Byer get(int i) {
        return byers.get(i);
    }

    @Override
    public int indexOf(Object o) {
        return byers.indexOf(o);
    }

    @Override
    public boolean isEmpty() {
        return byers.isEmpty();
    }

    @Override
    public Iterator<Byer> iterator() {
        return byers.iterator();
    }

    @Override
    public int lastIndexOf(Object o) {
        return byers.lastIndexOf(o);
    }

    @Override
    public ListIterator<Byer> listIterator() {
        return byers.listIterator();
    }

    @Override
    public ListIterator<Byer> listIterator(int i) {
        return byers.listIterator(i);
    }

    @Override
    public Byer remove(int i) {
        return byers.remove(i);
    }

    @Override
    public boolean remove(Object o) {
        return byers.remove(o);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return byers.removeAll(objects);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return byers.retainAll(objects);
    }

    @Override
    public Byer set(int i, Byer byer) {
        return byers.set(i,byer);
    }

    @Override
    public int size() {
        return byers.size();
    }

    @Override
    public List<Byer> subList(int i, int i2) {
        return subList(i,i2);
    }

    @Override
    public Object[] toArray() {
        return byers.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return byers.toArray(ts);
    }

    public boolean IsLoaded; // загружена или нет коллекция

    // Загрузка данных из бд
    public void Load(){
        clear();
        // создаем объект для создания и управления версиями БД
        dbHelper = new DBHelper(appContext).getWritableDatabase();

        // переменные для query
        String[] columns = new String[]{"ID","GUID","Name","SHOPS"};
        Cursor mCursor =dbHelper.query("BYERS",columns,null,null,null,null,"Name2");
        if (mCursor != null) {
            if (mCursor.moveToFirst()) {
                do {
                    Byer byer = new Byer(appContext);
                    byer.ID =mCursor .getInt(0);
                    byer.Guid = mCursor.getString(1);
                    byer.Name = mCursor.getString(2);
                    byer.ShopsCount = mCursor.getInt(3);
                    byers.add(byer);
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }

        IsLoaded = true;
        dbHelper.close();
    }
}
