package com.kum.managers.store;

/**
 * Created by Роман on 27.11.13.
 */
public class Manager {

    public int ID;
    public String Name;  // наименование менеджера
    public String Login;        // имя авторизации пользователя
    public String Password; // пароль

    public Manager(){
        ID=0;
        Name = "";
        Password = "";
        Login = "";
    }

}
