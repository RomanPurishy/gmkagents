package com.kum.managers.store;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kum.managers.R;

/**
 * Адаптер данных по товару
 */
public class GoodListAdapter   extends BaseAdapter {

    // Получаем контейнерный объект View
    private LayoutInflater inflater;
    private GoodCollection goods;  // коллекция товаров

    public GoodListAdapter(Context context,GoodCollection collection){
        super();
        goods = collection;
        inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return goods.size();
    }

    @Override
    public Object getItem(int i) {
        return goods.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View convertView =inflater.inflate(R.layout.order_good_view,null);
        Good good = goods.get(i);
        TextView txtName = (TextView)convertView.findViewById(R.id.good_name);
        txtName.setText(good.Name);
        return convertView;
    }
}
