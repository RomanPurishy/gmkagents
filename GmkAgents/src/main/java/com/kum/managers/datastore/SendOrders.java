package com.kum.managers.datastore;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;

import com.kum.managers.Common.Functions;
import com.kum.managers.Interfaces.IAsynkTask;
import com.kum.managers.JSONParser;
import com.kum.managers.R;
import com.kum.managers.logs.LogErrorDialog;
import com.kum.managers.logs.LogErrorItem;
import com.kum.managers.logs.LogErrorItemCollection;
import com.kum.managers.logs.LogErrorItemTypeEnums;
import com.kum.managers.store.Order;
import com.kum.managers.store.OrderCollection;
import com.kum.managers.store.Transaction;
import com.kum.managers.store.Workarea;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

/**
 * Класс отправляет все заявки на сбыт
 */
public class SendOrders {
    SQLiteDatabase _db; // работа с БД
    Context _context;  // контекст приложения
    private ProgressDialog pDialog;
    private OrderCollection _orders;  // коллекция заявок для отправки
    private Hashtable zOperation = new Hashtable();  // операции содержащие ошибки
    JSONParser jsonParser = new JSONParser(); // Creating JSON Parser object
    public LogErrorItemCollection errors = new LogErrorItemCollection();


    // свойство завершения события
    public IAsynkTask onAsynkTask;


    public void setOnCompleteListener(IAsynkTask task){
        onAsynkTask = task;
    }


    public SendOrders(Context context,OrderCollection orders){
        _context = context;
        _orders = orders;
    }

    // Отправка заявок
    public void Send(){
        if(Functions.IsOnline(_context)){
            // получаем DB
            new SenAllTask().execute();
        }
        else{
            Functions.showMessage(_context,R.layout.order,"Ошибка передачи данных по сети. Проверьте доступность интернета", Functions.ToastMessageType.Error,5000);
        }
    }

    // Отправка заявок
    public void CheckBeforeSend(){
        zOperation.clear();
        if(Functions.IsOnline(_context)){
            // получаем DB
            _db = new DBHelper(_context).getWritableDatabase();
            new CheckAllOrdersBeforeSend().execute();
        }
        else{
            Functions.showMessage(_context,R.layout.order,"Ошибка передачи данных по сети. Проверьте доступность интернета", Functions.ToastMessageType.Error,5000);
        }
    }


    // Отправка всех неотравленных заявок
    public class SenAllTask extends AsyncTask<Void, ResponseOrder, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(_context);
            pDialog.setMessage("Отправка зявок...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(_orders.size());
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... urls) {
            int myProgress = 0;
            int count = _orders.size();
            //Log.println(7,"SendOrders.java","doInBackground ВСЕГО ЗАЯВОК=["+count+"] ");
            for(int i=0;i<count;i++){

                ResponseOrder responseOrder = new ResponseOrder();
                myProgress++;
                responseOrder.Progress = myProgress;
                responseOrder.Order =  _orders.get(i);
               // Log.println(7,"SendOrders.java","doInBackground position=["+responseOrder.Progress+"]  orderID=["+responseOrder.Order.ID+"]");
                if(!zOperation.containsKey(responseOrder.Order.ID)){
                    responseOrder.Order.TransList.Load(_context,responseOrder.Order.ID);
                    try{
                        String strJson =responseOrder.Order.getJsonString();
                        DefaultHttpClient httpClient = new DefaultHttpClient();
                        HttpPost httpPost = new HttpPost( Functions.Domen+"manager/order-send");
                       // Log.println(7,"SendOrders.java","doInBackground  ЗАЯВКА ++  position=["+responseOrder.Progress+"]  orderID=["+responseOrder.Order.ID+"]");
                        List<NameValuePair> parms = new ArrayList<NameValuePair>();
                        parms.add(new BasicNameValuePair("Order",Uri.encode(strJson)));
                        parms.add(new BasicNameValuePair("Ticket", Workarea.Ticket));
                        httpPost.setEntity(new UrlEncodedFormEntity(parms));
                        responseOrder.Response = httpClient.execute(httpPost);
                       // _currentResponse = null;
                    }
                    catch (Exception ex){
                        WriteToLog("Ошибка запроса <"+ex.getMessage()+">",responseOrder.Order);
                    }
                }
                else{
                    responseOrder.Response = null;
                }

                publishProgress(responseOrder);
            }
            return "ГОТОВО";
        }

        @Override
        protected void onProgressUpdate(ResponseOrder... responseOrders) {
            super.onProgressUpdate(responseOrders);
            ResponseOrder  responseOrder= responseOrders[0];
            //Log.println(7,"SendOrders.java","onProgressUpdate position=["+responseOrder.Progress+"]  orderID=["+responseOrder.Order.ID+"]");
            pDialog.setMessage("Отправляется заявка ...");
            pDialog.setProgress(responseOrder.Progress);
            if(responseOrder.Response != null){
                try{
                    Header header= responseOrder.Response.getFirstHeader("UpdateStatus");
                    if(header !=null){
                        if(header.getValue().equals("ERROR")){
                            Header header2= responseOrder.Response.getFirstHeader("ERRORMESSAGE");
                            if(header2 != null){
                                WriteToLog(Uri.decode(header2.getValue()).replace("---"," "), responseOrder.Order);
                            }
                            else{
                                WriteToLog("Ошибка запроса <ДОСТУП ЗАПРЕЩЕН>",responseOrder.Order);
                            }
                        }
                        else{
                            // обновляем БД и
                            JSONParser parser = new JSONParser();
                            Order newOrder= new Order();
                            newOrder.DesirializeFromJson(_context,parser.GetJsonObject(responseOrder.Response));

                            // обновляем заявку
                            responseOrder.Order.OrderID = newOrder.OrderID;
                            responseOrder.Order.LinkID = newOrder.LinkID;
                            responseOrder.Order.Status = newOrder.Status;
                            responseOrder.Order.Number = newOrder.Number;
                            responseOrder.Order.Save(_context);
                            int i=0;
                            for(Transaction tr : responseOrder.Order.TransList){
                                Transaction trans = newOrder.TransList.get(i);
                                i++;
                                tr.Status = trans.Status;
                            }
                            responseOrder.Order.TransList.Update(_context);
                            // обновили
                        }
                    }
                }
                catch (Exception ex){
                    WriteToLog("Ошибка запроса <"+ex.getMessage()+">",responseOrder.Order);
                }
            }
        }

        // после окончания загрузки
        @Override
        protected void onPostExecute(String result) {

            if(onAsynkTask != null){
                onAsynkTask.TaskComplete();
            }

            pDialog.dismiss();
        }
    }

    // Класс ответа о синхроном запросе
    public class ResponseOrder{

        public ResponseOrder(){
            Progress = 0;
        }

        public Order Order;
        public HttpResponse Response;
        public Integer Progress;
    }

    // Метод проверяет можно ли отправить заявку
    public class CheckAllOrdersBeforeSend extends  AsyncTask<Void,ResponseOrder,String>{


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            errors.clear();
            pDialog = new ProgressDialog(_context);
            pDialog.setMessage("Отправка зявок...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(_orders.size());
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... urls) {
            int myProgress = 0;
            int count = _orders.size();

            for(int i=0;i<count;i++){
                ResponseOrder responseOrder = new ResponseOrder();
                responseOrder.Order =  _orders.get(i);
                responseOrder.Order.TransList.Load(_context,responseOrder.Order.ID);
                if(responseOrder.Order.TransList.size()>0){
                    // проверка даты отправки
                    Date dateNow = Functions.getShortDate();
                    Date orderDate = Functions.addDaysToShortDate(dateNow,1);
                    if(responseOrder.Order.OnDate.getTime()<orderDate.getTime()){
                        WriteToLog("Дата заявки не может быть меньше чем ["+Functions.getDateShortString(responseOrder.Order.OnDate)+"]",responseOrder.Order);
                    }
                    else{
                        try{
                            String strJson =responseOrder.Order.getJsonString();
                            DefaultHttpClient httpClient = new DefaultHttpClient();
                            HttpPost httpPost = new HttpPost( Functions.Domen+"manager/order-cansend");

                            List<NameValuePair> parms = new ArrayList<NameValuePair>();
                            parms.add(new BasicNameValuePair("Order",Uri.encode(strJson)));
                            parms.add(new BasicNameValuePair("Ticket", Workarea.Ticket));
                            httpPost.setEntity(new UrlEncodedFormEntity(parms));
                            responseOrder.Response= httpClient.execute(httpPost);
                        }
                        catch (Exception e){
                            WriteToLog("Ошибка запроса <"+e.getMessage()+">",responseOrder.Order);
                        }
                    }
                }
                else{
                    WriteToLog("Список товаров в заказе пуст. Добавьте вначале товары",responseOrder.Order);
                }
                myProgress++;
                responseOrder.Progress = myProgress;
                publishProgress(responseOrder);
            }
            if(errors.size()>0){
                return "ERROR";
            }
            return "SUCCESS";
        }

        @Override
        protected void onProgressUpdate(ResponseOrder... responseOrders) {
            super.onProgressUpdate(responseOrders);
            ResponseOrder responseOrder = responseOrders[0];
            pDialog.setMessage("Отправляется заявка ...");
            pDialog.setProgress(responseOrder.Progress);
            if(responseOrder.Response != null){
                try{
                    Header header= responseOrder.Response.getFirstHeader("UpdateStatus");
                    if(header !=null){
                        if(header.getValue().equals("ERROR")){
                            WriteToLog("ДОСТУП ЗАПРЕЩЕН",responseOrder.Order);
                        }
                        else{
                            JSONObject jsonObject= jsonParser.GetJsonObject(responseOrder.Response);
                            LogErrorItemCollection errors2 = new LogErrorItemCollection();
                            errors2.DesirializeFromJson(jsonObject);
                            if(errors2.size()>0){
                                LogErrorItem errorItem = new LogErrorItem();
                                errorItem.Type = LogErrorItemTypeEnums.Header;
                                errorItem.Description = "Заявка для ["+responseOrder.Order.Shop.Name+"]";
                                zOperation.put(responseOrder.Order.ID,responseOrder.Order.Byer.Name);
                                for (int j=0;j<errors2.size();j++){
                                    errors.add(errors2.get(j).Clone());
                                }
                            }
                        }
                    }
                }
                catch (Exception ex){
                    WriteToLog("Ошибка запроса <"+ex.getMessage()+">",responseOrder.Order);
                }
            }
        }

        // после окончания загрузки
        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            Send();
        }


    }

    private void WriteToLog(String message,Order od){
        LogErrorItem errorItem = new LogErrorItem();
        errorItem.Type = LogErrorItemTypeEnums.Header;
        errorItem.Description = "Заявка для ["+od.Shop.Name+"]";
        errors.add(errorItem);
        errorItem = new LogErrorItem();
        errorItem.Description = message;
        errors.add(errorItem);
        zOperation.put(od.ID,od.Byer.Name);
    }

    public void ShowErrors(android.support.v4.app.FragmentManager manager){
        if(errors.size()>0){
            LogErrorDialog dlg = new LogErrorDialog();
            dlg.setErrorList(errors);
            dlg.show(manager,"LogErrorWindow");
        }

    }
}
