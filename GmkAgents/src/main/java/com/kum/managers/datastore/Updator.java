package com.kum.managers.datastore;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.kum.managers.Common.Functions;
import com.kum.managers.ItaskComleteEvent;
import com.kum.managers.JSONParser;
import com.kum.managers.R;
import com.kum.managers.store.Workarea;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

// Класс для обновления данных
public class Updator  {

    SQLiteDatabase dbHelper; // работа с БД
    Context context;  // контекст приложения
    int LayoutId ; // идентификатор окна

    // Конструктор
    public Updator(Context cont,int layoutId){
        context = cont;
        LayoutId = layoutId;
    }

    // свойство завершения события
    public ItaskComleteEvent Compete;

    // Progress Dialog
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser(); // Creating JSON Parser object

    // метод запускает обновления
    public void Update(){
        if(Functions.IsOnline(context)){
            // получаем DB
            dbHelper = new DBHelper(context).getWritableDatabase();

            new DownloadFilesTask().execute(new String[]{
                    Functions.Domen+"api/"+ Workarea.Ticket+"/byers/",
                    Functions.Domen+"api/"+ Workarea.Ticket+"/wharehouse",
                    Functions.Domen+"api/"+ Workarea.Ticket+"/good",
                    Functions.Domen+"api/"+ Workarea.Ticket+"/ByersAndGoods"});
        }
        else{
            Functions.showMessage(context,"СЕТИ НЕ ДОСТУПНЫ",5000);
        }

    }

    String error = "";
    // Асинхронная задача
    private class DownloadFilesTask extends AsyncTask<String, Integer, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Загрузка покупателей ...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(4);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... urls) {
            int myProgress = 0;
            int count = urls.length;

            for(int i=0;i<count;i++){
                error = "";
                switch (i){
                    case 0: LoadByers(urls[i]); break;
                    case 1: LoadWareHouse(urls[i]); break;
                    case 2: LoadGoods(urls[i]);break;
                    case 3: LoadGoodsPopular(urls[i]); break;
                }
                if(error.length()>0){
                    break;
                }
                myProgress++;
               publishProgress(myProgress);
            }
            return "ГОТОВО";
        }

        // ШАГ - 1 обновляем покупателей
        private void LoadByers(String url){
            List<NameValuePair> prm = new ArrayList<NameValuePair>();
            JSONObject json = jsonParser.makeHttpRequest(url, "GET", prm,context,LayoutId);
            JSONArray TreeRoot;
            if(json==null){
                error =context.getResources().getString(R.string.network_error);
                return;
            }
            // удаление данных из таблиц
            dbHelper.delete("BYERS",null,null);
            dbHelper.delete("SHOPS",null,null);

            ContentValues values = new ContentValues();

            try {
                TreeRoot = json.getJSONArray("data");
                // looping through All messages
                for (int i = 0; i < TreeRoot.length(); i++) {
                    JSONObject c = TreeRoot.getJSONObject(i);
                    // Storing each json item in variable
                    Integer id = c.getInt("ID");
                    String guid = c.getString("Guid");
                    String name = c.getString("Name");
                    int shopsCount = c.getInt("ShopsCount");

                    // добавляем данные в БД
                    values.put("ID",id);
                    values.put("GUID",guid);
                    values.put("Name",name);
                    values.put("Name2",name.toLowerCase());
                    values.put("SHOPS",shopsCount);
                    dbHelper.insert("BYERS",name,values);

                    // добавляем грузополучателей
                    JSONArray shopArray = c.getJSONArray("Shops");
                    ContentValues shopValues = new ContentValues();
                    for(int j=0;j<shopArray.length();j++){
                        JSONObject sh = shopArray.getJSONObject(j);
                        Integer sID = sh.getInt("ID");
                        String sGuid = sh.getString("Guid");
                        String sName = sh.getString("Name");
                        shopValues.put("ID",sID);
                        shopValues.put("GUID",sGuid);
                        shopValues.put("Name",sName);
                        shopValues.put("Name2",sName.toLowerCase());
                        shopValues.put("BYER_ID",id);
                        dbHelper.insert("SHOPS",sName,shopValues);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        // Загрузка складов
        private void LoadWareHouse(String url){

            List<NameValuePair> prm = new ArrayList<NameValuePair>();
            JSONObject json = jsonParser.makeHttpRequest(url, "GET", prm,context,LayoutId);
            if(json==null){
                error =context.getResources().getString(R.string.network_error);
                return;
            }
            dbHelper.delete("WAREHOUSE",null,null);
            JSONArray TreeRoot;
            ContentValues values = new ContentValues();

            try {
                TreeRoot = json.getJSONArray("data");
                for (int i = 0; i < TreeRoot.length(); i++) {
                    JSONObject c = TreeRoot.getJSONObject(i);

                    // добавляем данные в БД
                    values.put("ID",c.getInt("ID"));
                    values.put("GUID", c.getString("Guid"));
                    values.put("Name", c.getString("Name"));
                    values.put("Name", c.getString("Name").toLowerCase());
                    values.put("IsDefault",c.getBoolean("Default"));
                    dbHelper.insert("WAREHOUSE","ID",values);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Загрузка товаров
        private void LoadGoods(String url){

            List<NameValuePair> prm = new ArrayList<NameValuePair>();
            JSONObject json = jsonParser.makeHttpRequest(url, "GET", prm,context,LayoutId);
            if(json==null){
                error =context.getResources().getString(R.string.network_error);
                return;
            }
            dbHelper.delete("GOODS",null,null);
            JSONArray TreeRoot;
            ContentValues values = new ContentValues();

            try {
                TreeRoot = json.getJSONArray("data");
                for (int i = 0; i < TreeRoot.length(); i++) {
                    JSONObject c = TreeRoot.getJSONObject(i);

                    // добавляем данные в БД
                    values.put("ID",c.getInt("ID"));
                    values.put("GUID", c.getString("Guid"));
                    values.put("Name", c.getString("Name"));
                    values.put("Name2", c.getString("Name").toLowerCase());
                    values.put("Price",c.getDouble("Price"));
                    values.put("WhareHouseID",c.getInt("WhareHouseID"));
                    dbHelper.insert("GOODS","ID",values);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // загружаем популярные продукты
        private void LoadGoodsPopular(String url){

            List<NameValuePair> prm = new ArrayList<NameValuePair>();
            JSONObject json = jsonParser.makeHttpRequest(url, "GET", prm,context,LayoutId);
            if(json==null){
                error =context.getResources().getString(R.string.network_error);
                return;
            }
            dbHelper.delete("GOODS_POPULAR",null,null);
            JSONArray TreeRoot;
            ContentValues values = new ContentValues();

            try {
                TreeRoot = json.getJSONArray("data");
                for (int i = 0; i < TreeRoot.length(); i++) {
                    JSONObject c = TreeRoot.getJSONObject(i);

                    // добавляем данные в БД
                    values.put("GoodID",c.getInt("GoodID"));
                    values.put("ByerID", c.getInt("ByerID"));
                    values.put("ByerShopID",c.getInt("ByerShopID"));
                    dbHelper.insert("GOODS_POPULAR",null,values);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            if(error.length()>0){
                Functions.showMessage(context,LayoutId,error, Functions.ToastMessageType.Error,5000);
            }
            setProgressPercent(progress[0]);
        }


        // прогресс загрузки
        protected void setProgressPercent(Integer progress) {
            switch (progress){
                case 1:
                    pDialog.setMessage("Загрузка складов ...");
                    break;
                case 2:
                    pDialog.setMessage("Загрузка товаров ...");
                    break;
                case 3:
                    pDialog.setMessage("Загрузка популярной продукции для покупателя ...");
                    break;
/*                case 4:
                    //pDialog.setMessage("Загрузка популярной продукции для покупателя ...");
                    break;*/
            }
            pDialog.setProgress(progress);
        }

        // после окончания загрузки
        @Override
        protected void onPostExecute(String result) {
            pDialog.dismiss();
            if(error.length()>0){
                Functions.showMessage(context,LayoutId,error, Functions.ToastMessageType.Error,5000);
            }
            Compete.TaskDone();
        }
    }
}
