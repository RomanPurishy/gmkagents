package com.kum.managers.datastore;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by PURISHI on 28.10.13.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 6;  // версия базы данных
    private static final String DB_NAME = "GMK_AGENTS.db"; // База данных
    public static final String BYERS="BYERS";   // таблица покупателей
    public static final String SHOPS="SHOPS";   // таблица магазинов покупателей
    public static final String WAREHOUSE="WAREHOUSE";  // таблица складов предприятия
    public static final String GOODS="GOODS";       // таблица товаров


    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context,DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // создаем таблицу с полями
        db.execSQL("create table "+BYERS+" (ID integer PRIMARY KEY  NOT NULL ,GUID TEXT,Name TEXT,Name2 TEXT,SHOPS integer);");
        db.execSQL("create table "+SHOPS+" (ID integer PRIMARY KEY  NOT NULL ,GUID TEXT,Name TEXT,Name2 TEXT,BYER_ID integer);");
        Change4(db);
        Change6(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion){
            case 2:
                Change4(db);
                Change6(db);
                break;
            case 4:
                Change4(db);
                Change6(db);
                break;
            case 5:
                Change6(db);
                break;
        }

    }

    private void Change6(SQLiteDatabase db){
        db.execSQL("CREATE  TABLE  IF NOT EXISTS \"GOODS_POPULAR\" (\"GoodID\" INTEGER NOT NULL , \"ByerID\" INTEGER NOT NULL , \"ByerShopID\" INTEGER)");
    }

    private void Change4(SQLiteDatabase db){
        db.execSQL("create table "+WAREHOUSE+" (ID integer PRIMARY KEY  NOT NULL ,GUID text,Name TEXT,Name2 TEXT,IsDefault NUMERIC);");
        db.execSQL("create table "+GOODS+" (ID integer PRIMARY KEY  NOT NULL ,GUID text,Name TEXT,Name2 TEXT,Price NUMERIC,WhareHouseID INTEGER NOT NULL);");
        db.execSQL("CREATE  TABLE  IF NOT EXISTS  ORDERS (ID INTEGER PRIMARY KEY  NOT NULL  UNIQUE ,"+
                " Guid TEXT NOT NULL  UNIQUE,OrderID INTEGER , DateAdded TEXT NOT NULL , OnDate TEXT NOT NULL ,"+
                " DateDelivary TEXT, Number TEXT, Status INTEGER NOT NULL , Memo TEXT, Fiscal NUMERIC, "+
                " ByerID INTEGER NOT NULL , ByerShopID INTEGER, AgentID INTEGER NOT NULL ,Qty NUMERIC,OrderSum NUMERIC, "
                +"WhareHouseID INTEGER NOT NULL , LinkID INTEGER, StoreCheck NUMERIC,StoreCheckDate TEXT)");
        db.execSQL("CREATE TABLE  IF NOT EXISTS \"JOURNAL\" (\"ID\" INTEGER PRIMARY KEY  NOT NULL  UNIQUE , \"Guid\" TEXT NOT NULL ,"+
                " \"DateAdded\" TEXT NOT NULL , \"OrderID\" INTEGER NOT NULL , \"OrderLinkID\" INTEGER NOT NULL, \"GoodID\" INTEGER NOT NULL , \"Date\" TEXT NOT NULL ,"+
                " \"Memo\" TEXT, \"Qty\" NUMERIC NOT NULL , \"Qty2\" NUMERIC, \"Qty3\" NUMERIC, \"Qty4\" NUMERIC, \"Qty5\" NUMERIC,"+
                " \"Fqty\" NUMERIC, \"Fqty2\" NUMERIC, \"Price\" NUMERIC, \"Price2\" NUMERIC, \"Sum\" NUMERIC, \"Sum2\" NUMERIC, \"IsFiscal\" NUMERIC NOT NULL , "+
                "\"Number\" INTEGER NOT NULL,\"ByerID\" INTEGER NOT NULL,\"ByerShopID\" INTEGER NOT NULL,\"Status\" INTEGER NOT NULL,"+
                "\"AgentID\" INTEGER NOT NULL)");
    }
}