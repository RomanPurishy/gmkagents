package com.kum.managers;

import android.content.Context;

import android.os.Environment;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.kum.managers.Common.Functions;
import com.kum.managers.Helpers.ExternalStorage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.util.Map;


/**
 * Created by PURISHI on 23.10.13.
 */
public class SettingsActionProvider extends android.support.v4.view.ActionProvider implements PopupMenu.OnMenuItemClickListener {

    private Context mContext;

    public SettingsActionProvider(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public View onCreateActionView() {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(R.drawable.config32);
        final android.support.v7.widget.PopupMenu menu = new android.support.v7.widget.PopupMenu(mContext, imageView);
        menu.inflate(R.menu.configs);
        menu.setOnMenuItemClickListener(this);

        try {
            Field[] fields = menu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(menu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        imageView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                menu.show();
            }
        });

        return imageView;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item){
        switch (item.getItemId()){
            case R.id.action_backup:  // создаем бекап бд
                BackUpDataBase();
                return true;
        }
        Toast.makeText(mContext, "I was clicked! "+item.getItemId(), Toast.LENGTH_SHORT).show();
        return true;
    }

    // создаем копию БД
    private void BackUpDataBase(){
        try {
            File sd = Environment.getExternalStorageDirectory();
            Map<String, File> externalLocations = ExternalStorage.getAllStorageLocations();
            File externalSdCard = externalLocations.get(ExternalStorage.EXTERNAL_SD_CARD);

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getContext().getPackageName() + "/databases/GMK_AGENTS.db";
                String backupDBPath = "gmkbackup.sqlite";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(externalSdCard, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }
        Functions.showMessage(getContext(), R.layout.activity_main, "База успешно сохранена", Functions.ToastMessageType.Info, 5000);
    }
}
