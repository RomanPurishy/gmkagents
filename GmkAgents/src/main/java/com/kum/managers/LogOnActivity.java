package com.kum.managers;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.kum.managers.Common.ConfigsDialog;
import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.DBHelper;
import com.kum.managers.store.Workarea;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Панель авторизации
 */
public class LogOnActivity extends Activity {

    private TextView txtLogin;
    private TextView txtPassword;
    private CheckBox chRememberMe;
    private Button btnLogin;
    private Button btnExit;
    private Workarea wrk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);

        btnLogin = (Button)findViewById(R.id.btn_login);
        btnExit = (Button)findViewById(R.id.btn_exit);
        txtLogin = (TextView)findViewById(R.id.txt_login_name);
        txtPassword = (TextView)findViewById(R.id.txt_login_password);
        chRememberMe = (CheckBox)findViewById(R.id.ch_remeber);

        wrk = new Workarea();
        wrk.ReadConfigs(getApplicationContext());
        txtLogin.setText(Workarea.Manager.Login);
        txtPassword.setText(Workarea.Manager.Password);
        chRememberMe.setChecked(Workarea.RememberMe);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Workarea.Manager.Login = txtLogin.getText().toString();
                Workarea.Manager.Password = txtPassword.getText().toString();
                Workarea.RememberMe = chRememberMe.isChecked();
                Workarea.Ticket = "";

                if(Workarea.Manager.Login.length()>0 && Workarea.Manager.Password.length()>0){
                    // авторизация
                    new LoginAsynkTask().execute();
                }
                else{
                    Functions.showMessage(getApplicationContext(),R.layout.login, "Укажите логин и пароль", Functions.ToastMessageType.Info, 5000);
                }
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            setResult(RESULT_CANCELED);
            finish();
            }
        });

        Button btnSelectIp = (Button)findViewById(R.id.btn_select_ip);
        if(btnSelectIp != null){
            btnSelectIp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ConfigsDialog configsDialog = new ConfigsDialog();
                    configsDialog.show(getFragmentManager(),null);
                }
            });
        }
    }

    private ProgressDialog pDialog;
    private class LoginAsynkTask extends AsyncTask<String,Integer,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            txtLogin.setEnabled(false);
            txtPassword.setEnabled(false);
            btnLogin.setEnabled(false);
            btnExit.setEnabled(false);
            chRememberMe.setEnabled(false);
            pDialog = new ProgressDialog(LogOnActivity.this);
            pDialog.setMessage("Авторизация ...");
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        }


        @Override
        protected String doInBackground(String... params) {
            int myProgress =0;
            publishProgress(myProgress);
            return TryToLogin();
        }

        @Override
        protected void onPostExecute(String result){
            pDialog.dismiss();
            if(result.length()>0){
                Functions.showMessage(getApplicationContext(),R.layout.login,result, Functions.ToastMessageType.Error,5000);
            }
            else{
                wrk.SaveConfigs(getApplicationContext());
            }
            txtLogin.setEnabled(true);
            txtPassword.setEnabled(true);
            btnLogin.setEnabled(true);
            btnExit.setEnabled(true);
            chRememberMe.setEnabled(true);
        }


    }

    // Попытка входа
    private String TryToLogin()  {
       DBHelper dbHelper = new  DBHelper(getApplicationContext());
       dbHelper.getWritableDatabase();
        JSONParser jsonParser = new JSONParser();
        List<NameValuePair> prm = new ArrayList<NameValuePair>();
        String login = Uri.encode(Workarea.Manager.Login);
        String passwd = Uri.encode(Workarea.Manager.Password);
        String url =Functions.Domen+"manager/logon/"+login+"/"+passwd;
/*        prm.add(new BasicNameValuePair("login",Uri.encode(Workarea.Manager.Login)));
        prm.add(new BasicNameValuePair("password",Uri.encode(Workarea.Manager.Password)));*/

        JSONObject json = jsonParser.makeHttpRequest(url, "GET", prm,getApplicationContext(),R.layout.login);
        if(json==null){
            return getApplicationContext().getResources().getString(R.string.network_error);
        }
        JSONArray TreeRoot;

        try{
            TreeRoot = json.getJSONArray("data");

            for (int i = 0; i < TreeRoot.length(); i++) {
                JSONObject c = TreeRoot.getJSONObject(i);
                Workarea.Manager.ID= c.getInt("ID");
                Workarea.Ticket = c.getString("Guid");
                Workarea.Manager.Name = c.getString("Name");
            }
            if(Workarea.Manager.ID ==0){
                return "Ошибка авторизации. Возможно неверно указано имя пользователя и пароль";
            }
            else{
                setResult(RESULT_OK);
                finish();
            }
        } catch (Exception e) {
            Workarea.Manager.ID=0;
            Workarea.Ticket = "";
            Workarea.Manager.Name = "";
            return e.getMessage();
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        // убираем реализацию чтобы заблокировать кнопку назад
        setResult(RESULT_CANCELED);// закрываем приложение
        finish();
    }

}
