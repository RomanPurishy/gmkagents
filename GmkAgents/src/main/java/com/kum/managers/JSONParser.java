package com.kum.managers;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by PURISHI on 24.10.13.
 */
public class JSONParser {
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }

    /**
     * @param url
     * @param method
     * @param params
     * @return
     */
    public String GetJSON(String url, String method,List<NameValuePair> params) {
        // Making HTTP request
        try {
            // check for request method
            if(method == "POST"){
                // request method is POST
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            }else if(method == "GET"){
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            //BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // return JSON String
        return json;
    }

    /**
     * Function get json from url by
     * making HTTP POST or GET mehtod
     *
     * @param url
     * @param method
     * @param params
     * @return
     */
    public JSONObject makeHttpRequest(String url, String method,List<NameValuePair> params,Context context,int layoutId) {
        // Making HTTP request
        try {
            // check for request method
            if(method == "POST"){
                // request method is POST
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                httpPost.setEntity(new UrlEncodedFormEntity(params));

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            }else if(method == "GET"){
                // request method is GET
                DefaultHttpClient httpClient = new DefaultHttpClient();
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            //Functions.showMessage(context,layoutId,"Ошибка подключения к БД. Время ожидание истекло. Возможно сервер не отвечает!!", Functions.ToastMessageType.Error,5000);
            return null;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
           // Functions.showMessage(context,layoutId,"Ошибка подключения к БД. Время ожидание истекло. Возможно сервер не отвечает!!", Functions.ToastMessageType.Error,5000);
            return null;
        } catch (IOException e) {
            e.printStackTrace();
           // Functions.showMessage(context,layoutId,"Ошибка подключения к БД. Время ожидание истекло. Возможно сервер не отвечает!!", Functions.ToastMessageType.Error,5000);
            return null;
        }
        catch (Exception e){
            e.printStackTrace();
           // Functions.showMessage(context,layoutId,"Ошибка подключения к БД. Время ожидание истекло. Возможно сервер не отвечает!!", Functions.ToastMessageType.Error,5000);
            return null;
        }

        try {
/*            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);*/
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);

            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            String str=sb.toString();

            if(str.length()>0 && str.startsWith("{")){
                json ="{\"data\":[" +str+"]}";
            }
            else{
                json ="{\"data\":" +str+"}";
            }
            //json = "{\"data\":[{\"ID\":21,\"Guid\":\"84d0eb91-bbf9-4283-b3bd-b844328b7685\",\"DateAdded\":\"2013-04-04T10:10:50.193\",\"Name\":\"Аверіна С.В.\",\"Code\":\"\",\"TownBind\":{\"ID\":0,\"Name\":\"\",\"ParentRegionID\":0},\"Email\":\"\",\"Memo\":\"\",\"RegNo\":\"\",\"VatNo\":\"\",\"WWW\":\"\",\"Country\":\"\",\"ParentFolderID\":0,\"ShopsCount\":5,\"InTrash\":false,\"Shops\":[],\"ErrorMessage\":\"\"},{\"ID\":2486,\"Guid\":\"4db6b35c-03fa-4ee3-beed-b03fa46aa1be\",\"DateAdded\":\"2013-08-15T23:00:00.35\",\"Name\":\"Аверіна Т.Д.\",\"Code\":\"\",\"TownBind\":{\"ID\":0,\"Name\":\"\",\"ParentRegionID\":0},\"Email\":\"\",\"Memo\":\"\",\"RegNo\":\"\",\"VatNo\":\"\",\"WWW\":\"\",\"Country\":\"\",\"ParentFolderID\":0,\"ShopsCount\":3,\"InTrash\":false,\"Shops\":[],\"ErrorMessage\":\"\"}]}";
           // json= "{\"res\":[{\"k20\":\"v20\",\"k10\":\"v10\"},{\"k11\":\"v11\",\"k21\":\"v21\"},{\"k22\":\"v22\",\"k12\":\"v12\"}]}";
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }

    public JSONObject GetJsonObject(HttpResponse result){

        try {
            HttpEntity httpEntity = result.getEntity();
            is  = httpEntity.getContent();
/*            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);*/
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"), 8);

            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            String str=sb.toString();

            if(str.length()>0 && str.startsWith("{")){
                json ="{\"data\":[" +str+"]}";
            }
            else{
                json ="{\"data\":" +str+"}";
            }
            //json = "{\"data\":[{\"ID\":21,\"Guid\":\"84d0eb91-bbf9-4283-b3bd-b844328b7685\",\"DateAdded\":\"2013-04-04T10:10:50.193\",\"Name\":\"Аверіна С.В.\",\"Code\":\"\",\"TownBind\":{\"ID\":0,\"Name\":\"\",\"ParentRegionID\":0},\"Email\":\"\",\"Memo\":\"\",\"RegNo\":\"\",\"VatNo\":\"\",\"WWW\":\"\",\"Country\":\"\",\"ParentFolderID\":0,\"ShopsCount\":5,\"InTrash\":false,\"Shops\":[],\"ErrorMessage\":\"\"},{\"ID\":2486,\"Guid\":\"4db6b35c-03fa-4ee3-beed-b03fa46aa1be\",\"DateAdded\":\"2013-08-15T23:00:00.35\",\"Name\":\"Аверіна Т.Д.\",\"Code\":\"\",\"TownBind\":{\"ID\":0,\"Name\":\"\",\"ParentRegionID\":0},\"Email\":\"\",\"Memo\":\"\",\"RegNo\":\"\",\"VatNo\":\"\",\"WWW\":\"\",\"Country\":\"\",\"ParentFolderID\":0,\"ShopsCount\":3,\"InTrash\":false,\"Shops\":[],\"ErrorMessage\":\"\"}]}";
            // json= "{\"res\":[{\"k20\":\"v20\",\"k10\":\"v10\"},{\"k11\":\"v11\",\"k21\":\"v21\"},{\"k22\":\"v22\",\"k12\":\"v12\"}]}";
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;
    }
}