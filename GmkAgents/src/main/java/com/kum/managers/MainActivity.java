package com.kum.managers;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kum.managers.Common.ConfigsDialog;
import com.kum.managers.Common.Functions;
import com.kum.managers.datastore.Updator;
import com.kum.managers.fragments.ByerListFragment;
import com.kum.managers.fragments.OrdersFragment;
import com.kum.managers.fragments.PeriodDialog;
import com.kum.managers.fragments.ShopsFragment;
import com.kum.managers.store.Byer;
import com.kum.managers.store.OrderStatus;
import com.kum.managers.store.Period;
import com.kum.managers.store.Workarea;

// Главное окно приложения
public class MainActivity extends ActionBarActivity implements ItaskComleteEvent {

    android.support.v4.app.FragmentTransaction fTrans;
    // ФРАГМЕНТЫ
    ByerListFragment frgmByers;     // фрагмент покупателей

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CustomActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private MainMenuAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        Period.setNow();
        frgmByers = new ByerListFragment();

        frgmByers.setOnByerSelectListener(new ByerListFragment.OnByerSelectListener() {
            @Override
            public void OnByerSelected(Byer byer) {
                ShopsFragment fragment = new ShopsFragment();
                fragment.setByer(byer);
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                fTrans.replace(R.id.content_frame, fragment);
                fTrans.addToBackStack("byers_fragment");
                fTrans.commit();
            }
        });

        mTitle  = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        _initMenu();
        mDrawerToggle = new CustomActionBarDrawerToggle(this, mDrawerLayout);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        setTitle(R.string.ns_menu_snippet7);
        LoadByers(false);

        Workarea workarea = new Workarea();
        workarea.ReadConfigs(getApplicationContext());      // читаем данные конфигурации
        // проверяем есть ли авторизация и если нет открываем окно запроса
        if(Workarea.Manager.ID==0 || Workarea.RememberMe==false){
            Intent intent = new Intent(this, LogOnActivity.class);
            startActivityForResult(intent,Workarea.LOGON_ACTIVITY);
        }
    }

    // Инициализация меню
    private void _initMenu() {
        mAdapter = new MainMenuAdapter(this);
        mAdapter.addHeader(getResources().getString(R.string.main_menu_header1));

        // покупатели
        MainMenuItem mnu = new MainMenuItem(getString(R.string.ns_menu_snippet1),R.drawable.byers32,1);
        mAdapter.addItem(mnu);

        mAdapter.addHeader(getString(R.string.main_menu_header2));
        // последние заявки
        mnu =  new MainMenuItem(getString(R.string.ns_menu_snippet7),R.drawable.order_last,2);
        mAdapter.addItem(mnu);
        // новые заявки
        mnu =  new MainMenuItem(getString(R.string.ns_menu_snippet2),R.drawable.order_new32,3);
        mAdapter.addItem(mnu);
        // отправленные на сбыт
        mnu =  new MainMenuItem(getString(R.string.ns_menu_snippet3),R.drawable.order_sended32,4);
        mAdapter.addItem(mnu);
        // выполненые
        mnu =  new MainMenuItem(getString(R.string.ns_menu_snippet6),R.drawable.order_executed32,5);
        mAdapter.addItem(mnu);

        mDrawerList = (ListView) findViewById(R.id.menu_drawer);
        if (mDrawerList != null)
            mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        // Set up ShareActionProvider's default share intent
/*
        MenuItem shareItem = menu.findItem(R.id.action_share);
        mShareActionProvider = (ShareActionProvider)MenuItemCompat.getActionProvider(shareItem);
        mShareActionProvider.setShareIntent(getDefaultIntent());
*/

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_refresh:
                Updator upd = new Updator(this,R.layout.activity_main);
                upd.Compete = this;
                upd.Update();
                return true;
            case R.id.action_period:
                PeriodDialog dialog = new PeriodDialog();
                dialog.show(getFragmentManager(),"PeriodDialog");
                return true;
            case R.id.action_ip:
                // смена привязки по IP
                ConfigsDialog configsDialog = new ConfigsDialog();
                configsDialog.show(getFragmentManager(),null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    //
    @Override
    public void TaskDone() {
        LoadByers(true);
        Functions.showMessage(getApplicationContext(),R.layout.activity_main,"ОБНОВЛЕНИЕ ЗАВЕРШЕНО", Functions.ToastMessageType.Success,5000);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        // Выбираем пункт из меню
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerList.setItemChecked(position, true);
            MainMenuItem selected = mAdapter.getItem(position);
            switch (selected.id){
                case 1: LoadByers(false); break;
                case 2: LoadOrders(null,true);break;
                case 3: LoadOrders(OrderStatus.New,false); break;
                case 4: LoadOrders(OrderStatus.Sended,false); break;
                case 5: LoadOrders(OrderStatus.Executed,false); break;
            }
            setTitle(selected.title);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    // Загрузка последних заявок
    private void LoadOrders(OrderStatus status,boolean last){
        OrdersFragment frgmOrders = new OrdersFragment();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.content_frame,frgmOrders);
        fTrans.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fTrans.setTransition(FragmentTransaction.TRANSIT_UNSET);
        fTrans.addToBackStack("last_orders_fragment");
        fTrans.commit();
        frgmOrders.setParams(status,last);
    }

    // Метод загружает коллекцию покупателей
    private void LoadByers(boolean reload){
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        fTrans.setTransition(FragmentTransaction.TRANSIT_UNSET);
        fTrans.replace(R.id.content_frame, frgmByers);
        frgmByers.LoadByers(reload);
        fTrans.addToBackStack("byers_fragment");
        fTrans.commit();
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    // выезжающее меню
    private class CustomActionBarDrawerToggle extends ActionBarDrawerToggle {

        public CustomActionBarDrawerToggle(Activity mActivity,DrawerLayout mDrawerLayout){
            super(
                    mActivity,
                    mDrawerLayout,
                    R.drawable.ic_drawer,
                    R.string.ns_menu_open,
                    R.string.ns_menu_close);
        }

        @Override
        public void onDrawerClosed(View view) {
            invalidateOptionsMenu();
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            invalidateOptionsMenu();
        }
    }

   //  Called whenever we call invalidateOptionsMenu()
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_refresh).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case Workarea.LOGON_ACTIVITY:
                if(resultCode==RESULT_CANCELED){
                    finish();
                }
                break;
            default:
                super.onActivityResult(requestCode,resultCode,data);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}

